﻿var JobGrid;
var jobTypes = null;
var funds = null;
var jobStatus;
// Format Job Type Combo
function FormatJobType(id) {
    var value = null;
    for (var i = 0, iLen = jobTypes.length; i < iLen; i++) {
        if (jobTypes[i].Id == id) {
            value = jobTypes[i].Name;
        }
    }
    return value;
}

// Format Fund Combo
function FormatFunds(id) {
    var value = null;
    for (var i = 0, iLen = funds.length; i < iLen; i++) {
        if (funds[i].Id == id) {
            value = funds[i].Name;
        }
    }
    return value;
}

jsJobs = function () {
    var EntityId = 0;
    var isAddMode = false;
    var EntityType = "Jobs";
    function GetValues() {
        var request = { Fundid: $("#FundMainId").val() };
        var result = CallController('/Jobs/GetJobList', 'Get', request);
        jobTypes = CallController('/Jobs/TaskTemplates', 'Get', null);
        funds = CallController('/Jobs/Funds', 'Get', null);
        //for (var i = 0; i < result.length; i++) {
        //    result[i].CreateDate = ParseDate(result[i].CreateDate);
        //    result[i].DueDate = ParseDate(result[i].DueDate);
        //    if (i == 1) {
        //        console.log(result[i].CreateDate);
        //        console.log(result[i].DueDate);
        //    }
        //}
        var columns = [];

        console.log(result);
        if (jsJobs.IsScUser) {

            columns = [
                {
                    field: "FirmName",
                    title: "Firm Name",
                    width: 150,
                    editable: false,
                },
                {
                    field: "FundName",
                    title: "Client Name",
                    width: 150,
                    editable: false,
                }, {
                    field: "FinancialYear",
                    title: "FY",
                    width: 50,
                    editable: false,
                },
                {
                    title: "Job Type",
                    field: "JobTypeName",
                    width: 145,
                    editable: false,
                    template: "<a target='_blank' href='" + siteRoot + "/Workflow/Recent/#:Id#'>#:JobTypeName#</a>"
                },
                //{
                //    title: "P. A.",
                //    width: 50,
                //    template: "<div style='text-align:left;' class='action-tools'><a target='_blank' class='btn btn-primary btn-xs btn-font' href='" + siteRoot + "/FileManager/Recent?FirmId=#:FirmId #&FundId=#:FundId #&DocTypeId=8&FinancialYear=#:FinancialYear #'><i class='fa fa-folder-open-o'></i></a></div>"
                //},
                {
                    title: "Created Date",
                    format: "{0:dd/MM/yyyy}",
                    field: "CreatedDateString",
                    width: 100,
                    editable: false,
                },
                 {
                     title: "Due Date",
                     format: "{0:dd/MM/yyyy}",
                     field: "DueDateString",
                     width: 100,
                     editable: false,
                 },
                {
                    title: "Status",
                    field: "StatusName",
                    width: 130,

                    //template: "<div>" + "#:data.StatusName#</div> ",
                    editor: function (container, options) {

                        var input = $('<input name="' + options.field + '"/>')

                        $('<input data-text-field="Text" data-value-field="Value"  data-bind="value:' + options.field + '"/>').appendTo(container).kendoDropDownList({
                            dataSource: jobStatus,
                            change: function (e) {
                                if (this.value() != "") {
                                    var statusId = this.value();
                                    var gview = $("#jobs-table").data("kendoGrid");
                                    //  gview.select(_currentcell.parentElement);
                                    var selectedItem = gview.dataItem(gview.select());
                                    selectedItem.Status = statusId
                                    var request = {
                                        Id: selectedItem.Id, Status: statusId, StatusName: this.text()
                                    }

                                    var jobStatus = CallController('/Jobs/GetJobCurrentStatus', 'Get', request);
                                    if (jobStatus.StatusName == 'Query Stage') {
                                        selectedItem.StatusName = this.text();
                                        gview.saveChanges();
                                        bootbox.confirm({
                                            message: "Please note - the fund still has some outstanding queries. Are you sure you want change status?",
                                            buttons: {
                                                confirm: {
                                                    label: 'Yes',
                                                    className: 'btn-success'
                                                },
                                                cancel: {
                                                    label: 'No',
                                                    className: 'btn-danger'
                                                }
                                            },
                                            callback: function (result) {
                                                if (result) {
                                                    var result1 = CallController('/Jobs/UpdateStatus', 'Post', request);
                                                    selectedItem.StatusName = request.StatusName;
                                                    gview.saveChanges();
                                                    return;
                                                }
                                                else {
                                                    selectedItem.StatusName = jobStatus.StatusName;
                                                    gview.saveChanges();
                                                    return;
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        var result = CallController('/Jobs/UpdateStatus', 'Post', request);
                                        if (result == undefined && result.HttpCode != "200") {
                                            ModelErrorMessage('#Message', '', result.Message);
                                            return;
                                        } else {
                                            selectedItem.StatusName = this.text();
                                            this.close();
                                            SuccessMessage('U', '');
                                        }
                                        gview.saveChanges();
                                    }

                                }
                            }
                        });

                    },

                }
                , {
                    width: 180,
                    template: "<div class='action-tools'> #if(JobTypeName == 'SMSF ESTABLISHMENT' || JobTypeName == 'SMSF TRANSFER' || JobTypeName == 'ACCOUNTING SERVICES' || JobTypeName == 'SMSF AUDIT' ||  JobTypeName == 'SMSF PORTFOLIO' || JobTypeName == 'DOCUMENT SERVICES') {# <a class='btn btn-primary btn-xs' href='" + siteRoot + "/Workflow/ViewJob?fid=#:FundId#&jbType=#:JobTypeName#&jid=#:Id#'><i class='fa fa-vimeo'></i> </a> #} else {# &nbsp;#}#" +
                    "#if(StatusName == 'Query Stage' || StatusName == 'Review Financials'){#&nbsp;<a class='btn btn-success btn-xs' href='" + siteRoot + "/Task/Queries?id=#:Id# &jq=1&cId=#:FundId#&StatusId=0&cName=#:FundName#&fy=#: FinancialYear#'><i class='fa fa-quora'></i></a>#}else{#&nbsp;#}#" +
                    "&nbsp;<a class='btn btn-success btn-xs' onclick='jsJobs.UploadFile(#:Id#);'><i class='fa fa-cloud-upload'></i></a>" +
                    "&nbsp;#if(JobTypeName == 'DOCUMENT SERVICES'){#&nbsp;#} else {# <a class='jobEditBtn btn btn-info edit btn-xs' onclick='jsJobs.GetEditValues(#:Id#,#:FirmId#);'><i class='fa fa-edit'></i></a>#}#" +
                    "&nbsp;<a class='jobDeleteBtn btn btn-danger delete btn-xs' onclick='jsJobs.DeleteValues(#:Id#);'><i class='fa fa-trash-o'></i></a> </div>"
                }
            ];
        }
        else {
            columns = [
                {
                    field: "FirmName",
                    title: "Firm Name",
                    width: 200,
                    editable: false,
                },
                {
                    field: "FundName",
                    title: "Client Name",
                    width: 200,
                    editable: false,
                }, {
                    field: "FinancialYear ",
                    title: "FY",
                    width: 50,
                    editable: false,
                }, {
                    title: "Job Type",
                    field: "JobTypeName",
                    width: 150
                }, {
                    title: "Created Date",
                    format: "{0:dd/MM/yyyy}",
                    field: "CreateDate",
                    width: 100,
                    editable: false,
                },
                {
                    title: "Status",
                    field: "StatusName",
                    width: 130,

                    editable: true,
                    template: "<div>" + "#:data.StatusName#</div> ",
                    editor: function (container, options) {
                        _currentcell = container[0];
                        var input = $('<input  name="' + options.field + '"/>')
                        input.appendTo(container);
                        input.kendoDropDownList({
                            autoBind: false,
                            optionLabel: 'Select',
                            dataTextField: "Text",
                            dataValueField: "Value",
                            dataSource: jobStatus,

                            change: function (e) {
                                if (this.value() != "") {
                                    var statusId = this.value();
                                    var gview = $("#gridProjectList").data("kendoGrid");
                                    //  gview.select(_currentcell.parentElement);
                                    var selectedItem = gview.dataItem(gview.select());
                                    // gview.editCell(_currentcell);
                                    $.ajax({
                                        type: "POST",
                                        url: '../handler/ProjectManagement.ashx?action=updateProjectStatusInProject&statusId=' + statusId + '&projectId=' + selectedItem.ProjectId,
                                        contentType: false,
                                        processData: false,
                                        success: updateStatus,
                                        error: OnError
                                    });
                                }

                                // Use the value of the widget
                            },
                        }).appendTo(container);
                    },
                }

                , {
                    width: 150,
                    template: "<div class='action-tools'> " +
                    "#if(StatusName != 'Query Stage'){#&nbsp;#} else {#&nbsp;<a class='btn btn-success btn-xs' href='" + siteRoot + "/Task/Queries?id=#:Id# &jq=1&cId=#:FundId#&StatusId=0&fy=#: FinancialYear#'><i class='fa fa-quora'></i></a>#}#" +
                    "#if(StatusName == 'Completed'){#&nbsp;<a class='btn btn-success btn-xs' href='" + siteRoot + "/FileManager/Recent?FirmId=#:FirmId #&FundId=#:FundId #&DocTypeId=10&FinancialYear=#:FinancialYear #'><i class='fa fa-folder-open-o'></i></a>#} else{#&nbsp;#}#" +
                    "&nbsp;<a class='btn btn-success btn-xs' onclick='jsJobs.UploadFile(#:Id#);'><i class='fa fa-cloud-upload'></i></a> </div>"
                }
            ];
        }
        if (!JobGrid) {
            JobGrid = $("#jobs-table").kendoGrid({
                selectable: "row",
                editable: true,
                dataSource: {
                    data: result,
                    schema: {
                        model: {
                            fields: {
                                CreateDate: { type: "date", editable: false },
                                DueDate: { type: "date", editable: false },
                                JobTypeName: { editable: false },
                                FinancialYear: { editable: false },
                                FirstName: { editable: false },
                                FirmName: { editable: false },
                                FundName: { editable: false },
                            }
                        }
                    },
                    pageSize: 20
                },
                height: 750,
                sortable: true,
                //filterable: true,
                filterable: {
                    operators: {
                        string: {
                            contains: "Contains",
                            doesnotcontain: "Does not contain",
                            eq: "Equal to",
                            neq: "Not equal to",
                            startswith: "Starts with",
                            endswith: "Ends with",
                            special: "Contains special characters",
                            isempty: "Is empty",
                            isnotempty: "Is not empty",
                            isnotnull: "Is not null",
                            isnull: "Is null",
                            neq: "Not equal"
                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gt: "Greater than",
                            gte: "Greater than or equal",
                            lt: "Less than",
                            lte: "Less than or equal",
                            isnotnull: "Is not null",
                            isnull: "Is null"
                        }
                    }
                },
                groupable: true,
                pageable: {
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: columns
            });
        }
        else {
            JobGrid.data('kendoGrid').dataSource.data(result);
            JobGrid.data('kendoGrid').refresh();

        }

        setTimeout(function () {
            if (!jsJobs.isEditAllowed) {
                $('.jobEditBtn').hide();
                $('.jobDeleteBtn').hide();
            }
        }, 10);
    }


    function updateStatus() {

        $('#jobs-table').data('kendoGrid').dataSource.read();
        $('#jobs-tablee').data('kendoGrid').refresh();
    }
    function CheckNull(value) {
        return (value == null) ? "" : value;
    }

    function format(d) {
        // `d` is the original data object for the row
        var values = d[0].split('|');
        if (values.length > 0) {
            var gender = values[5] == 0 ? 'Male' : 'Female';
            var date = new Date(parseInt(values[4].substr(6)));
            var formattedDate = ("0" + (date.getMonth() + 1)).slice(-2) + "-" +
                ("0" + date.getDate()).slice(-2) + "-" + date.getFullYear();

            return '<table class="table table-celled" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px; margin-bottom:20px;">' +
                '<tr>' +
                '<td>Title:</td>' +
                '<td>' + values[0] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Email:</td>' +
                '<td>' + values[1] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Phone:</td>' +
                '<td>' + values[2] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>TFN:</td>' +
                '<td>' + values[3] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>DOB:</td>' +
                '<td>' + formattedDate + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Gender:</td>' +
                '<td>' + gender + '</td>' +
                '</tr>' +
                '</table>';
        }
    }

    function GetEditValues(id, FirmId) {
        displayLoading(document.body);
        GetInitialValues(true).done(function (result2) {
            BindComboV2("#FundId", "/Jobs/Funds", null).done(function (result) {
                var request = { Id: id, EntityType: jsJobs.EntityType };
                var result = CallController('/Jobs/Show', 'Get', request);
                console.log(result);
                $('#Name').val(result.Name);
                $('#JobType').selectpicker('val', result.JobType);
                $('#FundId').selectpicker('val', result.FundId);
                $('#Id').val(result.Id);
                $('#FinancialYear').selectpicker('val', result.FinancialYear);
                $('#chkIsCompleted').prop('checked', result.IsCompleted);
                $('#CreateDate').val(result.CreateDate);
                $('#DueDate').val(result.DueDate);
                $('#ResponsibleManagerID').selectpicker('val', result.ResponsibleManagerID);
                $('#ReviewerID').selectpicker('val', result.ReviewerID);
                if (result.SeniorAcountantUserID != 'undefined')
                    $('#SeniorAcountantUserID').selectpicker('val', result.SeniorAcountantUserID);
                $('#AssignedUserID').selectpicker('val', result.AssignedUserID);
                $('#FirmId').selectpicker('val', FirmId);

                $('#ResponsibleManagerID').selectpicker('refresh');
                $('#ReviewerID').selectpicker('refresh');
                $('#SeniorAcountantUserID').selectpicker('refresh');
                $('#AssignedUserID').selectpicker('refresh');
                $('#SeniorAcountantUserID').selectpicker('refresh');

                BindJobMainDocuments(id, 'Job');
                openNav('JobModal');
                hideLoading(document.body);
            });
        });

    }

    function GetInitialValues(skipTemplates) {
        $('#dvSeniorAccountant').show();
        $('#Name').val('');
        $('#Id').val('0');
        $('#chkIsCompleted').prop('checked', false);

        $('#CreateDate').val('');
        $('#DueDate').val('');
        $('#ResponsibleManagerID').selectpicker('val', '');
        $('#ReviewerID').selectpicker('val', '');
        $('#AssignedUserID').selectpicker('val', '');
        $('#SeniorAcountantUserID').selectpicker('val', '');

        $('#JobType').selectpicker('val', '');
        $('#FinancialYear').selectpicker('val', '');
        $('#FundId').selectpicker('val', '');
        $('#FirmId').selectpicker('val', '');


        return BindComboV2("#JobType", "/Jobs/TaskTemplates", null);

    }

    function BindCombo(id, url, request) {
        // For Contacts Type Combo
        var resultType = CallController(url, 'Get', request);
        $(id).empty();
        var options = "";
        // options = "<option value='-1'>None</option>"; // uncomment if you want this item
        $.each(resultType, function (a, b) {
            options += "<option value='" + b.Id + "'>" + b.Name + "</option>";
        });
        $(id).html(options);
        $(id).selectpicker('refresh');
    }

    function BindComboV2(id, url) {
        var promiseCall = CallControllerV2(url, 'Get', null);
        promiseCall.done(function (result) {
            $(id).empty();
            var options = "";
            $.each(result, function (a, b) {
                options += "<option value='" + b.Id + "'>" + b.Name + "</option>";
            });
            $(id).html(options);
            $(id).selectpicker('refresh');
        });
        return promiseCall;
    }

    function OnError() {

    }


    function DeleteValues(id) {
        bootbox.confirm({
            message: "Are you sure you want to delete?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var request = { Id: id, EntityType: jsJobs.EntityType };
                    CallController('/Jobs/Delete', 'Post', request);
                    SuccessMessage('D', '');
                    GetValues();
                }
                else
                    return;
            }
        });
    }

    function ValidateAndSave() {
        var frmJobs = $('input[id="CreateDate"]').closest("form");
        $.validator.unobtrusive.parse(frmJobs);
        if (frmJobs.valid()) {
            AddUpdate();
        }
    }


    function UploadFile(id) {
        UploadControl(id, 0, '', 'Job-M')
    }

    function AddUpdate() {
        var request = {
            Id: $('#Id').val(), FundId: $('#FundId').val(), EntityType: jsJobs.EntityType, JobType: $('#JobType').val(), Name: $('#Name').val(), FinancialYear: $('#FinancialYear').val(), IsCompleted: $('#chkIsCompleted').is(":checked"),
            CreateDate: $('#CreateDate').val(), DueDateString: $('#CreateDate').val() + '$' + $('#DueDate').val(), DueDate: $('#DueDate').val(), ResponsibleManagerID: $('#ResponsibleManagerID').val(), ReviewerID: $('#ReviewerID').val(), SeniorAcountantUserID: $('#SeniorAcountantUserID').val(), AssignedUserID: $('#AssignedUserID').val()
        }
        isAddMode = false;

        var result = CallController('/Jobs/AddEdit', 'Post', request);
        if (result !== undefined && result.HttpCode != "200") {
            ModelErrorMessage('#Message', '', result.Message);
            return;
        } else {
            GetValues();
            JobGrid.data('kendoGrid').refresh();
            //$('#JobModal').modal('hide');
            closeNav('JobModal');
            SuccessMessage('U', '');
        }
    }

    function BindJobMainDocuments(DocId, ViewType) {
        // For Documents
        var request = {
            Id: DocId, ViewType: ViewType
        };
        var result = CallController('/TaskQuickView/DocumentsQuickView', 'Get', request, 'html');
        $('#div_JobDocuments').html(result);
        $("#div_JobDocuments #hf_ViewType").val("EditJob");

    }

    return {
        EntityType: EntityType, GetValues: GetValues,
        AddUpdate: AddUpdate, ValidateAndSave: ValidateAndSave, GetInitialValues: GetInitialValues, GetEditValues: GetEditValues,
        DeleteValues: DeleteValues, BindCombo: BindCombo, UploadFile: UploadFile
    }
}();
