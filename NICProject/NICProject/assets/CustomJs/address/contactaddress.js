var contactkendoGrid;
ContactAddressFile = function () {
    var isEditing = null;
    var addressTypes = null;
    var mainFormID = null;
    var isAddMode = false;
    var FormType_id = "";
    var FundRequestId = null;


    function AttachAddress() {

        var selectedValue = $("#AttachAddressTypeId :selected").text();
        if (!ValidateData(selectedValue)) {
            return;
        }

        var addressType = $('#AttachAddressTypeId').val();
        var addressId = $('#address_id').val();
        if ((ContactAddressFile.mainFormID > 0 && ContactAddressFile.mainFormID != null && ContactAddressFile.mainFormID != undefined) || (ContactAddressFile.FundRequestId > 0 && ContactAddressFile.FundRequestId != null && ContactAddressFile.FundRequestId != undefined)) {
            if (addressId != undefined && addressId > 0) {
                var request = {
                    AddressTypeId: addressType, AddressId: addressId, Main_id: ContactAddressFile.mainFormID, id: 0, FormType_id: ContactAddressFile.FormType_id, FundRequestId: FundRequestId
                }
                var result = CallController('/Address/AddressAttach', 'Post', request);
                if (typeof result != undefined && result.HttpCode != "200") {
                    ModelErrorMessage('#Message', '', result.Message);
                    return;
                } else {
                    //$('#AddressAttachModal').modal('hide');
                    closeNav('AddressAttachModal');
                    GetValues();
                    SuccessMessage('U', '');
                }
            } else {
                ModelErrorMessage('#Message', '', 'Must Select Address before Attachment.');
            }
        } else {
            ModelErrorMessage('#Message', '', 'Mandatory fields missing on Main Tab.');
        }
    }

    function GetValues() {
        GetInitialValues();
        var request = { ID: (ContactAddressFile.mainFormID > 0 ? ContactAddressFile.mainFormID : FundRequestId), FormType: ContactAddressFile.FormType_id };
        var result = CallController('/Address/AddressPartial', 'Get', request);
        ContactAddressFile.addressTypes = CallController('/Address/AddressPartialGetTypes', 'Get', null);
        //var existingGrid = $('#contact-address-table').data('kendoGrid');
        
        if (!contactkendoGrid) {
            contactkendoGrid = $("#contact-address-table").kendoGrid({
                dataSource: {
                    data: result,
                    pageSize: 10
                },
                //height: 950,
                filterable: true,
                sortable: true,
                pageable: {
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                    {
                        //field: "AddressTypeId",
                        template: "#= ContactAddressFile.formatAddressType(AddressTypeId) #",
                        //template: '#= Simple(OffDuty,OnDuty) #',
                        title: "Address Type",
                        width: 100
                    },
                    {
                        field: "AddressLine1",
                        title: "Address Line 1",
                        width: 150
                    }, {
                        field: "State",
                        title: "State",
                        width: 100
                    }, {
                    field: "Country",
                    title: "Country",
                    width: 100
                }
                    , {
                        template: "<div class='action-tools'> <a class='btn btn-success btn-xs btn-font btn-primary' onclick='ContactAddressFile.GetEditValues(#: id #);'><i class='fa fa-edit'></i></a>" +
                        "&nbsp;&nbsp;<a class='btn btn-danger btn-xs btn-font btn-Delete' onclick='ContactAddressFile.DeleteValues(#: id #);'><i class='fa fa-trash-o'></i></a> </div>"
                    }]
            });
        } else {
            contactkendoGrid.data('kendoGrid').dataSource.data(result);
            contactkendoGrid.data('kendoGrid').refresh();
        }
        


    }

    function GetEditValues(Id, AddressId) {

        GetInitialValues();
        var request = { ID: Id, FormType: ContactAddressFile.FormType_id };
        var result = CallController('/Address/AddressPartialEdit', 'Get', request);
        $('#ContactAddressPartialModal #AddressId').val(result.AddressId);
        $('#ContactAddressPartialModal #AddressLine1').val(result.AddressLine1);
        $('#ContactAddressPartialModal #AddressLine2').val(result.AddressLine2);
        $('#ContactAddressPartialModal #City').val(result.City);
        $('#ContactAddressPartialModal #State').val(result.State);
        $('#ContactAddressPartialModal #PostCode').val(result.PostCode);
        $('#ContactAddressPartialModal #Country').val(result.Country);
        $('#ContactAddressPartialModal #id').val(result.id);
        $(' #ContactAddressPartialModal#AddressTypeId_Old').val(result.AddressTypeId);

        $("#ContactAddressPartialModal #AddressTypeId option").each(function () {
            if ($(this).val() == result.AddressTypeId) {
                $(this).attr('selected', 'selected');
                $('#ContactAddressPartialModal #select2-AddressTypeId-container').text($("#ContactAddressPartialModal #AddressTypeId :selected").text());
            }
        });
        ShowAddressFinder(false)
        //$('#AddressPartialModal').modal('show');
        openNav("ContactAddressPartialModal");

    }


    function ShowAddressFinder(flag) {
        if (flag) {
            $(".visible").hide();
            $("#ContactAddressPartialModal .txtMailingAddress").show();
            $('#ContactAddressPartialModal #showCA').prop('checked', false);
        }
        else {
            $(".visible").show();
            $("#ContactAddressPartialModal .txtMailingAddress").hide();
            $('#ContactAddressPartialModal #showCA').prop('checked', true);
        }
    }

    function ClearFields() {
        ShowAddressFinder(false);
        $("#ContactAddressPartialModal #address_fieldCA").val("");
        $('#ContactAddressPartialModal #AddressId').val('0');
        $('#ContactAddressPartialModal #AddressLine1').val('');
        $('#ContactAddressPartialModal #AddressLine2').val('');
        $('#ContactAddressPartialModal #City').val('');
        $('#ContactAddressPartialModal #State').val('');
        $('#ContactAddressPartialModal #PostCode').val('');
        $('#ContactAddressPartialModal #Country').val('');
        $('#ContactAddressPartialModal #id').val('0');
    }

    function GetInitialValues() {
        ClearFields();

        ContactAddressFile.mainFormID = $('#ContactId').val();
        ContactAddressFile.FormType_id = 'Contact';

        // For Address Type Combo
        var resultType = CallController('/Address/AddressPartialGetTypes', 'Get', null);
        addressTypeSelect = document.querySelector("#ContactAddressPartialModal #AddressTypeId");
        // Previous
        $('#ContactAddressPartialModal #AddressTypeId').empty();
        if (addressTypeSelect != null) {
            $.each(resultType, function (i, item) {
                addressTypeSelect.options[addressTypeSelect.options.length] = new Option(item.Name, item.Id);
            });
        }
        $('#ContactAddressPartialModal #AddressTypeId').selectpicker('refresh');

    }

    function GetPopupValues() {
        // For Address Type Combo
        var resultType = CallController('/Address/AddressPartialGetTypes', 'Get', null);
        addressTypeSelect = document.getElementById('AttachAddressTypeId');
        $('#AttachAddressTypeId').empty();
        console.log("Address Type Combo");
        console.log(resultType);
        if (addressTypeSelect != null) {
            $.each(resultType, function (i, item) {
                addressTypeSelect.options[addressTypeSelect.options.length] = new Option(item.Name, item.Id);
            });
        }

        // For Address Combo
        var resultAddress = CallController('/Address/AddressGetAll', 'Get', null);
        addressSelect = document.getElementById('address_id');
        $('#address_id').empty();
        console.log("Address Combo");
        console.log(resultAddress);
        $.each(resultAddress, function (i, item) {
            addressSelect.options[addressSelect.options.length] = new Option(item.Name, item.Id);
        });

        //$("#AttachAddressTypeId").select2({ dropdownParent: $("#AddressAttachModal") });
        //$("#address_id").select2({ dropdownParent: $("#AddressAttachModal") });
    }


    // Format Address Type Combo
    function formatAddressType(id) {
        var value = null;
        for (var i = 0, iLen = ContactAddressFile.addressTypes.length; i < iLen; i++) {
            if (ContactAddressFile.addressTypes[i].Id == id) {
                value = ContactAddressFile.addressTypes[i].Name;
            }
        }
        return value;
    }

    function DeleteValues(Id) {
        bootbox.confirm({
            message: "Are you sure you want to delete?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var request = { ID: Id, FormType: ContactAddressFile.FormType_id };
                    CallController('/Address/AddressPartialDelete', 'Post', request);
                    SuccessMessage('D', '');
                    GetValues();
                }
                else
                    return;
            }
        });
    }

    function ValidateData(selectedValue) {
        // Validate Address Type if Exist
        var isExist = false;
        $('#contact-address-table > tbody  > tr').each(function () {
            if (this.cells[0].innerHTML == selectedValue) {
                ModelErrorMessage('#Message', '', 'Address Type Already Exist.');
                isExist = false;
                return isExist;
            } else {
                isExist = true;
            }
        });
        return isExist;
    }

    function ValidateAndSave() {
        if ($('#frmContactAddress').valid()) {
            AddUpdate();
        }
    }

    function AddUpdate() {
        var selectedValue = $("#ContactAddressPartialModal #AddressTypeId :selected").text();
        var newValue = $('#ContactAddressPartialModal #AddressTypeId').val();
        var oldValue = $('#ContactAddressPartialModal #AddressTypeId_Old').val();
        //if (oldValue != newValue && !ValidateData(selectedValue)) {
        //    return;
        //}

        if ((ContactAddressFile.mainFormID > 0 && ContactAddressFile.mainFormID != null && ContactAddressFile.mainFormID != undefined) || (ContactAddressFile.FundRequestId > 0 && ContactAddressFile.FundRequestId != null && ContactAddressFile.FundRequestId != undefined)) {
            var request = {
                FundRequestId: FundRequestId,
                AddressTypeId: $('#ContactAddressPartialModal #AddressTypeId').val(), AddressId: $('#ContactAddressPartialModal #AddressId').val(), AddressLine1: $('#ContactAddressPartialModal #AddressLine1').val(), AddressLine2: $('#ContactAddressPartialModal #AddressLine2').val(), City: $('#ContactAddressPartialModal #City').val(),
                State: $('#ContactAddressPartialModal #State').val(), PostCode: $('#ContactAddressPartialModal #PostCode').val(), Country: $('#ContactAddressPartialModal #Country').val(), id: $('#ContactAddressPartialModal #id').val(), Main_id: ContactAddressFile.mainFormID, FormType_id: ContactAddressFile.FormType_id
            }
            isAddMode = false;
            var result = CallController('/Address/AddressPartial', 'Post', request);
            if (typeof result != undefined && result.HttpCode != "200") {
                ModelErrorMessage('#ContactAddressPartialModal #Message', '', result.Message);
                return;
            } else {
                //$('#AddressPartialModal').modal('hide');
                closeNav('ContactAddressPartialModal');
                GetValues();
                SuccessMessage('U', '');
            }
        } else {
            ModelErrorMessage('#Message', '', 'Mandatory fields missing on Main Tab.');
        }
    }

    return {
        FormType_id: FormType_id, GetValues: GetValues,
        GetPopupValues: GetPopupValues, AttachAddress: AttachAddress,
        AddUpdate: AddUpdate, ValidateAndSave: ValidateAndSave, GetInitialValues: GetInitialValues, GetEditValues: GetEditValues,
        DeleteValues: DeleteValues, mainFormID: mainFormID, formatAddressType: formatAddressType
    }
}();



(function () {
    var widget, initAF = function () {
        widget = new AddressFinder.Widget(
            document.getElementById('address_fieldCA'),
            AddressFinderAccessKey,
            'AU',
            { show_locations: false }
        );

        widget.on("result:select", function (fullAddress, metaData) {
            $('#ContactAddressPartialModal #AddressLine1').val(metaData.address_line_1);
            $('#ContactAddressPartialModal #AddressLine2').val(metaData.address_line_2);
            $('#ContactAddressPartialModal #City').val(metaData.locality_name);
            $('#ContactAddressPartialModal #State').val(metaData.state_territory);
            $('#ContactAddressPartialModal #PostCode').val(metaData.postcode);
            $('#ContactAddressPartialModal #Country').val("Australia");
            $('.af_list').hide();
        });
    };

    $(document).ready(function () {
        $.getScript('https://api.addressfinder.io/assets/v3/widget.js', initAF);

        $("#ContactAddressPartialModal #showCA").click(function () {
            var val = $('#ContactAddressPartialModal #showCA').is(":checked");
            if (val == true) {
                $("#ContactAddressPartialModal .visible").show();
                $("#ContactAddressPartialModal .txtMailingAddress").hide();
            } else {
                $("#ContactAddressPartialModal .visible").hide();
                $("#ContactAddressPartialModal .txtMailingAddress").show();
            }
        });

    });

})();
