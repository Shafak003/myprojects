from base64 import urlsafe_b64encode

from django.contrib import redirects
from django.contrib.auth import authenticate
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpRequest, HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import login
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_decode
from django.views.decorators.csrf import csrf_exempt
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.contrib.auth import login as auth_login


# Create your views here.
from django.conf import settings

from .tokens import account_activation_token


def index(request):
    templates='index.html'
    return render(request,templates)


def homepage(request):
    templates='homepage.html'
    return render(request,templates)


@csrf_exempt
def create_user(request):
    if request.method =='POST':
        first_name = request.POST.get('username')
        username = request.POST.get('mobile')
        email = request.POST.get('email')
        password = request.POST.get('password')

        User.objects.create_user(
        first_name = first_name,
        username = username,
        email = email,
        password = password
         )
        print(username)
        data = {
            'is_taken': User.objects.filter(username__iexact=username).exists()
        }
        print(data)
        return JsonResponse(data)
    return HttpResponse('')


@csrf_exempt
def forgotpass(request):
    if request.method == 'POST':
        email = request.POST.get('email')
        print(email)
        subject = "Email send successfully"
        # current_site = get_current_site(request)
        # message = render_to_string('acc_active_email.html', {
        #     'domain': current_site.domain,
        #     'uid': urlsafe_b64encode(force_bytes(User.pk))
        # })
        message = 'hii....'
        from_email = settings.EMAIL_HOST_USER
        send_mail(subject=subject, message=message, from_email=from_email, recipient_list=[email], fail_silently=False)
    return render(request,'forgetpassmail.html')



#Activation function of an Email
def ActivateAccountView(request, uidb64,token):
      user = ''
      try:
        print(uidb64)
        uid =force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
      except Exception as e:
         print("exception happen")
         print(str(e))

      if user is not None and account_activation_token.check_token(user,token):
        user.is_email_verified = True
        user.is_active = True
        user.save()
        auth_login(request, user)
        # return redirect('home')
        return HttpResponse('Thank you for your email confirmation.Now you can login your account.')
      else:
        print(user)
        return HttpResponse('Activation link is invalid!')



@csrf_exempt
def passconfm(request):
    if request.method == 'POST':
        password = request.POST.get('password')
        Confirm_pass = request.POST.get('Confirm_password')
    return render(request,'passconfm.html')


def validate_username(request):
  if request.method == 'GET':
    username = request.GET('username')
    data = {
     'is_taken': User.objects.filter(username__iexact=username).exists()
    }
    return JsonResponse(data)


@csrf_exempt
def UserSingIn(request):
        if request.method == "POST":
            username = request.POST.get('mobile')
            password = request.POST.get('password')
            user = authenticate(request,username=username,password=password)
            print(user)
            if user is not None :
                print(user)
                if user.is_active:
                    login(request, user)
                    data = {
                        'is_taken': User.objects.filter(username__iexact=username).exists()
                    }
                    print(data)
                    return JsonResponse(data)
        return render(request,'login.html')










