from django.db import models
from passlib.hash import pbkdf2_sha256
# Create your models here.


class User(models.Model):
    password = models.CharField(max_length=32)
    first_name = models.CharField(max_length=32,null=True)
    username = models.CharField(max_length=32)
    email = models.EmailField()


    # def verify_password(self,raw_password):
    #     return pbkdf2_sha256.verify(raw_password,self.password)

def __str__(self):
    return self.username