﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using NICProject.Models;

namespace NICProject.Database_Access_Layer
{
   
    public class Dbcon
    {
        public SqlConnection con;
        public SqlCommand cmd;

        public Dbcon()
        {
            con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["VisitorTable_1"].ToString();
        }
        public void insert_record(Employee emp)
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "VistorReg";
            cmd.Parameters.AddWithValue("@Id",emp.Id);
            cmd.Parameters.AddWithValue("@Identification", emp.Identification);
            cmd.Parameters.AddWithValue("@CardNo", emp.CardNo);
            cmd.Parameters.AddWithValue("@FirstName", emp.FirstName);
            cmd.Parameters.AddWithValue("@MiddleName", emp.MiddleName);
            cmd.Parameters.AddWithValue("@LastName", emp.LastName);
            cmd.Parameters.AddWithValue("@Address", emp.Address);
            cmd.Parameters.AddWithValue("@Mobile", emp.Mobile);
            cmd.Parameters.AddWithValue("@Date", emp.Date);
            cmd.Parameters.AddWithValue("@Time", emp.Time);
            cmd.Parameters.AddWithValue("@Company",emp.Company);
            cmd.Parameters.AddWithValue("Purpose", emp.Purpose);
            con.Open();
            cmd.ExecuteNonQuery();

        }
      
    }
}