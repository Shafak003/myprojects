﻿var QueryGrid;

function BindQueries() {
    var request = null;
    var jobTaskId = $('#TaskId').val() == 0 ? $('#Task').val() : $('#TaskId').val();
    if ($('#JobId').val() != "") {
        jobTaskId = $('#JobId').val();
    }
    if ($('#TaskMode').val() == 1) {
        request = { Id: jobTaskId, jq: $('#JobQueries').val() }
    }

    var result = CallController('/Task/GetAllQueries', 'Get', request);

    if (!QueryGrid) {

        QueryGrid = $("#task-queries").kendoGrid({
            dataSource: {
                data: result,
                pageSize: 20
            },
            //height: 950,
            sortable: true,
            //filterable: true,
            filterable: {
                operators: {
                    string: {
                        contains: "Contains",
                        doesnotcontain: "Does not contain",
                        eq: "Equal to",
                        neq: "Not equal to",
                        startswith: "Starts with",
                        endswith: "Ends with",
                        special: "Contains special characters",
                        isempty: "Is empty",
                        isnotempty: "Is not empty",
                        isnotnull: "Is not null",
                        isnull: "Is null",
                        neq: "Not equal"
                    },
                    number: {
                        eq: "Equal to",
                        neq: "Not equal to",
                        gt: "Greater than",
                        gte: "Greater than or equal",
                        lt: "Less than",
                        lte: "Less than or equal",
                        isnotnull: "Is not null",
                        isnull: "Is null"
                    }
                }
            },
            groupable: true,
            pageable: {
                pageSizes: true,
                buttonCount: 5
            },
            columns: [
                {
                    field: "ClientName",
                    //template: "#= formatAddressType(AddressTypeId) #",
                    title: "Client Name",
                    width: 150
                },
                {
                    field: "JobYear",
                    title: "Financial Year",
                    width: 100
                }, {
                    field: "JobTypeName",
                    title: "Job Type",
                    width: 200
                }, {
                    field: "TaskName",
                    title: "Task Name",
                    width: 200
                }, {
                    field: "Title",
                    title: "Query",
                    width: 250
                }, {
                    width: 120,
                    template: "<div class='action-tools'><a class='btn btn-primary btn-xs tasktools-sidebar' onclick='jsQuickView.GetInitialValuesForQueries(#:Id#);'><i class='fa fa-comments-o'></i></a>" +
                    "<a class='btn btn-info btn-xs edit tasktools-sidebar2' href='javascript:EditQuery(#:Id#);' ><i class='fa fa-edit'></i></a> " + "<a href='javascript:DeleteQuery(#:Id#);' class='btn btn-danger btn-xs delete'><i class='fa fa-trash-o'></i></a> </div>"
                }]
        });

    }
    else {
        //QueryGrid.data('kendoGrid').dataSource.data(result);
        //QueryGrid.data('kendoGrid').refresh();
    }
}

function SaveChanges() {
    var tId = $('#TaskId').val();
    if (tId == "" || tId == 0) {
        tId = $('#Task').val();
    }
    if (tId > 0) {
        var request = {
            Id: $('#Id').val(),
            Job: $('#JobId').val(),
            Title: $('#Title').val(),
            Details: $('#Details').val(),
            ImportanceId: $('#ImportanceId').val(),
            IsReviewed: $('#chkIsReviewed').val(),
            TaskId: tId
        }
        ////$('#Id').val($('#Id').val());
        ////$('#TaskId').val(tId);
        //var result = CallController('/Task/AddEditQuery', 'Post', request);

        //if (typeof result != undefined && result.HttpCode != 200) {
        //    ErrorMessage('', result.Message);
        //    //ErrorMessage('', result.ErrorMessage);
        //    return;
        //} else {
        //    //var upload = $("#queryDocs").data("kendoUpload");
        //    //upload.upload();
        //    RefreshGrid('A');
        //}

    }
}
function ValidateAndSave() {
    var frmMain = $('input[name="Title"]').closest("form");
    $.validator.unobtrusive.parse(frmMain);
    // frmMain.validate();
    //var upload = $("#queryDocs").data("kendoUpload");
    //upload.upload();

    var tId = $('#TaskId').val();
    if (tId == "") {
        tId = $('#Task').val();
    }

    if (tId == "-1") {
        ModelErrorMessage('#Message', '', 'Please select Task.');
    }
    $('#UserName').val(tId);
    $('#ShortName').val(window.location);
    if (tId != "-1" && frmMain.valid()) {
        //var upload = $("#UploadDocuments").data("kendoUpload");
        //upload.upload();
        //SaveChanges();
        //return false;
    }

    var upload = $("#query_files").data("kendoUpload"),
           files = upload.getFiles();
    var docType = $('#DocumentTypeId').val();
    if (files.length > 0 && docType <= 0) {
        ModelErrorMessage('#Message', '', 'Please select document type to attach');
        return false;
    }
}

function RefreshGrid(prefix) {
    SuccessMessage(prefix, '');
    //ModelSuccessMessage('#Message', 'D', '');
    //BindQueries();
    ClosePopup();
}

function RefreshQueryGrid() {
    if (window.location.pathname.toLowerCase() === "/task/queries" && $("#NeedToRefreshQueryGrid").val() == "1") {
        GetAllQueries(StatusId);
        $("#NeedToRefreshQueryGrid").val("0");
    }
}

function ClosePopup() {
    //$("#task-side-wrap2").addClass("collapsed");
    closeNav('QueryPartialModal');
    $("#dialog-title").text("Ask a Question");
    window.location.reload();
}

function MarkIsResolvedMarkIsReviewed(chkIsReviewed, Id, chkIsResolved) {
    var chkIsReviewed = $('#' + chkIsReviewed).is(":checked");
    var chkIsResolved = $('#' + chkIsResolved).is(":checked");

    var request = {
        Id: Id,
        IsReviewed: chkIsReviewed,
        IsResolved: chkIsResolved,
    };
    var result = CallController('/Task/MarkIsResolved', 'Post', request);
    if (typeof result != undefined && result.HttpCode == "200") {
        //SuccessMessage('U', '');
        if (window.location.pathname.toLowerCase().indexOf("/task/queries") !== -1)
            $("#NeedToRefreshQueryGrid").val("1");
        window.location.reload();
    }
}

function BindEditQueryDocuments(DocId) {
    // For Documents
    var request = {
        Id: DocId, ViewType: 'Queries'
    };
    var result = CallController('/TaskQuickView/DocumentsQuickView', 'Get', request, 'html');
    $('#dv_QueryEditDocuments').html(result);
}

function EditQuery(id) {
    var request = {
        Id: id
    };
    var result = CallController('/Task/GetQueryByID', 'Get', request);
    $("#divClient").hide();
    $("#divJob").hide();
    $("#divTask").hide();
    $("#Id").val(result.Id);
    $("#TaskId").val(result.TaskId);
    $("#Title").val(result.Title);
    $("#Details").val(result.Details);
    $("#dialog-title").text("Edit a Question");
    //jsQuickView.BindMainDocuments(result.Id, "Queries");
    //$("#hf_ViewType").val("EditQuery");
    openNav("QueryPartialModal");
    BindImportanceCombo("#ImportanceId", "/Task/GetImportanceCombo");
    BindDocumentTypes("#DocumentTypeId", "/Workflow/GetAllDocumentTypes");
    $("#ImportanceId").selectpicker('val', result.ImportanceId);
    BindEditQueryDocuments(id);
    $('#dv_QueryEditDocuments').show();
}

function DeleteQuery(Id) {
    bootbox.confirm({
        message: "Are you sure you want to delete?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                var request = {
                    Id: Id
                };
                CallController('/Task/Delete', 'Post', request);
                RefreshGrid('D');
            }
            else
                return;
        }
    });
}

function BindCombo(id, url) {
    var resultType = CallController(url, 'Get', null);
    $(id).empty();
    var options = "<option value='-1'>-- Select Client --</option>";
    $.each(resultType, function (a, b) {
        options += "<option value='" + b.Id + "'>" + b.Name + "</option>";
    });
    $(id).html(options);
    $(id).selectpicker('refresh');
}

function BindComboV2(id, url) {
    var promiseCall = CallControllerV2(url, 'Get', null);
    promiseCall.done(function (result) {
        $(id).empty();
        var options = "<option value='-1'>-- Select Client --</option>";
        $.each(result, function (a, b) {
            options += "<option value='" + b.Id + "'>" + b.Name + "</option>";
        });
        $(id).html(options);
        $(id).selectpicker('refresh');
    });
    return promiseCall;
}

function BindComboFirms(id, url) {
    var request = {
        Id: $('#Firm').val() == '-1' && typeof ClientId != "object" ? ClientId : $('#Firm').val()
    };
    if (request.Id == null) {
        request.Id = '-1';
    }

    var resultType = CallController(url, 'Get', request);
    $(id).empty();
    var options = "<option value='-1'>-- Select Firm --</option>";
    $.each(resultType, function (a, b) {
        options += "<option value='" + b.Id + "'>" + b.Name + "</option>";
    });
    $(id).html(options);
    $(id).selectpicker('refresh');
}

function BindComboJobs(id, url) {
    var request = {
        Id: $('#Client').val() == '-1' && typeof ClientId != "object" ? ClientId : $('#Client').val()
    };
    if (request.Id == null) {
        request.Id = '-1';
    }

    var resultType = CallController(url, 'Get', request);
    $(id).empty();
    var options = "<option value='-1'>-- Select Job --</option>";
    $.each(resultType, function (a, b) {
        options += "<option value='" + b.Id + "'>" + b.Name + "</option>";
    });
    $(id).html(options);
    $(id).selectpicker('refresh');
}

function BindComboTasks(id, url) {
    var request = {
        Id: $('#Job').val()
    };
    var resultType = CallController(url, 'Get', request);
    $(id).empty();
    var options = "<option value='-1'>-- Select Task --</option>";
    $.each(resultType, function (a, b) {
        options += "<option value='" + b.Id + "'>" + b.Name + "</option>";
    });
    $(id).html(options);
    $(id).selectpicker('refresh');
}

function BindImportanceCombo(id, url) {
    var resultType = CallController(url, 'Get', null);
    $(id).empty();
    //var options = "<option value='-1'>-- Select Client --</option>";
    var options = "";
    $.each(resultType, function (a, b) {
        options += "<option value='" + b.ImportanceId + "'>" + b.Name + "</option>";
    });
    $(id).html(options);
    $(id).selectpicker('refresh');

    //set option 'None' by default.
    $(id).selectpicker('val', 3);
}

function BindDocumentTypes(id, url) {
    var resultType = CallController(url, 'Get', null);
    $(id).empty();
    var options = "<option value='-1'>-- Select Document Type --</option>";
    $.each(resultType, function (a, b) {
        options += "<option value='" + b.Id + "'>" + b.Name + "</option>";
    });
    $(id).html(options);
    $(id).selectpicker('refresh');

    //set option 'None' by default.
    $(id).selectpicker('val', -1);
}

//function onComplete(e) {
//    RefreshGrid('A');
//    //if (confirm("File Uploaded Successfully. Do you want to close?")) {
//    //$('#uploadMessage', opener.document).modal();
//    //setTimeout(close, 1000);

//    //}
//}
function InitializeValues(clientid, jobid) {
    $('#Id').val("");
    $("#divClient").show();
    $("#divJob").show();
    $("#divTask").show();


    $('#Title').val("");
    $('#Details').val("");
    $('#Client').selectpicker('val', clientid);

    $('#Job').selectpicker('val', jobid);

    $('#Task').selectpicker('val', -1);

}
