﻿var ShareContactGrid;
var contactTypes = null;
var roles = null;
var validationmessage = "";

// Format Contacts Type Combo
function formatContactsType(id) {
    var value = null;
    for (var i = 0, iLen = contactTypes.length; i < iLen; i++) {
        if (contactTypes[i].Id == id) {
            value = contactTypes[i].Name;
        }
    }
    return value;
}

// Format Roles Combo
function formatRoles(id) {
    var value = null;
    for (var i = 0, iLen = roles.length; i < iLen; i++) {
        if (roles[i].Id == id) {
            value = roles[i].Name;
        }
    }
    return value;
}


jsShareContact = function () {
    var isEditing = null;

    var EntityId = null;
    var isAddMode = false;
    var EntityType = "Share";
    var FundRequestId = null;



    function GetValues() {

        GetInitialValues();

        var request = { EntityId: (jsShareContact.EntityId == null ? jsShareContact.FundRequestId : jsShareContact.EntityId), EntityType: jsShareContact.EntityType, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual' };
        var result = CallController('/Contact/ShareContacts', 'Get', request);
        contactTypes = CallController('/Contact/GetAllContactTypes', 'Get', null);
        roles = CallController('/Contact/GetAllRoles?isfirm=' + jsShareContact.isfirm + '&issmsf=' + jsShareContact.issmsf + '&isclient=' + jsShareContact.isclient, 'Get', null);
        var icon = '';
        icon = '<i class="glyphicon glyphicon-ok"></i>';

        console.log(result);
        if (!ShareContactGrid) {
            $("#share-contacts-table").html("");
            ShareContactGrid = $("#share-contacts-table").kendoGrid({
                dataSource: {
                    data: result,
                    pageSize: 10
                },
                //height: 950,
                sortable: true,
                filterable: true,
                pageable: {
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                    //{
                    //    field: "Title + '|' +Email+ '|' + Phone + '|' + TFN + '|' + DOB + '|' + Gender",
                    //    //field: "formatAddressType(AddressTypeId)",
                    //    title: "",
                    //    width: 350
                    //},
                    //{
                    //    template: "#=formatContactsType(ContactTypeId)#",
                    //    title: "Contact Type",
                    //    width: 100
                    //},
                    {
                        field: "Title",
                        title: "Title",
                        width: 200
                    },
                    {
                        field: "TFN",
                        title: "TFN",
                        width: 150
                    }
                    , {
                        template: "<a class='btn btn-info edit' onclick='jsShareContact.GetEditValues(#:Id #,#: ContactId #);'><i class='fa fa-edit'></i></a>" +
                        "&nbsp;<a class='btn btn-danger delete' onclick='jsShareContact.DeleteValues(#: Id #);'><i class='fa fa-trash-o'></i></a>"
                    }]
            });
        }
        else {
            ShareContactGrid.data('kendoGrid').dataSource.data(result);
            ShareContactGrid.data('kendoGrid').refresh();
        }
    }

    function CheckNull(value) {
        return (value == null) ? "" : value;
    }
    // add event listener for opening and closing details
    $('#share-contacts-table tbody').on('click', 'td.details-control', function () {
        var table = $('#share-contacts-table').DataTable();
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });

    function format(d) {
        // `d` is the original data object for the row
        var values = d[0].split('|');
        if (values.length > 0) {
            var gender = values[5] == 0 ? 'Male' : 'Female';
            var date = new Date(parseInt(values[4].substr(6)));
            var formattedDate = ("0" + (date.getMonth() + 1)).slice(-2) + "-" +
                ("0" + date.getDate()).slice(-2) + "-" + date.getFullYear();

            return '<table class="table table-celled" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px; margin-bottom:20px;">' +
                '<tr>' +
                '<td>Title:</td>' +
                '<td>' + values[0] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Email:</td>' +
                '<td>' + values[1] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Phone:</td>' +
                '<td>' + values[2] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>TFN:</td>' +
                '<td>' + values[3] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>DOB:</td>' +
                '<td>' + formattedDate + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Gender:</td>' +
                '<td>' + gender + '</td>' +
                '</tr>' +
                '</table>';
        }
    }

    function GetEditValues(Id, ContactsId) {
        GetInitialValues();
        var request = { Id: Id, EntityType: jsShareContact.EntityType, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual' };
        var result = CallController('/Contact/GetShareContactById', 'Get', request);
        $('#ShareContactModal #DOB').val(result.DOB);
        $('#ShareContactModal #Email').val(result.Email);
        $('#ShareContactModal #FirstName').val(result.FirstName);
        $('#ShareContactModal #Gender').val(result.Gender);
        $('#ShareContactModal #LastName').val(result.LastName);
        $('#ShareContactModal #Phone').val(result.Phone);
        $('#ShareContactModal #TFN').val(result.TFN);
        $('#ShareContactModal #Title').val(result.Title);
        $('#ShareContactModal #RoleId').val(result.RoleId);
        $('#ShareContactModal #IsPrimary').prop('checked', result.IsPrimary);
        $('#ShareContactModal #Id').val(result.Id);
        $('#ShareContactModal #ContactId').val(result.ContactId);

        $('#ShareContactModal #AddressInfo_AddressId').val(result.AddressInfo.AddressId);
        $('#ShareContactModal #AddressInfo_AddressLine1').val(result.AddressInfo.AddressLine1);
        $('#ShareContactModal #AddressInfo_AddressLine2').val(result.AddressInfo.AddressLine2);
        $('#ShareContactModal #AddressInfo_City').val(result.AddressInfo.City);
        $('#ShareContactModal #AddressInfo_State').val(result.AddressInfo.State);
        $('#ShareContactModal #AddressInfo_PostCode').val(result.AddressInfo.PostCode);
        $('#ShareContactModal #AddressInfo_Country').val(result.AddressInfo.Country);

        openNav("ShareContactModal");
    }

    function GetInitialValues() {
        $('#ShareContactModal #DOB').val('');
        $('#ShareContactModal #Email').val('');
        $('#ShareContactModal #FirstName').val('');
        $('#ShareContactModal #Gender').val();
        $('#ShareContactModal #LastName').val('');
        $('#ShareContactModal #Phone').val('');
        $('#ShareContactModal #TFN').val('');
        $('#ShareContactModal #IsPrimary').prop('checked', false);
        $('#ShareContactModal #Title').val('');
        $('#ShareContactModal #Id').val('0');
        $('#ShareContactModal #ContactId').val('0');

        $('#ShareContactModal #AddressInfo_AddressId').val('0');
        $('#ShareContactModal #AddressInfo_AddressLine1').val('');
        $('#ShareContactModal #AddressInfo_AddressLine2').val('');
        $('#ShareContactModal #AddressInfo_City').val('');
        $('#ShareContactModal #AddressInfo_State').val('');
        $('#ShareContactModal #AddressInfo_PostCode').val('');
        $('#ShareContactModal #AddressInfo_Country').val('');

        BindCombo("#ShareContactModal #ContactIdAttach", "/Contact/GetAllContacts");

        jsShareContact.FundRequestId = $('#FundRequestId').val();
        jsShareContact.EntityId = $('#ShareId').val();
        jsShareContact.EntityType = 'Share';

        // Tab Initial Setting
        $('#ShareContactModal #sharecontactmain').addClass('active');
        $('#ShareContactModal #share-main-tab').addClass('active');

        $("#ShareContactModal #DOB").kendoDatePicker({
            animation: {
                open: {
                    effects: "zoom:in",
                    duration: 300
                }
            }
        });

        $('#ShareContactModal #ContactIdAttach').on('change', function () {

            var selected = $('#ShareContactModal #ContactIdAttach').val();
            if (selected > 0) {
                SetExistingValue(selected);
            }
        });
    }

    function SetExistingValue(selected) {
        var request = { id: selected };
        var result = CallController('/Contact/GetContact', 'Get', request);
        if (result != null) {
            $('#ShareContactModal #DOB').val(result.DOB);
            $('#ShareContactModal #Email').val(result.Email);
            $('#ShareContactModal #FirstName').val(result.FirstName);
            $('#ShareContactModal #Gender').val(result.Gender);
            $('#ShareContactModal #LastName').val(result.LastName);
            $('#ShareContactModal #Phone').val(result.Phone);
            $('#ShareContactModal #TFN').val(result.TFN);
            $('#ShareContactModal #Title').val(result.Title);

            $('#ShareContactModal #IsPrimary').prop('checked', result.IsPrimary);
            $('#ShareContactModal #Id').val(0);
            $('#ShareContactModal #ContactId').val(result.ContactId);
        }

    }
    function GetPopupValues() {
        // For Contacts Type Combo
        //BindCombo("#ContactTypeIdAttach", "/Contact/GetAllContactTypes");
        //$('#ContactTypeIdAttach').selectpicker('refresh');

        // For Contacts Combo
        BindCombo("#ContactIdAttach", "/Contact/GetAllContacts");
        //$('#ContactIdAttach').selectpicker('refresh');

        // For Contacts Combo
        //BindCombo("#ContactRoleIdAttach", "/Contact/GetAllRoles");
        //$('#ContactRoleIdAttach').selectpicker('refresh');

        //$("#ContactTypeIdAttach").select2({ dropdownParent: $("#AttachContactModal") });
        //$("#ContactIdAttach").select2({ dropdownParent: $("#AttachContactModal") });
        //$("#ContactRoleIdAttach").select2({ dropdownParent: $("#AttachContactModal") });
    }

    function BindCombo(id, url) {
        // For Contacts Type Combo
        var resultType = CallController(url, 'Get', null);
        $(id).empty();
        var options = "";
        // options = "<option value='-1'>None</option>"; // uncomment if you want this item
        $.each(resultType, function (a, b) {
            options += "<option value='" + b.Id + "'>" + b.Name + "</option>";
        });
        $(id).html(options);
        $(id).selectpicker('refresh');
    }



    function DeleteValues(Id) {
        bootbox.confirm({
            message: "Are you sure you want to delete?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var request = { ID: Id, FormType: jsShareContact.EntityType, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual' };
                    CallController('/Contact/DeleteAttachedContact', 'Post', request);
                    SuccessMessage('D', '');
                    GetValues();
                }
                else
                    return;
            }
        });
    }

    function ValidateAndSave() {
        if ($('#frmShareContact').valid()) {
            AddUpdate();
        }
    }

    function AddUpdate() {
        if ((jsShareContact.EntityId != null && jsShareContact.EntityId != undefined && jsShareContact.EntityId > 0) || (jsShareContact.FundRequestId != null && jsShareContact.FundRequestId != undefined && jsShareContact.FundRequestId > 0)) {
            var AddressInfo = {
                AddressId: $('#ShareContactModal #AddressInfo_AddressId').val(), AddressLine1: $('#ShareContactModal #AddressInfo_AddressLine1').val(), AddressLine2: $('#ShareContactModal #AddressInfo_AddressLine2').val(), City: $('#ShareContactModal #AddressInfo_City').val(),
                State: $('#ShareContactModal #AddressInfo_State').val(), PostCode: $('#ShareContactModal #AddressInfo_PostCode').val(), Country: $('#ShareContactModal #AddressInfo_Country').val()
            };
            var request = {
                Id: $('#ShareContactModal #Id').val(), FundRequestId: jsShareContact.FundRequestId, EntityId: jsShareContact.EntityId, EntityType: jsShareContact.EntityType, ContactTypeId: $('#ShareContactModal #ContactTypeId').val(), ContactId: $('#ShareContactModal #ContactId').val(), RoleId: $('#ShareContactModal #RoleId').val(), DOB: ToDateFormat($('#ShareContactModal #DOB').val()), Email: $('#ShareContactModal #Email').val(), FirstName: $('#ShareContactModal #FirstName').val(),
                Gender: $('#ShareContactModal #Gender').val(), LastName: $('#ShareContactModal #LastName').val(), Phone: $('#ShareContactModal #Phone').val(), TFN: $('#ShareContactModal #TFN').val(), Title: $('#ShareContactModal #Title').val(), IsPrimary: $("#ShareContactModal #IsPrimary").is(':checked'), IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual', AddressInfo
            }
            isAddMode = false;

            var result = CallController('/Contact/UpdateShareContact', 'Post', request);
            if (result !== undefined && result.HttpCode != "200") {
                ModelErrorMessage('#Message', '', result.Message);
                return;
            } else {
                GetValues();
                if (result.OperationStatuses.length > 0) {
                    var CurId = result.OperationStatuses[0].UniqueKey;
                    GetEditValues(CurId, 0);
                }
                closeNav('ShareContactModal');
                //$('#ContactModal').modal('hide');
                SuccessMessage('U', '');
            }

        } else {
            ModelErrorMessage('#Message', '', 'Mandatory fields missing on Main Tab.');
        }
    }

    return {
        EntityType: EntityType, GetValues: GetValues,
        GetPopupValues: GetPopupValues,
        AddUpdate: AddUpdate, ValidateAndSave: ValidateAndSave, GetInitialValues: GetInitialValues, GetEditValues: GetEditValues,
        DeleteValues: DeleteValues
    }
}();