﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NICProject.Database_Access_Layer;
using NICProject.Models;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace NICProject.Controllers
{
    public class HomeController : Controller
    {
        string connectionString = " Data Source=CG-DTE-STUDENT;Initial Catalog = CRUDwithNICProject;Integrated Security=true";
        //
        // GET: /Home/


        // this index perform view and insert visitor operation contain model employee emp 


        [HttpGet]
        public ActionResult Index(Employee emp)
        {
            return View(emp);
        }

        [HttpPost]

        public ActionResult Index(FormCollection fc)
        {
            Employee emp = new Employee();
            emp.Id = fc["Id"];
            emp.Identification = fc["Identification"];
            emp.CardNo = fc["CName"];
            emp.FirstName = fc["FName"];
            emp.MiddleName = fc["MName"];
            emp.LastName= fc["LName"];
            emp.Address = fc["Address"];
            emp.Mobile = fc["PNumber"];
            emp.Date = fc["Date"];
            emp.Time = fc["Time"];
            emp.Company = fc["Company"];
            emp.Purpose = fc["Purpose"];
            Database_Access_Layer.Dbcon dblayer = new Database_Access_Layer.Dbcon();
            dblayer.insert_record(emp);
            ViewData["MyVisitor"] = emp;
            return View("~/Views/DocumentViewer/PrintIndex.cshtml");
        }


        // it provide view to the FirstPage

        public ActionResult FirstPage() 
        {
            return View();
        }

        // it provide view to the ChangePassword Page

        public ActionResult ChangePassword()
        {
            return View();
        }


        //SecurityPersonRegistration

        [HttpGet]
        public ActionResult SecurityPersonRegistration(UserLogin usr)
        {
            return View(usr);
        }
        [HttpPost]
        public ActionResult SecurityPersonRegistration(FormCollection fc)
        {
            UserLogin usr = new UserLogin();
             usr.UserName = fc["UserName"];
            usr.Password = fc["Password"];
            Database_Access_Layer.DbForLogin dblayer = new Database_Access_Layer.DbForLogin();
            dblayer.SecurityPersonReg(usr);
            return View();
        }

        //VisitorShowRecord it only show the record FirstName,Middlename,LastName 
       
        [HttpGet]
        public ActionResult VisitorShowRecord()
        {
            DataTable dtblProduct = new DataTable();
            using (SqlConnection SqlCon = new SqlConnection(connectionString))
            {
                SqlCon.Open();
                SqlDataAdapter sqlda = new SqlDataAdapter("SELECT * from VisitorTable_1", SqlCon);
                sqlda.Fill(dtblProduct);
            }
            return View(dtblProduct);
        }

       //VisitorEditRecord or Visitor Selected Print the Record by view detail

       [HttpGet]
        public ActionResult Edit(int id)
        {
            Employee emp = new Employee();
            DataTable dtblProduct = new DataTable();
            using (SqlConnection SqlCon = new SqlConnection(connectionString))
            {
                SqlCon.Open();
                SqlDataAdapter sqlda = new SqlDataAdapter("SELECT * from VisitorTable_1 Where ID=@ID", SqlCon);
                sqlda.SelectCommand.Parameters.AddWithValue("@ID",id);
                sqlda.Fill(dtblProduct);
            }
            if (dtblProduct.Rows.Count == 1)
            {
                emp.Id = dtblProduct.Rows[0][0].ToString();
                emp.Identification = dtblProduct.Rows[0][1].ToString();
                emp.CardNo = dtblProduct.Rows[0][2].ToString();
                emp.FirstName = dtblProduct.Rows[0][3].ToString();
                emp.MiddleName = dtblProduct.Rows[0][4].ToString();
                emp.LastName = dtblProduct.Rows[0][5].ToString();             
                emp.Address = dtblProduct.Rows[0][6].ToString();
                emp.Mobile = dtblProduct.Rows[0][7].ToString();
                emp.Date = dtblProduct.Rows[0][8].ToString();
                emp.Time = dtblProduct.Rows[0][9].ToString();
                emp.Company = dtblProduct.Rows[0][10].ToString();
                emp.Purpose = dtblProduct.Rows[0][11].ToString();

                return View(emp);
            }
            else
            return RedirectToAction("FirstPage");     
        }

        //Login Authentication through Database

        public ActionResult homepage()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Authorize(NICProject.Models.DBConnect UserModel)
        {
            using (CRUDwithNICProjectEntities1 db = new CRUDwithNICProjectEntities1())
            {
                var userdetail = db.DBConnects.Where(x => x.UserName == UserModel.UserName && x.Password == UserModel.Password).FirstOrDefault();
                if (userdetail == null)
                {
                  
                    return View("homepage", UserModel);
                }
                else 
                {
                    Session["Id"] = userdetail.Id;
                    return RedirectToAction("FirstPage","Home");
                }
            }
            //string cs = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;
            //using (SqlConnection con = new SqlConnection(cs))
            //{
            //    SqlCommand cmd = new SqlCommand("dbsignin", con);
            //    cmd.CommandType = CommandType.StoredProcedure;
                
                
            //    SqlParameter Userparam = new SqlParameter("@UserName",UserName);
            //    SqlParameter Passparam = new SqlParameter("@Password",Password);

            //    cmd.Parameters.Add(Userparam);
            //    cmd.Parameters.Add(Passparam);

            //    con.Open();
            //    int ReturnCode = (int)cmd.ExecuteScalar();
            //    return ReturnCode ==1;
            
            
            }
            // This Method is for taking only First row from database fro password and username sign in

            //string sql = "Select * from DBConnect "; 

            //SqlCommand cmd;

            //SqlDataReader dr;

            //SqlConnection cn = new SqlConnection(connectionString);

            //cn.Open();

            //cmd = new SqlCommand(sql,cn);

            //dr = cmd.ExecuteReader();

            //dr.Read();
            
            
            //    if (dr.GetString(1) == UserLogin.UserName && dr.GetString(2) == UserLogin.Password)
            //    {
            //        Session["UserID"] = dr.GetString(1);
            //        return RedirectToAction("FirstPage");
            //    }

            //    else
            //        return RedirectToAction("homepage");
            
        }



    
}
