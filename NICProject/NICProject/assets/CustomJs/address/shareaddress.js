var SAkendoGrid;
ShareAddressFile = function () {
    var isEditing = null;
    var addressTypes = null;
    var mainFormID = null;
    var isAddMode = false;
    var FormType_id = "";
    var FundRequestId = null;


    function AttachAddress() {

        var selectedValue = $("#AttachAddressTypeId :selected").text();
        if (!ValidateData(selectedValue)) {
            return;
        }

        var addressType = $('#AttachAddressTypeId').val();
        var addressId = $('#address_id').val();
        if ((ShareAddressFile.mainFormID > 0 && ShareAddressFile.mainFormID != null && ShareAddressFile.mainFormID != undefined) || (ShareAddressFile.FundRequestId > 0 && ShareAddressFile.FundRequestId != null && ShareAddressFile.FundRequestId != undefined)) {
            if (addressId != undefined && addressId > 0) {
                var request = {
                    AddressTypeId: addressType, AddressId: addressId, Main_id: ShareAddressFile.mainFormID, id: 0, FormType_id: ShareAddressFile.FormType_id, FundRequestId: FundRequestId
                }
                var result = CallController('/Address/AddressAttach', 'Post', request);
                if (typeof result != undefined && result.HttpCode != "200") {
                    ModelErrorMessage('#Message', '', result.Message);
                    return;
                } else {
                    //$('#AddressAttachModal').modal('hide');
                    closeNav('AddressAttachModal');
                    GetValues();
                    SuccessMessage('U', '');
                }
            } else {
                ModelErrorMessage('#Message', '', 'Must Select Address before Attachment.');
            }
        } else {
            ModelErrorMessage('#Message', '', 'Mandatory fields missing on Main Tab.');
        }
    }

    function GetValues() {
        GetInitialValues();
        var request = { ID: (ShareAddressFile.mainFormID > 0 ? ShareAddressFile.mainFormID : ShareAddressFile.FundRequestId), FormType: ShareAddressFile.FormType_id, FundRequestId: ShareAddressFile.FundRequestId, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual' };
        var result = CallController('/Address/AddressPartial', 'Get', request);
        ShareAddressFile.addressTypes = CallController('/Address/AddressPartialGetTypes', 'Get', null);
        //var existingGrid = $('#share-address-table').data('kendoGrid');
        
        if (!SAkendoGrid) {
            SAkendoGrid = $("#share-address-table").kendoGrid({
                dataSource: {
                    data: result,
                    pageSize: 10
                },
                //height: 950,
                filterable: true,
                sortable: true,
                pageable: {
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                    {
                        //field: "AddressTypeId",
                        template: "#= ShareAddressFile.formatAddressType(AddressTypeId) #",
                        //template: '#= Simple(OffDuty,OnDuty) #',
                        title: "Address Type",
                        width: 100
                    },
                {
                    field: "AddressLine1",
                    title: "Address Line 1",
                    width: 150
                }, {
                    field: "State",
                    title: "State",
                    width: 100
                }, {
                    field: "Country",
                    title: "Country",
                    width: 100
                }
                    , {
                        template: "<div class='action-tools'><a class='btn btn-success btn-xs btn-font btn-primary' onclick='ShareAddressFile.GetEditValues(#: id #,#: AddressId #);'><i class='fa fa-edit'></i></a>" +
                        "&nbsp;&nbsp;<a class='btn btn-danger btn-xs btn-font btn-Delete' onclick='ShareAddressFile.DeleteValues(#: id #);'><i class='fa fa-trash-o'></i></a></div>"
                    }]
            });
        } else {
            SAkendoGrid.data('kendoGrid').dataSource.data(result);
            SAkendoGrid.data('kendoGrid').refresh();
        }
        


    }

    function GetEditValues(Id, AddressId) {

        GetInitialValues();
        var request = { ID: Id, FormType: ShareAddressFile.FormType_id, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual' };
        var result = CallController('/Address/AddressPartialEdit', 'Get', request);
        $('#ShareAddressPartialModal #AddressId').val(result.AddressId);
        $('#ShareAddressPartialModal #AddressLine1').val(result.AddressLine1);
        $('#ShareAddressPartialModal #AddressLine2').val(result.AddressLine2);
        $('#ShareAddressPartialModal #City').val(result.City);
        $('#ShareAddressPartialModal #State').val(result.State);
        $('#ShareAddressPartialModal #PostCode').val(result.PostCode);
        $('#ShareAddressPartialModal #Country').val(result.Country);
        $('#ShareAddressPartialModal #id').val(result.id);
        $(' #ShareAddressPartialModal #AddressTypeId_Old').val(result.AddressTypeId);
        
        $("#ShareAddressPartialModal #AddressTypeId option").each(function () {
            if ($(this).val() == result.AddressTypeId) {
                $(this).attr('selected', 'selected');
                $('#ShareAddressPartialModal #select2-AddressTypeId-container').text($("#ShareAddressPartialModal #AddressTypeId :selected").text());
            }
        });
        ShowAddressFinder(false)
        //$('#AddressPartialModal').modal('show');
        openNav("ShareAddressPartialModal");

    }


    function ShowAddressFinder(flag) {
        if (flag) {
            $(".visible").hide();
            $("#ShareAddressPartialModal .txtMailingAddress").show();
            $('#ShareAddressPartialModal #showSA').prop('checked', false);
        }
        else {
            $(".visible").show();
            $("#ShareAddressPartialModal .txtMailingAddress").hide();
            $('#ShareAddressPartialModal #showSA').prop('checked', true);
        }
    }

    function ClearFields() {
        ShowAddressFinder(false);
        $("#ShareAddressPartialModal #address_fieldSA").val("");
        $('#ShareAddressPartialModal #AddressId').val('0');
        $('#ShareAddressPartialModal #AddressLine1').val('');
        $('#ShareAddressPartialModal #AddressLine2').val('');
        $('#ShareAddressPartialModal #City').val('');
        $('#ShareAddressPartialModal #State').val('');
        $('#ShareAddressPartialModal #PostCode').val('');
        $('#ShareAddressPartialModal #Country').val('');
        $('#ShareAddressPartialModal #id').val('0');
    }

    function GetInitialValues() {
        ClearFields();

        ShareAddressFile.FundRequestId = $('#FundRequestId').val();
        ShareAddressFile.mainFormID = $('#ShareId').val();
        ShareAddressFile.FormType_id = 'ShareHolder';

        // For Address Type Combo
        var resultType = CallController('/Address/AddressPartialGetTypes', 'Get', null);
        addressTypeSelect = document.querySelector("#ShareAddressPartialModal #AddressTypeId");
        // Previous
        $('#ShareAddressPartialModal #AddressTypeId').empty();
        $.each(resultType, function (i, item) {
            addressTypeSelect.options[addressTypeSelect.options.length] = new Option(item.Name, item.Id);
        });
        $('#ShareAddressPartialModal #AddressTypeId').selectpicker('refresh');

    }

    function GetPopupValues() {
        // For Address Type Combo
        var resultType = CallController('/Address/AddressPartialGetTypes', 'Get', null);
        addressTypeSelect = document.getElementById('AttachAddressTypeId');
        $('#AttachAddressTypeId').empty();
        console.log("Address Type Combo");
        console.log(resultType);
        $.each(resultType, function (i, item) {
            addressTypeSelect.options[addressTypeSelect.options.length] = new Option(item.Name, item.Id);
        });

        // For Address Combo
        var resultAddress = CallController('/Address/AddressGetAll', 'Get', null);
        addressSelect = document.getElementById('address_id');
        $('#address_id').empty();
        console.log("Address Combo");
        console.log(resultAddress);
        $.each(resultAddress, function (i, item) {
            addressSelect.options[addressSelect.options.length] = new Option(item.Name, item.Id);
        });

        //$("#AttachAddressTypeId").select2({ dropdownParent: $("#AddressAttachModal") });
        //$("#address_id").select2({ dropdownParent: $("#AddressAttachModal") });
    }


    // Format Address Type Combo
    function formatAddressType(id) {
        var value = null;
        for (var i = 0, iLen = ShareAddressFile.addressTypes.length; i < iLen; i++) {
            if (ShareAddressFile.addressTypes[i].Id == id) {
                value = ShareAddressFile.addressTypes[i].Name;
            }
        }
        return value;
    }

    function DeleteValues(Id) {
        bootbox.confirm({
            message: "Are you sure you want to delete?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var request = { ID: Id, FormType: ShareAddressFile.FormType_id };
                    CallController('/Address/AddressPartialDelete', 'Post', request);
                    SuccessMessage('D', '');
                    GetValues();
                }
                else
                    return;
            }
        });
    }

    function ValidateData(selectedValue) {
        // Validate Address Type if Exist
        var isExist = false;
        $('#share-address-table > tbody  > tr').each(function () {
            if (this.cells[0].innerHTML == selectedValue) {
                ModelErrorMessage('#Message', '', 'Address Type Already Exist.');
                isExist = false;
                return isExist;
            } else {
                isExist = true;
            }
        });
        return isExist;
    }

    function ValidateAndSave() {
        if ($('#frmShareAddress').valid()) {
            AddUpdate();
        }
    }

    function AddUpdate() {
        var selectedValue = $("#ShareAddressPartialModal #AddressTypeId :selected").text();
        var newValue = $('#ShareAddressPartialModal #AddressTypeId').val();
        var oldValue = $('#ShareAddressPartialModal #AddressTypeId_Old').val();
        //if (oldValue != newValue && !ValidateData(selectedValue)) {
        //    return;
        //}

        if ((ShareAddressFile.mainFormID > 0 && ShareAddressFile.mainFormID != null && ShareAddressFile.mainFormID != undefined) || (ShareAddressFile.FundRequestId > 0 && ShareAddressFile.FundRequestId != null && ShareAddressFile.FundRequestId != undefined)) {
            var request = {
                FundRequestId: ShareAddressFile.FundRequestId,
                AddressTypeId: $('#ShareAddressPartialModal #AddressTypeId').val(), AddressId: $('#ShareAddressPartialModal #AddressId').val(), AddressLine1: $('#ShareAddressPartialModal #AddressLine1').val(), AddressLine2: $('#ShareAddressPartialModal #AddressLine2').val(), City: $('#ShareAddressPartialModal #City').val(),
                State: $('#ShareAddressPartialModal #State').val(), PostCode: $('#ShareAddressPartialModal #PostCode').val(), Country: $('#ShareAddressPartialModal #Country').val(), id: $('#ShareAddressPartialModal #id').val(), Main_id: ShareAddressFile.mainFormID, FormType_id: ShareAddressFile.FormType_id
            }
            isAddMode = false;
            var result = CallController('/Address/AddressPartial', 'Post', request);
            if (typeof result != undefined && result.HttpCode != "200") {
                ModelErrorMessage('#ShareAddressPartialModal #Message', '', result.Message);
                return;
            } else {
                //$('#AddressPartialModal').modal('hide');
                closeNav('ShareAddressPartialModal');
                GetValues();
                SuccessMessage('U', '');
            }
        } else {
            ModelErrorMessage('#Message', '', 'Mandatory fields missing on Main Tab.');
        }
    }

    return {
        FormType_id: FormType_id, GetValues: GetValues,
        GetPopupValues: GetPopupValues, AttachAddress: AttachAddress,
        AddUpdate: AddUpdate, ValidateAndSave: ValidateAndSave, GetInitialValues: GetInitialValues, GetEditValues: GetEditValues,
        DeleteValues: DeleteValues, mainFormID: mainFormID, formatAddressType: formatAddressType, FundRequestId: FundRequestId
    }
}();



(function () {
    var widget, initAF = function () {
        widget = new AddressFinder.Widget(
            document.getElementById('address_fieldSA'),
            AddressFinderAccessKey,
            'AU',
            { show_locations: false }
        );

        widget.on("result:select", function (fullAddress, metaData) {
            $('#ShareAddressPartialModal #AddressLine1').val(metaData.address_line_1);
            $('#ShareAddressPartialModal #AddressLine2').val(metaData.address_line_2);
            $('#ShareAddressPartialModal #City').val(metaData.locality_name);
            $('#ShareAddressPartialModal #State').val(metaData.state_territory);
            $('#ShareAddressPartialModal #PostCode').val(metaData.postcode);
            $('#ShareAddressPartialModal #Country').val("Australia");
            $('.af_list').hide();
        });
    };

    $(document).ready(function () {
        $.getScript('https://api.addressfinder.io/assets/v3/widget.js', initAF);

        $("#ShareAddressPartialModal #showSA").click(function () {
            var val = $('#ShareAddressPartialModal #showSA').is(":checked");
            if (val == true) {
                $("#ShareAddressPartialModal .visible").show();
                $("#ShareAddressPartialModal .txtMailingAddress").hide();
            } else {
                $("#ShareAddressPartialModal .visible").hide();
                $("#ShareAddressPartialModal .txtMailingAddress").show();
            }
        });

    });

})();
