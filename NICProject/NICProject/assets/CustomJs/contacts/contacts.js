﻿var ContactGrid;
var UnAssignedClientGrid;
var ContactBaseGrid;
var contactTypes = null;
var roles = null;
var validationmessage = "";

// Format Contacts Type Combo
function formatContactsType(id) {
    var value = null;
    for (var i = 0, iLen = contactTypes.length; i < iLen; i++) {
        if (contactTypes[i].Id == id) {
            value = contactTypes[i].Name;
        }
    }
    return value;
}

// Format Roles Combo
function formatRoles(id) {
    var value = null;
    for (var i = 0, iLen = roles.length; i < iLen; i++) {
        if (roles[i].Id == id) {
            value = roles[i].Name;
        }
    }
    return value;
}


jsContact = function () {
    var isEditing = null;

    var EntityId = null;
    var isAddMode = false;
    var EntityType = "Fund";
    var FundRequestId = null;


    function AttachContact() {
        var contactType = $('#ContactTypeIdAttach').val();
        var contactId = $('#ContactIdAttach').val();
        var contactRoleId = $('#ContactRoleIdAttach').val();

        var id = $('#Id').val();
        if (jsContact.EntityId > 0 && jsContact.EntityId != null && jsContact.EntityId != undefined) {
            if (contactId != undefined && contactId > 0) {
                var request = {
                    ContactTypeId: contactType, ContactId: contactId, EntityId: jsContact.EntityId, EntityType: jsContact.EntityType, Id: id, RoleId: contactRoleId
                }
                var result = CallController('/Contact/UpdateAttachedContact', 'Post', request);
                if (typeof result != undefined && result.HttpCode != "200") {
                    ModelErrorMessage('#Message', '', result.Message);
                    return;
                } else {
                    //$('#AttachContactModal').modal('hide');
                    //closeNav('AttachContactModal');
                    GetValues();
                    SuccessMessage('U', '');
                }
            }
        } else {
            ModelErrorMessage('#Message', '', 'Mandatory fields missing on Main Tab.');
        }

    }

    //function GetName(FullName,type) {
    //    //type 1 = firstname , type = 2 lastname
    //    if (FullName && FullName.length > 0) {
    //        if(FullName.split(' ').length )
    //    }

    //}


    function CreateFirmUser(Email, FirstName, LastName, RoleId, Id, FirmUserId, ContactId) {

        //hidden fields to preload the fields which are mandatory in sct_users table
        $('#hf_Email').val(Email);
        $('#hf_FirstName').val(FirstName);
        $('#hf_LastName').val(LastName);
        $('#hf_RoleId').val(RoleId);
        $('#hf_Id').val(Id);
        $('#hf_ContactId').val(ContactId);

        // empty user and password fields and also enable user name textbox field
        $("#txtFirmUserName").val("");
        $("#txtFirmUserName").prop("readonly", false);
        $("#txtFirmPassword").val("");
        $("#txtFirmConfirmPassword").val("");
        //clear FirmUserId for Creating New User
        $("#hf_FirmUserId").val("");

        //edit user case
        if (FirmUserId != "null") {
            var request = {
                Id: FirmUserId
            };
            var result = CallController('/Settings/GetUserByID', 'Get', request);
            $("#hf_FirmUserId").val(FirmUserId);
            $("#txtFirmUserName").val(result.Username);
            $("#txtFirmUserName").prop("readonly", true);

        }

        openNav("FirmUser");
    }

    function AssignClients(FirstName, LastName, FirmUserId) {
        $('#hdn_selectedassigneeUserId').val(FirmUserId);
        $('#lblDisplauSelecteduser').html("Assign clients to " + FirstName + " " + LastName);
        openNav("AssignClientsToFirmUser");
        LoadUnAssignedClients(FirmUserId);
    }

    function ValidateAndSaveFirmUser() {
        if (IsValidFirmUser()) {

            // email field is not on the UI so username is email as well
            $('#hf_Email').val($('#txtFirmUserName').val());

            var model = {
                Username: $('#txtFirmUserName').val(),
                FirstName: $('#hf_FirstName').val(),
                LastName: $('#hf_LastName').val(),
                Email: $('#hf_Email').val(),
                Password: $('#txtFirmPassword').val(),
                //ConfirmPassword: $('#ConfirmPassword').val(),
                Roles: $('#hf_RoleId').val()
            }
            //var result = null;
            if ($("#hf_FirmUserId").val().length == 36) {
                model.Id = $("#hf_FirmUserId").val();
            }

            var request = { model: model, ContactId: $('#hf_ContactId').val() };

            var result = CallController('/Settings/AddUpdateFirmUser', 'Post', request);

            if (typeof result != undefined && !result.Succeeded) {
                ModelErrorMessage('#Message', '', result.Errors[0]);
                return;
            }
            else {
                SuccessMessage('A', '');
                closeNav("FirmUser");
                GetValues();
            }
        }
        else {
            ModelErrorMessage('#Message', '', validationmessage);
        }
    }

    function IsValidEmail(email) {
        var re = /\S+@\S+\.\S+/;
        return re.test(email);
    }

    function IsValidFirmUser() {
        validationmessage = "";
        var IsValid = true;
        if ($("#txtFirmUserName").val().length == 0) {
            validationmessage += "Email is required";
            IsValid = false;
        }
        else if (!IsValidEmail($("#txtFirmUserName").val())) {
            validationmessage += "Email address is invalid";
            IsValid = false;
        }
        else if ($("#txtFirmPassword").val().length == 0 && $("#hf_FirmUserId").val().length < 36) {
            validationmessage += "\nPassword is required";
            IsValid = false;
        }
        else if ($("#txtFirmConfirmPassword").val().length == 0 && $("#hf_FirmUserId").val().length < 36) {
            validationmessage += "\nConfirm Password is required";
            IsValid = false;
        }
        else if ($("#txtFirmPassword").val() != $("#txtFirmConfirmPassword").val()) {
            validationmessage += "\nPasswords do not match";
            IsValid = false;
        }

        else
            return IsValid;
    }

    function GetBaseContacts() {

        var result = CallController('/Contact/GetAllContactsAllValues', 'Get', null);

        if (!ContactBaseGrid) {

            ContactBaseGrid = $("#tblContacts").kendoGrid({
                dataSource: {
                    data: result,
                    pageSize: 20
                },
                height: 850,
                sortable: true,
                //filterable: true,
                filterable: {
                    operators: {
                        string: {
                            contains: "Contains",
                            doesnotcontain: "Does not contain",
                            eq: "Equal to",
                            neq: "Not equal to",
                            startswith: "Starts with",
                            endswith: "Ends with",
                            special: "Contains special characters",
                            isempty: "Is empty",
                            isnotempty: "Is not empty",
                            isnotnull: "Is not null",
                            isnull: "Is null",
                            neq: "Not equal"
                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gt: "Greater than",
                            gte: "Greater than or equal",
                            lt: "Less than",
                            lte: "Less than or equal",
                            isnotnull: "Is not null",
                            isnull: "Is null"
                        }
                    }
                },
                pageable: {
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [

                    {
                        field: "FullName",
                        title: "Full Name",
                        width: '*'
                    },
                    {
                        field: "firmnames",
                        title: "Firms",
                        width: '*'
                    }
                    ,
                    {
                        field: "havelogin",
                        title: "Has Login",
                        width: '*'
                    }
                    ,
                    {
                        width: 150,
                        template: "<div class='action-tools'><a class='btn btn-success btn-xs btn-font btn-primary' onclick=GetContactData(#= ContactId #)><i class='fa fa-edit'></i></a> " +
                            "<a class='btn btn-danger " + (IsSCManager ? "hidden" : "") + " btn-xs btn-font btn-Delete' onclick=DeleteContactData(#= ContactId #)><i class='fa fa-trash-o'></i></a></div>"
                    }]
            });
        }
        else {
            ContactBaseGrid.data('kendoGrid').dataSource.data(result);
            ContactBaseGrid.data('kendoGrid').refresh();
        }
    }

    function GetValues() {
        GetInitialValues();
        //$("#contacts-table > tbody:last").children().remove();
        //oTable.fnClearTable();
        var request = { EntityId: (jsContact.EntityId == null ? jsContact.FundRequestId : jsContact.EntityId), EntityType: jsContact.EntityType };
        var result = CallController('/Contact/AttachedContacts', 'Get', request);
        contactTypes = CallController('/Contact/GetAllContactTypes', 'Get', null);
        roles = CallController('/Contact/GetAllRoles?isfirm=' + jsContact.isfirm + '&issmsf=' + jsContact.issmsf + '&isclient=' + jsContact.isclient, 'Get', null);
        var icon = '';
        icon = '<i class="glyphicon glyphicon-ok"></i>';
        //$.each(result, function (i, item) {
        //    if (item.IsPrimary) {
        //        icon = '<i class="glyphicon glyphicon-ok"></i>';
        //    }
        //    else
        //        icon = '';
        //    var aiNew = oTable.fnAddData([
        //    CheckNull(item.Title) + '|' + CheckNull(item.Email) + '|' + CheckNull(item.Phone) + '|' + CheckNull(item.TFN) + '|' + item.DOB + '|' + item.Gender,
        //    formatContactsType(item.ContactTypeId), item.FullName, formatRoles(item.RoleId), icon,
        //    "<a href='#'  class='btn btn-info edit' onclick='jsContact.GetEditValues(" + item.Id + "," + item.ContactId + ");'><i class='fa fa-edit'></i>Edit</a> <a href='#' class='btn btn-danger delete' onclick='jsContact.DeleteValues(" + item.Id + ");'><i class='fa fa-trash-o'></i>Delete</a>"
        //    ]);

        //});
        if (!ContactGrid) {
            $("#contacts-table").html("");
            ContactGrid = $("#contacts-table").kendoGrid({
                dataSource: {
                    data: result,
                    pageSize: 10
                },
                //height: 950,
                sortable: true,
                filterable: true,
                pageable: {
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                    //{
                    //    field: "Title + '|' +Email+ '|' + Phone + '|' + TFN + '|' + DOB + '|' + Gender",
                    //    //field: "formatAddressType(AddressTypeId)",
                    //    title: "",
                    //    width: 350
                    //},
                    //{
                    //    template: "#=formatContactsType(ContactTypeId)#",
                    //    title: "Contact Type",
                    //    width: 100
                    //},
                    {
                        field: "FullName",
                        title: "Name",
                        width: '*'
                    },
                    {
                        template: "#=formatRoles(RoleId)#",
                        title: "Role",
                        width: 300
                    }
                    , {
                        width: 300,
                        template: "<a class='btn btn-success btn-xs btn-font btn-primary' onclick='jsContact.GetEditValues(#:Id #,#: ContactId #);'><i class='fa fa-edit'></i></a>" +
                        "&nbsp;<a class='btn btn-danger btn-xs btn-font btn-Delete' onclick='jsContact.DeleteValues(#: Id #);'><i class='fa fa-trash-o'></i></a>" +
                        (IsFirm ?
                            "&nbsp;<a class='btn btn-success btn-xs btn-font btn-primary' data-id='#:Id#' onclick='jsContact.CreateFirmUser(\"#:Email#\",\"#:FirstName#\",\"#:LastName#\",#:RoleId#,#:Id#,\"#:FirmUserId#\",#:Id#);'>" +
                            "<i class='glyphicon glyphicon-plus'></i><span>#if (!FirmUserId) {#Create#} else{#Edit#}# User</span> </a>" +
                            "&nbsp;<a class='btn btn-success btn-xs btn-font btn-primary' data-id='achr_#:Id#' onclick='jsContact.AssignClients(\"#:FirstName#\",\"#:LastName#\",\"#:FirmUserId#\");'>" +
                            "<i class='glyphicon glyphicon-plus'></i><span>Assign Client</span> </a>"
                            : "")
                    }]
            });
        }
        else {
            ContactGrid.data('kendoGrid').dataSource.data(result);
            ContactGrid.data('kendoGrid').refresh();
        }
    }

    function LoadUnAssignedClients(FirmUserId) {
        var request = { FirmId: $('#FirmId').val(), UserId: FirmUserId };
        var result = CallController('/Fund/GetUnAssignedFunds', 'Get', request);
        if (!UnAssignedClientGrid) {
            $("#assignclient-table").html("");
            UnAssignedClientGrid = $('#assignclient-table').kendoGrid({
                dataSource: {
                    data: result,
                    //pageSize: 11
                },
                height: 700,
                sortable: true,
                filterable: {
                    operators: {
                        string: {
                            contains: "Contains",
                            doesnotcontain: "Does not contain",
                            eq: "Equal to",
                            neq: "Not equal to",
                            startswith: "Starts with",
                            endswith: "Ends with",
                            special: "Contains special characters",
                            isempty: "Is empty",
                            isnotempty: "Is not empty",
                            isnotnull: "Is not null",
                            isnull: "Is null",
                            neq: "Not equal"
                        }
                    }
                },
                pageable: {
                    numeric: false,
                    previousNext: false,
                    messages: {
                        display: "<label for='total-clients'>Total Clients: {2}</label> "
                    }
                },
                columns: [
                   {
                       width: '60px',
                       template: "<input type='checkbox' #= HasAssigned ? \'checked=\"checked\"\' : '' # name='assignclient_checkbox' id=#:FundId # />"
                   },
                   {
                       field: "FundName",
                       title: "Cleint Name",
                       width: '140px'
                   },
                    {
                        field: "Name",
                        title: "Assign To",
                        width: "150px"
                    },
                ]
            });

        }
        else {

            UnAssignedClientGrid.data('kendoGrid').dataSource.read();
            UnAssignedClientGrid.data('kendoGrid').dataSource.sync();
            UnAssignedClientGrid.data('kendoGrid').dataSource.data(result);
            UnAssignedClientGrid.data('kendoGrid').refresh();
        }
    }


    function CheckNull(value) {
        return (value == null) ? "" : value;
    }
    // add event listener for opening and closing details
    $('#contacts-table tbody').on('click', 'td.details-control', function () {
        var table = $('#contacts-table').DataTable();
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });

    function format(d) {
        // `d` is the original data object for the row
        var values = d[0].split('|');
        if (values.length > 0) {
            var gender = values[5] == 0 ? 'Male' : 'Female';
            var date = new Date(parseInt(values[4].substr(6)));
            var formattedDate = ("0" + (date.getMonth() + 1)).slice(-2) + "-" +
                ("0" + date.getDate()).slice(-2) + "-" + date.getFullYear();

            return '<table class="table table-celled" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px; margin-bottom:20px;">' +
                '<tr>' +
                '<td>Title:</td>' +
                '<td>' + values[0] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Email:</td>' +
                '<td>' + values[1] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Phone:</td>' +
                '<td>' + values[2] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>TFN:</td>' +
                '<td>' + values[3] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>DOB:</td>' +
                '<td>' + formattedDate + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Gender:</td>' +
                '<td>' + gender + '</td>' +
                '</tr>' +
                '</table>';
        }
    }

    function GetContactValues(Id) {
        GetInitialValues();
        var request = { Id: Id };
        var result = CallController('/Contact/GetContact', 'Get', request);
        $('#DOB').val(result.DOB);
        $('#Email').val(result.Email);
        $('#FirstName').val(result.FirstName);
        $('#Gender').val(result.Gender);
        $('#LastName').val(result.LastName);
        $('#Phone').val(result.Phone);
        $('#TFN').val(result.TFN);
        $('#Title').val(result.Title);
        $('#IsPrimary').prop('checked', result.IsPrimary);
        $('#ContactId').val(result.ContactId);
        $('#BirthCity').val(result.BirthCity);
        $("#BirthState option").each(function () {
            if ($(this).val() == result.BirthState) {
                $(this).attr('selected', 'selected');
                $('#BirthState').selectpicker('val', result.BirthState);
            }
        });
        openNav("ContactModal");
    }

    function GetEditValues(Id, ContactsId) {
        GetInitialValues();
        var request = { Id: Id, EntityType: jsContact.EntityType };
        var result = CallController('/Contact/GetAttachedContactById', 'Get', request);
        $('#DOB').val(result.DOB);
        $('#Email').val(result.Email);
        $('#FirstName').val(result.FirstName);
        $('#Gender').val(result.Gender);
        $('#LastName').val(result.LastName);
        $('#Phone').val(result.Phone);
        $('#TFN').val(result.TFN);
        $('#Title').val(result.Title);
        //$('#RoleId').val(result.RoleId);
        $('#RoleId').selectpicker('val', result.RoleId);
        $('#IsPrimary').prop('checked', result.IsPrimary);
        $('#Id').val(result.Id);

        if (result.FirmUserId)
            $('#hf_FirmUserId').val(result.FirmUserId);

        $('#ContactId').val(result.ContactId);
        $('#BirthCity').val(result.BirthCity);

        if ($('#FundId')) {
            $('#FundId').val(result.EntityId);
        }
        else {
            $('#FundRequestId').val(result.FundRequestId);
        }
        $('#ContactTypeId').val(result.ContactTypeId);

        $("#ContactTypeId option").each(function () {
            if ($(this).val() == result.ContactTypeId) {
                $(this).attr('selected', 'selected');
                $('#ContactTypeId').selectpicker('val', result.ContactTypeId);
            }
        });

        $("#BirthState option").each(function () {
            if ($(this).val() == result.BirthState) {
                $(this).attr('selected', 'selected');
                $('#BirthState').selectpicker('val', result.BirthState);
            }
        });

        $("#RoleId option").each(function () {
            if ($(this).val() == result.RoleId) {
                $(this).attr('selected', 'selected');
                $('#RoleId').selectpicker('val', result.RoleId);
            }
        });
        //$('#ContactModal').modal('show');
        openNav("ContactModal");
    }

    function GetInitialValues() {
        $('#DOB').val('');
        $('#Email').val('');
        $('#FirstName').val('');
        $('#Gender').val();
        $('#LastName').val('');
        $('#Phone').val('');
        $('#TFN').val('');
        $('#IsPrimary').prop('checked', false);
        $('#Title').val('');
        $('#Id').val('0');
        $('#ContactId').val('0');

        BindCombo("#ContactTypeId", "/Contact/GetAllContactTypes");

        BindCombo("#RoleId", '/Contact/GetAllRoles?isfirm=' + jsContact.isfirm + '&issmsf=' + jsContact.issmsf + '&isclient=' + jsContact.isclient);
        BindCombo("#AdminRoleId", "/Settings/GetRoles");
        BindCombo("#ContactIdAttach", "/Contact/GetAllContacts");
        //$("#ContactTypeId").select2({ dropdownParent: $("#ContactModal") });
        //$("#RoleId").select2({ dropdownParent: $("#ContactModal") });
        //$("#Gender").select2({ dropdownParent: $("#ContactModal") });


        // Tab Initial Setting
        $('#contactmain').addClass('active');
        $('#address-contact-tab').removeClass('active');
        $('#contactaddress').removeClass('active');
        $('#addresss-main-tab').addClass('active');
        $('#contactmain2').addClass('active');

        $("#DOB").kendoDatePicker({
            format: "dd/MM/yyyy",
            animation: {
                open: {
                    effects: "zoom:in",
                    duration: 300
                }
            }
        });

        $("#txt_member_nominee_dateofbirth_row1").kendoDatePicker({
            format: "dd/MM/yyyy",
            animation: {
                open: {
                    effects: "zoom:in",
                    duration: 150
                }
            }
        });

        $("#txt_member_nominee_dateofbirth_row2").kendoDatePicker({
            format: "dd/MM/yyyy",
            animation: {
                open: {
                    effects: "zoom:in",
                    duration: 150
                }
            }
        });

        $("#txt_member_nominee_dateofbirth_row3").kendoDatePicker({
            format: "dd/MM/yyyy",
            animation: {
                open: {
                    effects: "zoom:in",
                    duration: 150
                }
            }
        });

        $("#txt_member_nominee_dateofbirth_row4").kendoDatePicker({
            format: "dd/MM/yyyy",
            animation: {
                open: {
                    effects: "zoom:in",
                    duration: 150
                }
            }
        });

        $('#ContactIdAttach').on('change', function () {

            var selected = $('#ContactIdAttach').val();
            if (selected > 0) {
                SetExistingValue(selected);
            }
        });
    }

    function SetExistingValue(selected) {
        var request = { id: selected };
        var result = CallController('/Contact/GetContact', 'Get', request);
        if (result != null) {
            $('#DOB').val(result.DOB);
            $('#Email').val(result.Email);
            $('#FirstName').val(result.FirstName);
            $('#Gender').val(result.Gender);
            $('#LastName').val(result.LastName);
            $('#Phone').val(result.Phone);
            $('#TFN').val(result.TFN);
            $('#Title').val(result.Title);

            $('#IsPrimary').prop('checked', result.IsPrimary);
            $('#Id').val(0);
            $('#ContactId').val(result.ContactId);
        }

    }
    function GetPopupValues() {
        // For Contacts Type Combo
        //BindCombo("#ContactTypeIdAttach", "/Contact/GetAllContactTypes");
        //$('#ContactTypeIdAttach').selectpicker('refresh');

        // For Contacts Combo
        BindCombo("#ContactIdAttach", "/Contact/GetAllContacts");
        //$('#ContactIdAttach').selectpicker('refresh');

        // For Contacts Combo
        //BindCombo("#ContactRoleIdAttach", "/Contact/GetAllRoles");
        //$('#ContactRoleIdAttach').selectpicker('refresh');

        //$("#ContactTypeIdAttach").select2({ dropdownParent: $("#AttachContactModal") });
        //$("#ContactIdAttach").select2({ dropdownParent: $("#AttachContactModal") });
        //$("#ContactRoleIdAttach").select2({ dropdownParent: $("#AttachContactModal") });
    }

    function BindCombo(id, url) {
        // For Contacts Type Combo
        var resultType = CallController(url, 'Get', null);
        $(id).empty();
        var options = "";
        // options = "<option value='-1'>None</option>"; // uncomment if you want this item
        $.each(resultType, function (a, b) {
            options += "<option value='" + b.Id + "'>" + b.Name + "</option>";
        });
        $(id).html(options);
        $(id).selectpicker('refresh');
    }

    function DeleteContact(Id) {
        bootbox.confirm({
            message: "Are you sure you want to delete?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var request = { ContactId: Id };
                    CallController('/Contact/DeleteContact', 'Post', request);
                    SuccessMessage('D', '');
                    GetBaseContacts();
                }
                else
                    return;
            }
        });
    }


    function DeleteValues(Id) {
        bootbox.confirm({
            message: "Are you sure you want to delete?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var request = { ID: Id, FormType: jsContact.EntityType };
                    CallController('/Contact/DeleteAttachedContact', 'Post', request);
                    SuccessMessage('D', '');
                    GetValues();
                }
                else
                    return;
            }
        });
    }

    function ValidateContact() {
        var frmContact = $('input[name="FirstName"]').closest("form");
        $.validator.unobtrusive.parse(frmContact);
        frmContact.validate();
        if (frmContact.valid()) {
            UpdateContact();
        }
    }

    function UpdateContact() {
        if ($('#ContactId').val() != undefined && $('#ContactId').val() > 0) {
            var request = {
                ContactId: $('#ContactId').val(), DOB: $('#DOB').val(), Email: $('#Email').val(), FirstName: $('#FirstName').val(),
                Gender: $('#Gender').val(), LastName: $('#LastName').val(), Phone: $('#Phone').val(), TFN: $('#TFN').val(), Title: $('#Title').val(), IsPrimary: $("#IsPrimary").is(':checked'),
                BirthState: $('#BirthState').val(), BirthCity: $('#BirthCity').val(), BirthCountry: $('#BirthCountry').val()
            }
            isAddMode = false;

            var result = CallController('/Contact/UpdateInfo', 'Post', request);
            if (result !== undefined && result.HttpCode != "200") {
                ModelErrorMessage('#Message', '', result.Message);
                return;
            } else {
                if (result.OperationStatuses.length > 0) {
                    var CurId = result.OperationStatuses[0].UniqueKey;
                    GetBaseContacts();
                    GetContactValues(CurId);
                    closeNav('ContactModal');
                }

                SuccessMessage('U', '');
            }

        } else {
            ModelErrorMessage('#Message', '', 'Mandatory fields missing on Main Tab.');
        }
    }

    function ValidateAndSave() {
        var frmContact = $('input[name="FirstName"]').closest("form");
        $.validator.unobtrusive.parse(frmContact);
        frmContact.validate();
        if (frmContact.valid()) {
            AddUpdate();
            closeNav('ContactModal');
        }
    }

    function SaveAssignClients() {
        displayLoading(this.body);
        var selectedClients = [];
        $.each($("input[name='assignclient_checkbox']:checked"), function () {
            selectedClients.push($(this).attr("id"));
        });
        if (selectedClients.length > 0) {
            var clientsJson = JSON.stringify(selectedClients);
            var request = {
                FirmId: $('#FirmId').val(),
                UserId: $('#hdn_selectedassigneeUserId').val(),
                SelectedCleitnIds: clientsJson
            }
            var response = CallController('/Fund/SaveClientsToUser', 'Post', request);
            if (typeof response != undefined && response.HttpCode == "200") {
                SuccessMessage('A', 'Selected client has been assigned successfully');
                closeNav('AssignClientsToFirmUser');
                //window.location = "../Fund/Recent";
            }
            else
                ErrorMessage('', result.Message);
            hideLoading(this.body);
        }
        else
            ErrorMessage('', "Select atleast one client");
    }

    function AddUpdate() {
        if ((jsContact.EntityId != null && jsContact.EntityId != undefined && jsContact.EntityId > 0) || (jsContact.FundRequestId != null && jsContact.FundRequestId != undefined && jsContact.FundRequestId > 0)) {
            console.log($('#BirthCountry').val());
            var request = {
                Id: $('#Id').val(), FundRequestId: jsContact.FundRequestId, EntityId: jsContact.EntityId, EntityType: jsContact.EntityType, ContactTypeId: $('#ContactTypeId').val(), ContactId: $('#ContactId').val(), RoleId: $('#RoleId').val(), DOB: $('#DOB').val(), Email: $('#Email').val(), FirstName: $('#FirstName').val(),
                Gender: $('#Gender').val(), LastName: $('#LastName').val(), Phone: $('#Phone').val(), TFN: $('#TFN').val(), Title: $('#Title').val(), IsPrimary: $("#IsPrimary").is(':checked'),
                BirthState: $('#BirthState').val(), BirthCity: $('#BirthCity').val(), BirthCountry: $('#BirthCountry').val(), FirmUserId: $('#hf_FirmUserId').val()
            }
            isAddMode = false;

            var result = CallController('/Contact/UpdateContact', 'Post', request);

            if (result !== undefined && result.HttpCode != "200") {
                ModelErrorMessage('#Message', '', result.Message);
                return;
            } else {
                GetValues();
                if (result.OperationStatuses.length > 0) {
                    var CurId = result.OperationStatuses[0].UniqueKey;
                    GetEditValues(CurId, 0);
                }

                //closeNav('ContactModal');
                //$('#ContactModal').modal('hide');
                SuccessMessage('U', '');
            }

        } else {
            ModelErrorMessage('#Message', '', 'Mandatory fields missing on Main Tab.');
        }
    }

    return {
        EntityType: EntityType, GetValues: GetValues,
        GetPopupValues: GetPopupValues, AttachContact: AttachContact,
        AddUpdate: AddUpdate, ValidateAndSave: ValidateAndSave, GetInitialValues: GetInitialValues, GetEditValues: GetEditValues,
        DeleteValues: DeleteValues, CreateFirmUser: CreateFirmUser, ValidateAndSaveFirmUser: ValidateAndSaveFirmUser,
        GetContactValues: GetContactValues, DeleteContact: DeleteContact, ValidateContact: ValidateContact, GetBaseContacts: GetBaseContacts, AssignClients: AssignClients, LoadUnAssignedClients: LoadUnAssignedClients, SaveAssignClients: SaveAssignClients
    }
}();