﻿var JobGrid;
var jobTypes = null;
var funds = null;

// Format Job Type Combo
function FormatJobType(id) {
    var value = null;
    for (var i = 0, iLen = jobTypes.length; i < iLen; i++) {
        if (jobTypes[i].Id == id) {
            value = jobTypes[i].Name;
        }
    }
    return value;
}

// Format Fund Combo
function FormatFunds(id) {
    var value = null;
    for (var i = 0, iLen = funds.length; i < iLen; i++) {
        if (funds[i].Id == id) {
            value = funds[i].Name;
        }
    }
    return value;
}


jsJobs = function () {
    var EntityId = 0;
    var isAddMode = false;
    var EntityType = "Jobs";
    //Jobs Datatable
    //var oTable = $('#jobs-table').dataTable({
    //    "paging": true,
    //    "filter": true,
    //    "pageLength": 25,
    //    "aoColumns": [
    //         null,
    //        null,
    //        null,
    //        null,
    //        null,
    //        { "bSortable": false }
    //    ]
    //});

    function GetValues() {
        //$("#jobs-table > tbody:last").children().remove();
        //oTable.fnClearTable();

        var request = { Fundid: $("#FundMainId").val() };
        var result = CallController('/Jobs/GetJobList', 'Get', request);
        jobTypes = CallController('/Jobs/TaskTemplates', 'Get', null);
        funds = CallController('/Jobs/Funds', 'Get', null);
        for (var i = 0; i < result.length; i++) {
            result[i].CreateDate = ParseDate(result[i].CreateDate);
            result[i].DueDate = ParseDate(result[i].DueDate);
        }
        //$.each(result, function (i, item) {
        //    //CheckNull(item.Title) + '|' + CheckNull(item.Email) + '|' + CheckNull(item.Phone) + '|' + CheckNull(item.TFN) + '|' + item.DOB + '|' + item.Gender
        //    var completeValue = '<span class="label label-danger">Incomplete</span>'

        //    if (item.IsCompleted == true) {
        //        completeValue = '<span class="label label-success">Completed</span>'
        //    }
        //    var aiNew = oTable.fnAddData([
        //    item.FirmName, FormatFunds(item.FundId), item.FinancialYear,
        //    FormatJobType(item.JobType), completeValue,
        //    " <a class='btn btn-primary' href='/Workflow/Recent/" + item.Id + "'><i class='fa fa-check'></i>Tasks</a> <a class='btn btn-success' href='/Task/Queries?id=" + item.Id + "&jq=1&cId=" +item.FundId + "'><i class='pe-7s-help1'></i>Queries</a> <a href='#'  class='btn btn-info edit' onclick='jsJobs.GetEditValues(" +item.Id + ");'><i class='fa fa-edit'></i>Edit</a> <a href='#' class='btn btn-danger delete' onclick='jsJobs.DeleteValues(" +item.Id + ");'><i class='fa fa-trash-o'></i>Delete</a>"
        //    ]);

        //});
        var columns = [];
        console.log(result);
        if (jsJobs.IsScUser) {
            var options = {
                navigatable: true,
                sortable: true,
                editable: true,
                save: function (e) {
                    console.log("saved", e, "dd " + e.model.car.rego, e.model.car.display);
                },
                //columns: getColumns(),
                //dataSource: vm.gridData,

            };
            var carList = new kendo.data.DataSource({
                data: [
                    new RegoDescriptionPair('C1', 'Car 1 - Red GMH sedan'),
                    new RegoDescriptionPair('C2', 'Car 2 - Blue Toyota'),
                    new RegoDescriptionPair('C3', 'Car 3 - Green Toyota'),
                    new RegoDescriptionPair('C4', 'Car 4 - Black Mazda'),]


            });

            columns = [
                //{ field: "CustomFunction", title: "Custom Function", editor: customFunctionEditor, template: "#=CustomFunction.Name#" },
                {
                    field: "ComboBoxtest",
                    editor: function (container, options) {
                        createCombo(container, options, carList);
                    },
                    width: 200
                },
                {
                    field: "FirmName",
                    title: "Firm Name",
                    width: 200
                },
                {
                    field: "FundName",
                    title: "Client Name",
                    width: 150
                }, {
                    field: "FinancialYear",
                    title: "Financial Year",
                    width: 120
                }, {
                    title: "Job Type",
                    field: "JobTypeName",
                    width: 150
                }, {
                    title: "Created Date",
                    format: "{0:dd/MM/yyyy HH:mm:ss}",
                    field: "CreateDate",
                    width: 150
                }, {
                    title: "Due Date",
                    format: "{0:dd/MM/yyyy HH:mm:ss}",
                    field: "DueDate",                    
                    width: 150
                },
                {
                    title: "Status",
                    field: "StatusName",
                    width: 150
                }
                , {
                    width: 300,
                    template: "<div class='action-tools'> <a class='btn btn-primary btn-xs' href='" + siteRoot + "/Workflow/Recent/#:Id#'><i class='fa fa-check'></i> </a>" +
                    "&nbsp;<a class='btn btn-success btn-xs' href='" + siteRoot + "/Task/Queries?id=#:Id# &jq=1&cId=#:FundId#'><i class='fa fa-info'></i></a>" +
                    "&nbsp;<a class='btn btn-info edit btn-xs' onclick='jsJobs.GetEditValues(#:Id#,#:FirmId#);'><i class='fa fa-edit'></i></a>" +
                    "&nbsp;<a class='btn btn-danger delete btn-xs' onclick='jsJobs.DeleteValues(#:Id#);'><i class='fa fa-trash-o'></i></a> </div>"
                }
            ];
        }
        else {
            columns = [
                {
                    field: "FirmName",
                    title: "Firm Name",
                    width: 200
                },
                {
                    field: "FundName",
                    title: "Client Name",
                    width: 150
                }, {
                    field: "FinancialYear",
                    title: "Financial Year",
                    width: 120
                }, {
                    title: "Job Type",
                    field: "JobTypeName",
                    width: 150
                }, {
                    title: "Created At",
                    format: "{0:dd/MM/yyyy HH:mm:ss}",
                    field: "CreateDate",
                    width: 150
                },
                {
                    title: "Status",
                    field: "StatusName",
                    width: 150
                }
            ];
        }
        if (!JobGrid) {
            JobGrid = $("#jobs-table").kendoGrid({
                dataSource: {
                    data: result,
                    schema: {
                        model: {
                            fields: {
                                CustomFunction: { defaultValue: { Value: "0", Name: "0" } },                               
                                CreateDate: { type: "date" },
                                DueDate: { type: "date" }
                            }
                        }
                    },
                    pageSize: 20
                },
                //height: 950,
                autoBind: false,
                sortable: true,
                filterable: true,
                groupable: true,
                pageable: {
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: columns
            });
        }
        else {
            JobGrid.data('kendoGrid').dataSource.data(result);
            JobGrid.data('kendoGrid').refresh();

        }




    }

    function createCombo(container, options, data) {
        var dataField = options.field.split('.');
        var fieldName = dataField[0];

        var input = $('<input/>')
        input.appendTo(container)
        input.kendoComboBox({
            autoBind: true,
            filter: "contains",
            placeholder: "select...",
            suggest: true,
            dataTextField: "display",
            dataValueField: "rego",
            dataSource: data,
            value: options.model[fieldName].rego,
            change: function (e) {
                var dataItem = this.dataItem();
                options.model[fieldName]['rego'] = dataItem.rego;
                options.model.set(fieldName + '.display', dataItem.display);
            }
        });
    }

    function customFunctionEditor(container, options) {
        $('<input id="CustomFunction" name="CustomFunction">')
            .appendTo(container)
            .kendoComboBox({
                dataTextField: "Name",
                dataValueField: "Value",
                change: onComboBoxChange,
                //       autoBind: abind,
                dataSource: [
                    { Name: "Add", Value: "1" },
                    { Name: "Subtract", Value: "2" },
                    { Name: "Multiply", Value: "3" },
                    { Name: "Divide", Value: "4" }
                ],

            }).appendTo(container);

    }


    function DisplayError(xhr) {
        var msg = JSON.parse(xhr.responseText);
        errornotification("Error", msg.Message);
        // alert(msg.Message);
    }



    function onComboBoxChange(e) {
        var combo = e.sender;

        var comboText = combo.text();
        // check if new value is a custom one
        if (!combo.dataItem()) {


            var dataSource = combo.dataSource;
            dataSource.add({
                Value: combo.text(),
                Name: combo.text()
            });

            dataSource.one("sync", function () {
                combo.select(dataSource.view().length - 1);
            });

            dataSource.sync();


        }

    }
    function CheckNull(value) {
        return (value == null) ? "" : value;
    }
    // add event listener for opening and closing details
    //$('#jobs-table tbody').on('click', 'td.details-control', function () {
    //    var table = $('#jobs-table').DataTable();
    //    var tr = $(this).closest('tr');
    //    var row = table.row(tr);
    //    if (row.child.isShown()) {
    //        // This row is already open - close it
    //        row.child.hide();
    //        tr.removeClass('shown');
    //    } else {
    //        // Open this row
    //        row.child(format(row.data())).show();
    //        tr.addClass('shown');
    //    }
    //});

    function format(d) {
        // `d` is the original data object for the row
        var values = d[0].split('|');
        if (values.length > 0) {
            var gender = values[5] == 0 ? 'Male' : 'Female';
            var date = new Date(parseInt(values[4].substr(6)));
            var formattedDate = ("0" + (date.getMonth() + 1)).slice(-2) + "-" +
                ("0" + date.getDate()).slice(-2) + "-" + date.getFullYear();

            return '<table class="table table-celled" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px; margin-bottom:20px;">' +
                '<tr>' +
                '<td>Title:</td>' +
                '<td>' + values[0] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Email:</td>' +
                '<td>' + values[1] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Phone:</td>' +
                '<td>' + values[2] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>TFN:</td>' +
                '<td>' + values[3] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>DOB:</td>' +
                '<td>' + formattedDate + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Gender:</td>' +
                '<td>' + gender + '</td>' +
                '</tr>' +
                '</table>';
        }
    }

    function GetEditValues(id, FirmId) {
        GetInitialValues(true);
        var request = { Id: id, EntityType: jsJobs.EntityType };
        var result = CallController('/Jobs/Show', 'Get', request);
        console.log(result);
        $('#Name').val(result.Name);
        $('#JobType').selectpicker('val', result.JobType);
        $('#FundId').selectpicker('val', result.FundId);
        $('#Id').val(result.Id);
        $('#FinancialYear').selectpicker('val', result.FinancialYear);
        $('#chkIsCompleted').prop('checked', result.IsCompleted);

        $('#CreateDate').val(result.CreateDate);
        $('#DueDate').val(result.DueDate);
        $('#ResponsibleManagerID').selectpicker('val', result.ResponsibleManagerID);
        $('#ReviewerID').selectpicker('val', result.ReviewerID);
        $('#AssignedUserID').selectpicker('val', result.AssignedUserID);
        $('#FirmId').selectpicker('val', FirmId);

        $('#ResponsibleManagerID').selectpicker('refresh');
        $('#ReviewerID').selectpicker('refresh');
        $('#AssignedUserID').selectpicker('refresh');

        //$("#JobType option").each(function () {
        //    if ($(this).val() == result.JobType) {
        //        $(this).attr('selected', 'selected');
        //        //$('#select2-JobType-container').text($("#JobType :selected").text());
        //    }
        //});

        //$("#FundId option").each(function () {
        //    if ($(this).val() == result.FundId) {
        //        $(this).attr('selected', 'selected');
        //        //$('#select2-FundId-container').text($("#FundId :selected").text());
        //    }
        //});

        //$("#FinancialYear option").each(function () {
        //    if ($(this).val() == result.FinancialYear) {
        //        $(this).attr('selected', 'selected');
        //        //$('#select2-FinancialYear-container').text($("#FinancialYear :selected").text());
        //    }
        //});
        //$('#JobModal').modal('show');
        openNav('JobModal');
    }

    function GetInitialValues(skipTemplates) {
        $('#Name').val('');
        $('#Id').val('0');
        $('#chkIsCompleted').prop('checked', false);

        $('#CreateDate').val('');
        $('#DueDate').val('');
        $('#ResponsibleManagerID').selectpicker('val', '');
        $('#ReviewerID').selectpicker('val', '');
        $('#AssignedUserID').selectpicker('val', '');

        $('#JobType').selectpicker('val', '');
        $('#FinancialYear').selectpicker('val', '');
        $('#FundId').selectpicker('val', '');
        $('#FirmId').selectpicker('val', '');


        BindCombo("#JobType", "/Jobs/TaskTemplates", null);
        BindCombo("#FundId", "/Jobs/Funds", null);
        BindCombo(".StatusItem", "/Jobs/GetJobStatuses", null);

        //$("#JobType").select2({ dropdownParent: $("#JobModal") });
        //$("#FundId").select2({ dropdownParent: $("#JobModal") });
        //$("#FinancialYear").select2({ dropdownParent: $("#JobModal") });

    }

    function BindCombo(id, url, request) {
        // For Contacts Type Combo
        var resultType = CallController(url, 'Get', request);
        $(id).empty();
        var options = "";
        // options = "<option value='-1'>None</option>"; // uncomment if you want this item
        $.each(resultType, function (a, b) {
            options += "<option value='" + b.Id + "'>" + b.Name + "</option>";
        });
        $(id).html(options);
        $(id).selectpicker('refresh');
    }


    function DeleteValues(id) {
        bootbox.confirm({
            message: "Are you sure you want to delete?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var request = { Id: id, EntityType: jsJobs.EntityType };
                    CallController('/Jobs/Delete', 'Post', request);
                    SuccessMessage('D', '');
                    GetValues();
                }
                else
                    return;
            }
        });
    }

    function ValidateAndSave() {
        var frmJobs = $('input[id="CreateDate"]').closest("form");
        $.validator.unobtrusive.parse(frmJobs);
        if (frmJobs.valid()) {
            AddUpdate();
        }
    }

    function AddUpdate() {
        var request = {
            Id: $('#Id').val(), FundId: $('#FundId').val(), EntityType: jsJobs.EntityType, JobType: $('#JobType').val(), Name: $('#Name').val(), FinancialYear: $('#FinancialYear').val(), IsCompleted: $('#chkIsCompleted').is(":checked"),
            CreateDate: $('#CreateDate').val(), DueDate: $('#DueDate').val(), ResponsibleManagerID: $('#ResponsibleManagerID').val(), ReviewerID: $('#ReviewerID').val(), AssignedUserID: $('#AssignedUserID').val()
        }
        isAddMode = false;

        var result = CallController('/Jobs/AddEdit', 'Post', request);
        if (result !== undefined && result.HttpCode != "200") {
            ModelErrorMessage('#Message', '', result.Message);
            return;
        } else {
            GetValues();
            //$('#JobModal').modal('hide');
            closeNav('JobModal');
            SuccessMessage('U', '');
        }
    }

    return {
        EntityType: EntityType, GetValues: GetValues,
        AddUpdate: AddUpdate, ValidateAndSave: ValidateAndSave, GetInitialValues: GetInitialValues, GetEditValues: GetEditValues,
        DeleteValues: DeleteValues, BindCombo: BindCombo
    }
}();
