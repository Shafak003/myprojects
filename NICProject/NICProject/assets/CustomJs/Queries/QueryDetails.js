﻿

function BindQueryListDataAdmin() {
    var result = CallController('/Task/GetQuesriesList', 'Get', 0);

    $('#Queries-list').kendoGrid({
        dataSource: {
            data: result,
            pageSize: 20
        },
        height: 850,
        sortable: true,
        //filterable: true,
        filterable: {
            operators: {
                string: {
                    contains: "Contains",
                    doesnotcontain: "Does not contain",
                    eq: "Equal to",
                    neq: "Not equal to",
                    startswith: "Starts with",
                    endswith: "Ends with",
                    special: "Contains special characters",
                    isempty: "Is empty",
                    isnotempty: "Is not empty",
                    isnotnull: "Is not null",
                    isnull: "Is null",
                    neq: "Not equal"
                },
                number: {
                    eq: "Equal to",
                    neq: "Not equal to",
                    gt: "Greater than",
                    gte: "Greater than or equal",
                    lt: "Less than",
                    lte: "Less than or equal",
                    isnotnull: "Is not null",
                    isnull: "Is null"
                }
            }
        },
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
        columns: [
            {
                field: "FirmName",
                title: "Firm Name",
                width: 200
            },
            {
                field: "ClientName",
                title: "Client Name",
                width: 200
            },
            
            {
                field: "JobYear",
                title: "FY",
                width: 80
            },
            {
                field: "JobTypeName",
                title: "Job Type",
                width: 150
            },
            {
                title: "Ready to Review",
                template: "<a href='Queries?Id=-1&StatusId=3&cId=#: FundId #&fy=#: JobYear#'>#: TLToReview #</a>",
                width: 100
            },
            {
                title: "Outstanding Queries",
                template: "<a href='Queries?Id=-1&StatusId=2&cId=#: FundId #&fy=#: JobYear#'>#: FirmToRespond #</a>",
                width: 120
            },
            {
                title: "Pending Resolution",
                template: "<a href='Queries?Id=-1&StatusId=1&cId=#: FundId #&fy=#: JobYear#'>#: SCToRespond #</a>",
                width: 120
            },

            {
                title: "All",
                template: "<a href='Queries?Id=-1&StatusId=0&cId=#: FundId #&fy=#: JobYear#'>All</a>",
                width: 30
            }
        ]
    });
}



function BindQueryListDataAviser() {
    var result = CallController('/Task/GetQuesriesList', 'Get', 0);

    $('#Queries-list').kendoGrid({
        dataSource: {
            data: result,
            pageSize: 20
        },
        height: 850,
        sortable: true,
        //filterable: true,
        filterable: {
            operators: {
                string: {
                    contains: "Contains",
                    doesnotcontain: "Does not contain",
                    eq: "Equal to",
                    neq: "Not equal to",
                    startswith: "Starts with",
                    endswith: "Ends with",
                    special: "Contains special characters",
                    isempty: "Is empty",
                    isnotempty: "Is not empty",
                    isnotnull: "Is not null",
                    isnull: "Is null",
                    neq: "Not equal"
                },
                number: {
                    eq: "Equal to",
                    neq: "Not equal to",
                    gt: "Greater than",
                    gte: "Greater than or equal",
                    lt: "Less than",
                    lte: "Less than or equal",
                    isnotnull: "Is not null",
                    isnull: "Is null"
                }
            }
        },
        pageable: {
            refresh: true,
            pageSizes: true,
            buttonCount: 5
        },
        columns: [
             {
                 field: "FirmName",
                 title: "Firm Name",
                 width: 200
             },
            {
                field: "ClientName",
                title: "Client Name",
                width: 200
            },
           
             {
                 field: "JobYear",
                 title: "FY",
                 width: 80
             },
            {
                field: "JobTypeName",
                title: "Job Type",
                width: 150
            },
            {
                title: "Outstanding Queries",
                template: "<a href='Queries?Id=-1&StatusId=2&cId=#: FundId #&fy=#: JobYear#'>#: FirmToRespond #</a>",
                width: 120
            },
            {
                title: "Pending Resolution",
                template: "<a href='Queries?Id=-1&StatusId=1&cId=#: FundId #&fy=#: JobYear#'>#: SCToRespond #</a>",
                width: 110
            },

            {
                title: "All",
                template: "<a href='Queries?Id=-1&StatusId=0&cId=#: FundId #&fy=#: JobYear#'>All</a>",
                width: 50
            }
        ]
    });
}