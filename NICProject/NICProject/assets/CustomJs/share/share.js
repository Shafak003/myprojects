var ShareGrid;
ShareFile = function () {
    var isEditing = null;
    var shareTypes = null;
    var mainFormID = null;
    var isAddMode = false;
    var FormType_id = "";
    var FundRequestId = null;
    
    function AttachShare() {
        debugger;
        var shareId = $('#share_id').val();
        if ((ShareFile.mainFormID > 0 && ShareFile.mainFormID != null && ShareFile.mainFormID != undefined) || (ShareFile.FundRequestId > 0 && ShareFile.FundRequestId != null && ShareFile.FundRequestId != undefined)) {
            if (shareId != undefined && shareId > 0) {
                var request = {
                    ShareId: shareId, MainId: mainFormID, FormType_id: ShareFile.FormType_id, FundRequestId: FundRequestId
                }
                var result = CallController('/Share/ShareAttach', 'Post', request);
                if (typeof result != undefined && result.HttpCode != "200") {
                    ModelErrorMessage('#Message', '', result.Message);
                    return;
                } else {
                    //$('#TrsuteeAttachModal').modal('hide');
                    closeNav('TrsuteeAttachModal');
                    GetValues();
                    SuccessMessage('U', '');
                }
            }
        }
        else {
            {
                ModelErrorMessage('#Message', '', 'Mandatory fields missing on Main Tab.');
            }
        }

    }

    function EnableCorportateInfo() {
        if ($("#sharemain #PartyType option:selected").text() == 'Individual' || $("#sharemain #PartyType option:selected").text() == '' || $("#sharemain #PartyType option:selected").text() == null) {
            $('#DivCorporate').hide();
            $('#share-contact-tab').hide();
            
        } else {
            $('#DivCorporate').show();
            $('#share-contact-tab').show();
        }
    }

    function GetValues() {
        GetInitialValues();

        $('#sharemain #PartyType').on('change', function () {

            EnableCorportateInfo();

        });
        
        if ($('#FundId')) {
            ShareFile.mainFormID = $('#FundId').val();
        }
        else {
            ShareFile.FundRequestId = $('#FundRequestId').val();
        }
        if (!ShareFile.FundRequestId) {
            ShareFile.FormType_id = $("#FundTypeId option:selected").text();
        }
        
        $("#SHCompanyName").hide();
        $("#IsCompany").click(function () {
            EnableCompanyName();
        });

        var request = {
            ID: ShareFile.mainFormID, FormType: ShareFile.FormType_id, FundRequestId: ShareFile.FundRequestId, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual'
        }
        var result = CallController('/Share/SharePartial', 'Get', request);
        var icon = '';
        icon = '<i class="glyphicon glyphicon-ok"></i>';
        
        if (!ShareGrid) {
            $("#share-table").html("");
            ShareGrid = $("#share-table").kendoGrid({
                dataSource: {
                    data: result,
                    pageSize: 10
                },
                //height: 950,
                filterable: true,
                sortable: true,
                pageable: {
                    refresh: true,
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                    {
                        field: "ShareName",
                        title: "Share Name",
                        width: '*',
                    }, {
                        field: "ACN",
                        title: "ACN",
                        width: 250
                    }, {
                        width: 250,
                        template: "<a class='btn btn-success btn-xs btn-font btn-primary' onclick='ShareFile.GetEditValues(#:ShareId#);'><i class='fa fa-edit'></i></a>"
                        + "<a class='btn btn-danger btn-xs btn-font btn-Delete' onclick='ShareFile.DeleteValues(#:ShareId#);'><i class='fa fa-trash-o'></i></a>"
                    }]
            });
        }
        else {
            ShareGrid.data('kendoGrid').dataSource.data(result);
            ShareGrid.data('kendoGrid').refresh();
        }


    }

    function EnableCompanyName()
    {
        var val = $('#IsCompany').is(":checked");
        if (val == true) {
            $("#SHCompanyName").show();
        } else {
            $("#SHCompanyName").hide();
        }
    }

    function GetCompanyName() {
        var request = { id: $('#ACN').val() }
        var result = CallController('/Share/GetCompanyName', 'Get', request);
        if (result != null) {
            if (result.Message != null) {
                $('#ShareName').val(result.Message);
            } else {
                $('#ShareName').val(result.Name);
            }
            
        }
    }

    function GetEditValues(ShareId) {

        GetInitialValues();
        var request = {
            ID: ShareId, FormType: ShareFile.FormType_id, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual'
        }
        var result = CallController('/Share/SharePartialEdit', 'Get', request);
        $('#ShareId').val(result.ShareId);
        $('#ShareName').val(result.ShareName);
        $('#sharemain #TFN').val(result.TFN);
        $('#ACN').val(result.ACN);
        $('#ABN').val(result.ABN);
        $('#ATF').val(result.ATF);
        $('#ATFName').val(result.ATFName);

        $('#ShareName').val(result.ShareName);
        $('#ShareClass').val(result.ShareClass);
        $('#NoOfShare').val(result.NoOfShare);
        $('#CompanyName').val(result.CompanyName);
        //$('#PartyType').val(result.PartyType);
        $('#ShareOnBehalf').val(result.ShareOnBehalf);
        $('#IsCompany').prop('checked', result.IsCompany);

        $("#PartyType option").each(function () {
            if ($(this).val() == result.PartyType) {
                $(this).attr('selected', 'selected');
                $("#PartyType").selectpicker('val', result.PartyType);
            }
        });

        EnableCompanyName();
        EnableCorportateInfo();
        openNav('SharePartialModal');
        //$('#SharePartialModal').modal('show');
    }

    function GetInitialValues() {

        $('#ShareId').val(0);
        $('#ShareName').val('');
        $('#sharemain #TFN').val('');
        $('#ACN').val('');
        $('#ABN').val('');
        $('#ATF').prop('checked', false);
        $('#ATFName').val('');
        $('#ShareName').val('');
        $('#ShareClass').val('');
        $('#NoOfShare').val(0);
        $('#CompanyName').val('');
        $('#IsCompany').prop('checked', false);
        $('#PartyType').val('');
        $('#ShareOnBehalf').val('');

        // Get All Share's
        //GetPopupValues();

        // Tab Initial Setting
        $('#sharemain').addClass('active');
        $('#share-contact-tab').removeClass('active');
        $('#share-address-tab').removeClass('active');
        $('#shareaddress').removeClass('active');
        $('#sharecontact').removeClass('active');
        $('#share-main-tab').addClass('active');

        $('#share_id').on('change', function () {

            var selected = $('#share_id').val();
            if (selected > 0) {
                SetExistingValue(selected);
            }
        });
        EnableCorportateInfo();
    }

    function SetExistingValue(selected) {
        var request = {
            ID: selected, FormType: ShareFile.FormType_id
        }
        var result = CallController('/Share/SharePartialEdit', 'Get', request);
        $('#ShareId').val(0);
        $('#ShareName').val(result.ShareName);
        $('#sharemain #TFN').val(result.TFN);
        $('#ACN').val(result.ACN);
        $('#ABN').val(result.ABN);
        $('#ATF').val(result.ATF);
        $('#ATFName').val(result.ATFName);

    }

    function GetPopupValues() {

        // For Share Combo
        var request = {
            ID: ShareFile.mainFormID, FormType: ShareFile.FormType_id, FundRequestId: ShareFile.FundRequestId, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual'
        }
        var resultShare = CallController('/Share/ShareGetAll', 'Get', request);
        shareSelect = document.getElementById('share_id');
        $('#share_id').empty();
        $.each(resultShare, function (i, item) {
            shareSelect.options[shareSelect.options.length] = new Option(item.ShareName, item.ShareId);
        });
        $('#share_id').selectpicker('refresh');
    }


    function DeleteValues(Id) {
        bootbox.confirm({
            message: "Are you sure you want to delete?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var request = {
                        ShareId: Id, FormType_id: ShareFile.FormType_id, MainId: mainFormID
                    }
                    CallController('/Share/SharePartialDelete', 'Post', request);
                    SuccessMessage('D', '');
                    GetValues();
                }
                else
                    return;
            }
        });
    }

    function ValidateAndSave() {
        var frmShare = $('input[name="ShareName"]').closest("form");
        $.validator.unobtrusive.parse(frmShare);
        //frmContact.validate();
        if (frmShare.validate()) {
            //if (frmShare.valid()) {
            AddUpdate();
        }
    }

    function AddUpdate() {
        debugger;
        if (!ShareFile.FundRequestId) {
            ShareFile.FormType_id = $("#FundTypeId option:selected").text();
        }
        if ((ShareFile.mainFormID > 0 && ShareFile.mainFormID != null && ShareFile.mainFormID != undefined) || (ShareFile.FundRequestId > 0 && ShareFile.FundRequestId != null && ShareFile.FundRequestId != undefined)) {
            var request = {
                ShareId: $('#ShareId').val(), ShareName: $('#ShareName').val(), TFN: $('#sharemain #TFN').val(), ACN: $('#ACN').val(), ABN: $('#ABN').val(), ATF: $('#ATF').is(":checked"), ATFName: $('#ATFName').val(),
                MainId: ShareFile.mainFormID, FormType_id: ShareFile.FormType_id, FundRequestId: ShareFile.FundRequestId, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual', ShareName : $('#ShareName').val(),
                ShareClass: $('#ShareClass').val(), NoOfShare: $('#NoOfShare').val(), CompanyName: $('#CompanyName').val(), IsCompany: $('#IsCompany').is(':checked'),
                PartyType : $('#PartyType').val(), ShareOnBehalf : $('#ShareOnBehalf').val()
            }
            isAddMode = false;
            var result = CallController('/Share/SharePartial', 'Post', request);
            if (typeof result != undefined && result.HttpCode != "200") {
                ModelErrorMessage('#Message', '', result.Message);
                return;
            } else {
                GetValues();
                if (result.OperationStatuses.length > 0) {
                    var CurId = result.OperationStatuses[0].UniqueKey;
                    GetEditValues(CurId);
                }
                //$('#SharePartialModal').modal('hide');
                //closeNav('SharePartialModal');
                SuccessMessage('U', '');
            }
        } else {
            ModelErrorMessage('#Message', '', 'Mandatory fields missing on Main Tab.');
        }
    }

    return {
        FormType_id: FormType_id, GetValues: GetValues,
        GetPopupValues: GetPopupValues, AttachShare: AttachShare,
        AddUpdate: AddUpdate, ValidateAndSave: ValidateAndSave, GetInitialValues: GetInitialValues, GetEditValues: GetEditValues,
        DeleteValues: DeleteValues, mainFormID: mainFormID, GetCompanyName: GetCompanyName
    }
}();