﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="ShoppingCart.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 168px;
        }
        .auto-style3 {
            height: 168px;
            width: 318px;
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="Label1" runat="server" style="font-weight: 700; font-size: xx-large" Text="Detail Of Product"></asp:Label>
        <br />
        This product is very useful for all</div>
        <asp:DataList ID="DataList1" runat="server" DataSourceID="SqlDataSource1">
            <ItemTemplate>
                <table class="auto-style1">
                    <tr>
                        <td colspan="2" style="text-align: center; background-color: #FFFF66">
                            <asp:Label ID="Label2" runat="server" style="font-size: x-large" Text='<%# Eval("[product name]", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style3">
                            <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("[product image]") %>' />
                        </td>
                        <td class="auto-style2">
                            <asp:Label ID="Label3" runat="server" style="text-align: left" Text='<%# Eval("[product description]", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: right; background-color: #66FF33">RS
                            <asp:Label ID="Label4" runat="server" Text='<%# Eval("price", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center; background-color: #FFCC99">
                            <asp:Label ID="Label5" runat="server" style="text-align: right" Text="Buy now"></asp:Label>
                        </td>
                    </tr>
                </table>
                <br />
                <br />
            </ItemTemplate>
        </asp:DataList>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:shoppingcartConnectionString7 %>" SelectCommand="SELECT * FROM [ShoppingCart_1] WHERE ([product id] = @product_id)">
            <SelectParameters>
                <asp:QueryStringParameter Name="product_id" QueryStringField="id" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
    </form>
</body>
</html>
