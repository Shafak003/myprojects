﻿var TaskGrid;
// Count of selected task followers
var TFCount = 0;

$("#StartDate").kendoDatePicker({
    animation: {
        open: {
            effects: "zoom:in",
            duration: 300
        }
    }
});

$("#DueDate").kendoDatePicker({
    animation: {
        open: {
            effects: "zoom:in",
            duration: 300
        }
    }
});
function GetTaskOptions(TaskId) {
    // For Task
    var request = {
        Id: TaskId, ViewType: 'Task'
    };
    var html = CallController('/TaskQuickView/TaskOptionView', 'Get', request, 'html');

    return html;
}

jsTask = function () {

    function GetPopupValues() {
        //Dropzone.forElement("#dropzoneForm").removeAllFiles(true);
        // For Contacts Type Combo
        BindCombo("#Type", "/Workflow/GetAllDocumentTypes");
        //$("#Type").select2({ dropdownParent: $("#TaskAddModal") });
        jsQuickView.BindTaskMainDocuments($('#TaskId').val(), "Task");
    }

    //var oTable = $('#task-list').dataTable({
    //    "paging": true,
    //    "filter": true,
    //    "pageLength": 25,
    //    "aoColumns": [
    //       null,
    //       null,
    //       null,
    //       null,
    //        { "bSortable": false }
    //    ]
    //});


    function BindTaskList() {

        //Table Schema
        //$("#task-list > tbody:last").children().remove();
        //oTable.fnClearTable();
        var request = null;
        var result = CallController('/Task/GetTaskList', 'Get', request);
        //$.each(result, function (i, item) {
        //    var aiNew = oTable.fnAddData([
        //     item.ClientName, item.JobYear, String(item.JobType), String(item.Name),
        //     GetTaskOptions(item.TaskId)

        //    ]);
        //});
        if (!TaskGrid) {
            TaskGrid = $("#task-list").kendoGrid({
                dataSource: {
                    data: result,
                    pageSize: 20
                },
                height: 800,
                sortable: true,
                //filterable: true,
                filterable: {
                    operators: {
                        string: {
                            contains: "Contains",
                            doesnotcontain: "Does not contain",
                            eq: "Equal to",
                            neq: "Not equal to",
                            startswith: "Starts with",
                            endswith: "Ends with",
                            special: "Contains special characters",
                            isempty: "Is empty",
                            isnotempty: "Is not empty",
                            isnotnull: "Is not null",
                            isnull: "Is null",
                            neq: "Not equal"
                        },
                        number: {
                            eq: "Equal to",
                            neq: "Not equal to",
                            gt: "Greater than",
                            gte: "Greater than or equal",
                            lt: "Less than",
                            lte: "Less than or equal",
                            isnotnull: "Is not null",
                            isnull: "Is null"
                        }
                    }
                },
                groupable: true,
                pageable: {
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                    {
                        field: "FirmName",
                        title: "Firm Name",

                    },
                    {
                        field: "ClientName",
                        title: "Client Name",
                    },
                    {
                        field: "JobYear",
                        title: "Financial Year",
                    },
                    {
                        field: "JobType",
                        title: "Job Type",
                    },
                    {
                        field: "Name",
                        title: "Task Name",
                    },
                    {
                        field: "ResponsibleManager",
                        title: "Client Manager",
                    },
                    {
                        field: "Reviewer",
                        title: "Team Leader",
                    },
                    {
                        field: "AssignedUser",
                        title: "Accountant",
                    },
                    {
                        width: 180,
                        template: "#= GetTaskOptions(TaskId) #"
                    }]
            });

        }
        else {
            TaskGrid.data('kendoGrid').dataSource.data(result);
            TaskGrid.data('kendoGrid').refresh();

        }
        setTimeout(function () {
            $("#task-list").css('table-layout', 'auto');
        }, 100);

    }

    function GetTaskOptions(TaskId) {
        // For Task
        var request = {
            Id: TaskId, ViewType: 'Task'
        };
        var html = CallController('/TaskQuickView/TaskOptionView', 'Get', request, 'html');

        return html;
    }

    function BindCombo(id, url) {
        // For Contacts Type Combo
        var resultType = CallController(url, 'Get', null);
        $(id).empty();
        var options = "";
        // options = "<option value='-1'>None</option>"; // uncomment if you want this item
        $.each(resultType, function (a, b) {
            options += "<option value='" + b.Id + "'>" + b.Name + "</option>";
            $(id).html(options);
        });
        $(id).selectpicker('refresh');
    }

    function ValidateAndSave() {
        var frmTask = $('input[name="Type"]').closest("form");
        $.validator.unobtrusive.parse(frmContact);
        if (frmTask.valid()) {

        }
    }

    return {
        GetPopupValues: GetPopupValues, ValidateAndSave: ValidateAndSave, BindTaskList: BindTaskList
    }
}();



// Permission Tab Script
jsPermission = function () {
    function GetPermissionInitialValues() {
        // For User Combo
        var resultType = CallController('/Workflow/GetAllUsers', 'Get', null);
        UserPermissionList = document.getElementById('UserPermissionList');
        $('#UserPermissionList').empty();
        $.each(resultType, function (i, item) {
            UserPermissionList.options[UserPermissionList.options.length] = new Option(item.Name, item.Id);
        });
        $('#UserPermissionList').selectpicker('refresh');

        //$("#UserPermissionList").select2({
        //    dropdownParent: $("#TaskAddModal"), placeholder: 'Select a User',
        //    allowClear: true
        //});
    }

    function GetEditValuesPermission() {
        GetPermissionInitialValues();
        var TaskId = $('#TaskId').val();

        if (TaskId > 0) {
            var request = { Id: TaskId };
            var result = CallController('/TaskPermission/GetPermissionByID', 'Get', request);
            if (result.UserIds[0] != null) {
                $('#UserPermissionList').selectpicker('val', result.UserIds);
                //$('#UserPermissionList').select2().select2('val', result.UserIds);
            }
            $('#IsPublic').prop('checked', result.IsPublic);
        }
        ComboSetting();
    }

    function ValidateAndSave(TaskId) {
        var request = {
            TaskId: TaskId,
            UserIds: $('#UserPermissionList').val(),
            Id: $('#PermissionId').val(),
            IsPublic: $('#IsPublic').is(":checked")
        };
        var result = CallController('/TaskPermission/AddEdit', 'Post', request);
        if (typeof result != undefined && result.HttpCode != "200") {
            ErrorMessage('', result.Message);
            return;
        }
    }

    function ComboSetting() {

        var value = $('#IsPublic').is(":checked")
        if (value) {
            $('#UserPermissionList').val(null).trigger("change");
            $("#UserPermissionList").prop("disabled", true);
        } else {
            $("#UserPermissionList").prop("disabled", false);
        }
    }

    return {
        GetEditValuesPermission: GetEditValuesPermission, ValidateAndSave: ValidateAndSave, ComboSetting: ComboSetting
    }
}();

// Comments Tab Script
jsQuickView = function () {

    function Questions() {
        $('.message-compose').toggleClass('collapsed');
    }
    function GetInitialValues(DocId, ViewType) {
        Initialization(DocId, ViewType);
    }
    function GetInitialValuesForQueries(DocId) {
        Initialization(DocId, 'Queries');
    }

    function Initialization(DocId, ViewType) {
        $('#DocId').val(DocId);
        PageRefreshMain(DocId, ViewType);
        PageRefreshComment(DocId, ViewType);
        PageRefreshDocument(DocId, ViewType);
        $("#hf_ViewType").val("ViewQuery");
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover({
            html: true,
            container: 'body',
            trigger: 'hover',
        });
        openNav("task-side-wrap");
        $('#ViewType').val(ViewType);

        //$('#QuickViewModal').modal('show');
    }

    function PageRefreshMain(DocId, ViewType) {
        // For Task
        var request = {
            Id: DocId, ViewType: ViewType
        };
        var result = CallController('/TaskQuickView/MainQuickView', 'Get', request, 'html');
        $('#MainView').html(result);
    }

    function PageRefreshComment(DocId, ViewType) {
        // For Comments
        var request = {
            Id: DocId, ViewType: ViewType
        };
        var result = CallController('/TaskQuickView/CommentsQuickView', 'Get', request, 'html');
        $('#QuickViewComments').html(result);
        if (ViewType == 'Queries') {
            $('#MainLabelComment').html('Response');
        }
        //PageRefreshCommentDocument($('#CommentId').val(), ViewType);
    }

    function PageRefreshDocument(DocId, ViewType) {
        // For Documents
        var request = {
            Id: DocId, ViewType: ViewType
        };
        var result = CallController('/TaskQuickView/DocumentsQuickView', 'Get', request, 'html');
        $('#QuickViewDocuments').html(result);
    }

    function BindEditQueryDocuments(DocId) {
        // For Documents
        var request = {
            Id: DocId, ViewType: 'Query'
        };
        var result = CallController('/TaskQuickView/DocumentsQuickView', 'Get', request, 'html');
        $('#dv_QueryEditDocuments').html(result);
    }

    function BindMainDocuments(DocId, ViewType) {
        // For Documents
        var request = {
            Id: DocId, ViewType: ViewType
        };
        var result = CallController('/TaskQuickView/DocumentsQuickView', 'Get', request, 'html');
        $('#div_Documents').html(result);
    }

    function BindTaskMainDocuments(DocId, ViewType) {
        // For Documents
        var request = {
            Id: DocId, ViewType: ViewType
        };
        var result = CallController('/TaskQuickView/DocumentsQuickView', 'Get', request, 'html');
        $('#div_TaskDocuments').html(result);
        $("#div_TaskDocuments #hf_ViewType").val("EditTask");

    }

    function PageRefreshCommentDocument(DocId, event) {
        // For Documents
        //Imgcommentdoc
        $('#Def' + DocId + ' #CommentsDocuments').html('');
        var ViewType = $('#ViewType').val();
        $('#Def' + DocId + ' #CommentsDocuments').html("");
        var request = {
            Id: DocId, ViewType: ViewType
        };
        var result = CallController('/TaskQuickView/CommentsDocumentsQuickView', 'Get', request, 'html');
        $('#Def' + DocId + ' #CommentsDocuments').html(result);
        //var className = $('#Def' + DocId + ' #CommentsDocuments').attr('class');
        //if (className == 'hide') {
        //    $('#Def' + DocId + ' #CommentsDocuments').removeClass('hide');
        //    $('#Def' + DocId + ' #CommentsDocuments').addClass('show');
        //} else {
        //    $('#Def' + DocId + ' #CommentsDocuments').removeClass('show');
        //    $('#Def' + DocId + ' #CommentsDocuments').addClass('hide');
        //}
    }

    function GetCommentsDocuments(DocId, event) {
        // For Documents
        debugger;
        var ViewType = $('#ViewType').val();
        var className = $('#CommentsAttachDocuments').attr('class');
        if (className == 'hide') {
            var request = {
                Id: DocId, ViewType: ViewType
            };
            var result = CallController('/TaskQuickView/CommentsDocumentsQuickView', 'Get', request, 'html');
            $('#CommentsAttachDocuments').html(result);

            $('#CommentsAttachDocuments').removeClass('hide');
            $('#CommentsAttachDocuments').addClass('show');
        } else {
            $('#CommentsAttachDocuments').removeClass('show');
            $('#CommentsAttachDocuments').addClass('hide');
        }
    }

    function DownloadFile(DocumentId) {
        var request = {
            Id: DocumentId
        };
        $.ajax(
            {
                type: 'Get',
                cache: false,
                async: false,
                url: '/TaskQuickView/DownloadFile',
                data: request,
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    alert(errorThrown);
                },
                success: function (returnValue) {
                    window.location = '/TaskQuickView/DownloadFile?Id=' + returnValue;
                }
            });

    }

    function DeleteDocument(RecordId, event) {
        bootbox.confirm({
            message: "Are you sure you want to delete?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var MainFormType = event.parentElement.parentElement.parentElement.id;
                    if (MainFormType == 'CommentsDocuments') {
                        var request = { Id: RecordId, FormType: $('#ViewType').val() };
                        CallController('/TaskQuickView/DeleteCommentDocument', 'Post', request);

                        jsQuickView.PageRefreshCommentDocument($('#CommentId').val(), $('#ViewType').val());
                    } else {
                        var taskid = $("#TaskId").val();
                        var viewtype = $('#ViewType').val();
                        if ($("#hf_ViewType").val() == "EditTask") {
                            viewtype = "Task";
                        }
                        else if ($("#hf_ViewType").val() == "EditQuery")
                            viewtype = "Queries";

                        var request = { Id: RecordId, FormType: viewtype };
                        CallController('/TaskQuickView/DeleteDocument', 'Post', request);
                        if ($("#hf_ViewType").val() == "EditQuery")
                            jsQuickView.BindMainDocuments($("#Id").val(), "Queries");
                        else if ($("#hf_ViewType").val() == "EditTask")
                            jsQuickView.BindTaskMainDocuments(taskid, "Task");
                        else
                            jsQuickView.PageRefreshDocument($('#DocId').val(), $('#ViewType').val());
                    }

                    ModelSuccessMessage('#Message', 'D', '');

                    setTimeout(function () {
                        if (!$("body").hasClass("modal-open")) {
                            $("body").addClass("modal-open")
                        }
                    }, 1000);

                }
                else {
                    setTimeout(function () {
                        if (!$("body").hasClass("modal-open")) {
                            $("body").addClass("modal-open")
                        }
                    }, 1000);

                    return;
                }
            }
        });
    }


    return {
        GetInitialValues: GetInitialValues, PageRefreshMain: PageRefreshMain, PageRefreshComment: PageRefreshComment,
        PageRefreshDocument: PageRefreshDocument, DownloadFile: DownloadFile, PageRefreshCommentDocument: PageRefreshCommentDocument, Questions: Questions,
        GetCommentsDocuments: GetCommentsDocuments, DeleteDocument: DeleteDocument, GetInitialValuesForQueries: GetInitialValuesForQueries,
        BindMainDocuments: BindMainDocuments, BindTaskMainDocuments: BindTaskMainDocuments
    }
}();

// Comments Tab Script
jsComments = function () {
    function GetCommentsInitialValues(TaskId, CommentId) {
        // create Editor from textarea HTML element with default set of tools

        var editor = $("#commentseditor").data("kendoEditor");
        if (editor == undefined) {
            $("#commentseditor").kendoEditor({
                resizable: {
                    content: true,
                    toolbar: true
                }
            });
        } else {
            editor.value('');
        }

        //var detailId = $('#DetailId').val() != undefined ? $('#DetailId').val() : 0
        var upload = $("#commentfiles").data("kendoUpload");
        if (upload == undefined) {
            $("#commentfiles").kendoUpload({
                async: {
                    saveUrl: "/TaskComment/UploadDocument",
                    removeUrl: "/TaskComment/RemoveDocument",
                    autoUpload: false
                },
                localization: {
                    dropFilesHere: "customDropFilesHere",
                },
                upload: function (e) {
                    e.data = { Id: $('#DetailId').val(), FormType: $('#ViewType').val() };
                }
            });
        } else {
            upload.clearAllFiles();
            upload.removeAllFiles();
        }

        $('#DocId').val(TaskId);
        $('#DetailId').val(CommentId);
        $('#CommentsAddModal').modal('show');
        $('#CommentsAttachDocuments').removeClass('show');
        $('#CommentsAttachDocuments').addClass('hide');
        //$('#DocumentAttach').hide();
        $('.docs').hide()



    }


    function GetEditValuesComments(TaskId, CommentId) {
        GetCommentsInitialValues(TaskId, CommentId);

        var request = { Id: CommentId, FormType: $('#ViewType').val() };
        var result = CallController('/TaskComment/GetCommentByID', 'Get', request);
        var editor = $("#commentseditor").data("kendoEditor");
        editor.value(result.CommentHTML);
        //$('#QuickViewModal').modal('hide');
        $('#CommentsAddModal').modal('show');
        //$('#CommentsAttachDocuments').show();
        jsQuickView.GetCommentsDocuments($('#DetailId').val(), this);
    }

    function DeleteValues(TaskId, CommentId) {
        bootbox.confirm({
            message: "Are you sure you want to delete?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var request = { Id: CommentId, FormType: $('#ViewType').val() };
                    CallController('/TaskComment/Delete', 'Post', request);
                    ModelSuccessMessage('#Message', 'D', '');
                    jsQuickView.PageRefreshComment($('#DocId').val(), $('#ViewType').val());
                }
                else
                    return;
            }
        });
    }

    function GetDocuments() {

        //$('#DocumentAttach').show();
        $('.docs').show()

    }

    function ValidateAndSave(TaskId) {
        // Get Comments Editor
        var editor = $("#commentseditor").data("kendoEditor");
        if (editor.value() == undefined || editor.value() == '') {
            ModelErrorMessage('#Message', '', 'Comment area is empty.');
            return false;
        }

        var upload = $("#query_comment_files").data("kendoUpload"),
        files = upload.getFiles();
        var docType = $('#ddlcommentDocumentTypeId').val();
        if (files.length > 0 && docType <= 0)
        {
            ModelErrorMessage('#Message', '', 'Please select document type to attach');
            return false;
        }

        if ($('#DocId').val() > 0 && editor.value() != '') {
            var request = {
                DocId: $('#DocId').val(),
                Comment: editor.value(),
                CommentHTML: editor.value(),
                FormType: $('#ViewType').val(),
                Id: $('#DetailId').val(),
                ShortName: window.location.href,
                UserName: $('#ddlcommentDocumentTypeId').val()
            };
            var result = CallController('/TaskComment/TempPostData', 'Post', request);
            //if (typeof result != undefined && result.HttpCode != "200") {
            //    ModelErrorMessage('#Message', '', result.Message);
            //    return;
            //} else {
            //    if (result.OperationStatuses.length > 0) {
            //        $('#DetailId').val(result.OperationStatuses[0].UniqueKey);
            //    }
            //    debugger;
            //    // Upload Files to the Server
            //    //var upload = $("#commentfiles").data("kendoUpload");
            //    //upload.upload();

            //    jsQuickView.PageRefreshComment($('#DocId').val(), $('#ViewType').val());
            //    //var className = $('#Def' + $('#CommentId').val() + ' #CommentsDocuments').attr('class');
            //    //if (className == 'show') {
            //    //    $('#Def' + $('#CommentId').val() + ' #CommentsDocuments').removeClass('show');
            //    //    $('#Def' + $('#CommentId').val() + ' #CommentsDocuments').addClass('hide');
            //    //}
            //    jsQuickView.PageRefreshCommentDocument($('#CommentId').val(), $('#ViewType').val());
            //    $('#CommentsAddModal').modal('hide');
            //    ModelSuccessMessage('#Message', 'U', '');
            //    if (window.location.pathname.toLowerCase().indexOf("/task/queries") !== -1)
            //        $("#NeedToRefreshQueryGrid").val("1");
            //}
        }
    }



    return {
        GetEditValuesComments: GetEditValuesComments, ValidateAndSave: ValidateAndSave, GetCommentsInitialValues: GetCommentsInitialValues,
        GetDocuments: GetDocuments, DeleteValues: DeleteValues
    }
}();

// Followers Tab Script
jsFollowers = function () {

    var taskFollowers = {};
    function GetFollowers() {
        return taskFollowers;
    }
    function GetFollowersInitialValues() {
        // For User Combo
        debugger;

        var resultType = CallController('/Workflow/GetAllUsers', 'Get', null);
        UserFollowersList = document.getElementById('UserFollowersList');
        $('#UserFollowersList').empty();
        $.each(resultType, function (i, item) {
            UserFollowersList.options[UserFollowersList.options.length] = new Option(item.Name, item.Id);
        });
        $('#UserFollowersList').selectpicker('refresh');

        $('#UserFollowersList').on('change', function () {
            if ($(this).val() != null) {

                //it means a user is added.
                if ($(this).val().length > TFCount) {
                    console.log('new user added, now count is :' + $(this).val().length);
                }
                    //this means a user is removed
                else {
                    console.log('user removed, now count is :' + $(this).val().length);

                }
                TFCount = $(this).val().length;
                //var a = $(this).val();
                //var b = $(this).find('option[value=' + a[a.length - 1] + ']').text();
                //AddFollower(a[a.length - 1], $(this).find('option[value=' + a[a.length - 1] + ']').text(), 1);
            }
            else {
                //it means there is no user selected.
                TFCount = 0;
                console.log('all users removed');
            }
        });
        //$("#UserFollowersList").select2({
        //    dropdownParent: $("#TaskAddModal"), placeholder: 'Select a User',
        //    allowClear: true
        //});

        //$("#UserFollowersList").on("select2:select", function (e) { AddList("select2:select", e); });
        //$("#UserFollowersList").on("select2:unselect", function (e) { RemoveList("select2:unselect", e); });


        taskFollowers = {};
    }

    function AddList(name, evt) {
        if (!evt) {
            var args = "{}";
        } else {
            var userId = evt.params.data.id;
            var userName = evt.params.data.text;
            AddFollower(userId, userName, 1);
        }
    }

    function AddFollower(userId, userName, notificationType) {
        var AllChecked = '';
        var StatusChecked = '';
        var CommentsChecked = '';

        if (notificationType == 1) {
            AllChecked = 'checked=""';
        }
        else if (notificationType == 2) {
            StatusChecked = 'checked=""';
        }
        else {
            CommentsChecked = 'checked=""';
        }
        var InnerHTML = '<div class="task-followers" id="User' + userId + '">' +
                                    '<a href="#" class="option-control editTask" onclick="jsFollowers.RemoveUser(\'' + userId + '\')"> ' +
                                        '<i class="fa fa-trash-o fa-lg" aria-hidden="true"></i> ' +
                                    '</a> ' +
                                    '<a class="personInvolved"> ' +
                                        '<span class="followers-short-name" src="">' + GetShortName(userName) + '</span> ' +
                                        '<span class="personName">' + userName + '</span> ' +
                                    '</a> ' +
                                    '<div id="OptionList' + userId + '" class="task-followers-options left"> ' +
                                        '<label class="radio-inline"> ' +
                                            '<input class="radio-info" name="notificationType' + userId + '" onclick="jsFollowers.UpdateNotificationType(\'' + userId + '\',\'1\')" type="radio" ' + AllChecked + ' value="1"> ' +
                                            '<span class="text">All</span> ' +
                                        '</label> ' +
                                        '<label class="radio-inline"> ' +
                                            '<input class="radio-success" name="notificationType' + userId + '" onclick="jsFollowers.UpdateNotificationType(\'' + userId + '\',\'2\')" type="radio" ' + StatusChecked + ' value="2"> ' +
                                            '<span class="text">Status Changes</span> ' +
                                        '</label> ' +
                                        '<label class="radio-inline"> ' +
                                            '<input class="radio-warning" name="notificationType' + userId + '" onclick="jsFollowers.UpdateNotificationType(\'' + userId + '\',\'3\')" type="radio" ' + CommentsChecked + ' value="3"> ' +
                                            '<span class="text">Comments</span> ' +
                                        '</label> ' +
                                    '</div> ' +
                                '</div> ';
        if ($('#User' + userId).length == 0) {
            $('#UserContainer').append(InnerHTML);
            //var notificationType = $("input[name='notificationType" + userId + "']:checked").val();
            taskFollowers[userId] = notificationType;
        }
    }

    function UpdateNotificationType(userId, typeId) {
        //var notificationType = $("input[name='notificationType" + userId + "']:checked").val();
        taskFollowers[userId] = typeId;
    }
    function RemoveList(name, evt) {
        if (!evt) {
            var args = "{}";
        } else {
            $('#User' + evt.params.data.id).remove();
        }
    }

    function RemoveUser(id) {
        $('#User' + id).remove();
        delete taskFollowers[id];
        //now unselect value from dropdown
        var $select = $('#UserFollowersList');
        var idToRemove = id;

        var values = $select.val();
        if (values) {
            var i = values.indexOf(idToRemove);
            if (i >= 0) {
                values.splice(i, 1);
                $select.val(values).change();
            }
        }
    }

    function GetEditValuesFollowers() {
        GetFollowersInitialValues();
        var TaskId = $('#TaskId').val();
        if (TaskId > 0) {
            var request = { Id: TaskId };
            var result = CallController('/TaskFollowers/GetFollowersByID', 'Get', request);
            if (result.UserIds != null) {
                // $('#UserFollowersList').select2().select2('val', result.UserIds);
                $('#UserFollowersList').selectpicker('val', result.UserIds);
            }
            if (result.Followers != null) {
                taskFollowers = result.Followers;
                $.each(taskFollowers, function (index, value) {
                    var userName = $("#UserFollowersList option[value='" + index + "']").text();
                    AddFollower(index, userName, value)
                });
            }
        }
    }

    function ValidateAndSave(TaskId) {
        var request = {
            TaskId: TaskId,
            UserIds: $('#UserFollowersList').val(),
            Id: $('#FollowersId').val(),
            Followers: taskFollowers
        };
        var result = CallController('/TaskFollowers/AddEdit', 'Post', request);
        if (typeof result != undefined && result.HttpCode != "200") {
            ErrorMessage('', result.Message);
            return;
        }
    }

    return {
        GetEditValuesFollowers: GetEditValuesFollowers, ValidateAndSave: ValidateAndSave, RemoveUser: RemoveUser, UpdateNotificationType: UpdateNotificationType, GetFollowers: GetFollowers
    }
}();
// For Task Template Panel and Notifications
jsNotification = function () {


    function SetPermissionUsers(TaskId, forelement) {
        if (TaskId > 0) {
            var request = { TaskId: TaskId };
            var result = CallController('/Workflow/GetPermissionUsersByID', 'Get', request);
            var element = $('#' + forelement.parentElement.id + ' #' + forelement.id);
            if (result.length > 0) {
                // Set Data Content With Specific Response
                var InnerHTML = '';
                for (var i = 0; i < result.length; i++) {
                    InnerHTML += '<span class="viewpanel-short-name" src="">' + GetShortName(result[i].Text) + '</span> ' +
                            '<span class="personName small-text pull-left-xs">' + result[i].Text + '</span> <br /> <br />';
                }

                element.attr('data-content', InnerHTML);
            } else {
                element.attr('data-content', 'Everybody');
            }
        }
    }

    function SetTaskPriority(TaskId, forelement) {
        if (TaskId > 0) {
            var request = { TaskId: TaskId };
            var result = CallController('/Workflow/GetTaskPriorityByID', 'Get', request);
            if (result.length > 0) {
                var element = $('#' + forelement.parentElement.id + ' #' + forelement.id);
                // Set Data Content With Specific Response
                var InnerHTML = '';
                for (var i = 0; i < result.length; i++) {
                    InnerHTML += result[i].Text
                    break;
                }
                element.attr('data-original-title', InnerHTML);
            }
        }
    }

    function SetFollowers(TaskId, forelement) {
        if (TaskId > 0) {
            var request = { TaskId: TaskId };
            var result = CallController('/Workflow/GetTaskFollowersByID', 'Get', request);
            var element = $('#' + forelement.parentElement.id + ' #' + forelement.id);
            if (result.length > 0) {
                // Set Data Content With Specific Response
                var InnerHTML = '';
                for (var i = 0; i < result.length; i++) {
                    InnerHTML += '<span class="viewpanel-short-name" src="">' + GetShortName(result[i].Text) + '</span> ' +
                            '<span class="personName">' + result[i].Text + '</span> <br /> <br />';
                }

                element.attr('data-content', InnerHTML);
            } else {
                element.attr('data-content', 'Nobody');
            }
        }
    }

    return {
        SetPermissionUsers: SetPermissionUsers, SetTaskPriority: SetTaskPriority, SetFollowers: SetFollowers
    }
}();

function GetDocuments(parametrs) {
    if (typeof parametrs == 'object') {
        if (typeof parametrs.type == 'undefined')
            parametrs.type = typeViews;
    }
    var request = parametrs
    var result = CallController('/WorkFlow/GetDocuments', 'Get', request, 'html');
    $('#dvDocuments').html(result);

    return;
}

//// Task Queries Script
//jsQueries = function () {
//    function ValidateAndSave() {
//        debugger;
//        var frmMain = $('input[name="Title"]').closest("form");
//        $.validator.unobtrusive.parse(frmMain);
//        // frmMain.validate();
//        if (frmMain.valid()) {
//            SaveChanges();
//        }
//    }
//    function SaveChanges() {

//        var model = {
//            Title: $('#Title').val(),
//            Details: $('#Details').val(),
//        }

//        var url = '@Url.Action("AddEditQuery","Task")';
//        var result = CallController('/Task/AddEditQuery', 'POST', model);
//        var upload = $("#queryDocs").data("kendoUpload");
//        upload.upload();
//        SuccessMessage('U', '');
//    }

//    return {
//        ValidateAndSave: ValidateAndSave
//    }
//}();