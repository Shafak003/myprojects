(function() {
    'use strict';

    angular
        .module('app')
        .controller('FundsPanelController', FundsPanelController);

    FundsPanelController.$inject = ['$window'];

    function FundsPanelController($window) {
        $window.initialize = initializeMap;

        function initializeMap() {
            if (typeof google.maps.LatLng == 'function') {
                yimaPage.init();
            }
        }

        initializeMap();
    }
}());