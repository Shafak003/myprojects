﻿/*!

 =========================================================
 * Material Bootstrap Wizard - v1.0.2
 =========================================================
 
 * Product Page: https://www.creative-tim.com/product/material-bootstrap-wizard
 * Copyright 2017 Creative Tim (http://www.creative-tim.com)
 * Licensed under MIT (https://github.com/creativetimofficial/material-bootstrap-wizard/blob/master/LICENSE.md)
 
 =========================================================
 
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 */

// Material Bootstrap Wizard Functions

var searchVisible = 0;
var transparent = true;
var mobile_device = false;

$(document).ready(function () {

    $.material.init();

    /*  Activate the tooltips      */
    $('[rel="tooltip"]').tooltip();

    // Code for the Validator
    var $validator = $('.wizard-card form').validate({
        rules: {
            firstname: {
                required: true,
                minlength: 3
            },
            lastname: {
                required: true,
                minlength: 3
            },
            email: {
                required: true,
                minlength: 3,
            }
        },

        errorPlacement: function (error, element) {
            $(element).parent('div').addClass('has-error');
        }


    });

    // Wizard Initialization
    $('.wizard-card').bootstrapWizard({
        'tabClass': 'nav nav-pills',
        'nextSelector': '.btn-next',
        'previousSelector': '.btn-previous',

        onNext: function (tab, navigation, index) {
            var $valid = $('.wizard-card form').valid();
            if (index == 1) {
                var frmMain = $('input[name="FundNumber"]').closest("form");
                $.validator.unobtrusive.parse(frmMain);
                //frmMain.validate();
                //if (frmMain.valid()) {
                //    //SaveChanges();
                //} else {
                //    $validator.focusInvalid();
                //    return false;
                //}
            }
            else if (index == 2)
            { }
                //jsContact.GetValues();
            else if (index == 3) {
                //validateTrusteeCheckBox();
                //// Validate Information 
                //var Count = ContactGrid.data('kendoGrid').dataSource.total();
                //if ($("#FundTypeId option:selected").text() == 'SMSF - Individual') {
                //    if (Count != undefined && Count < 2) {
                //        ErrorMessage('', 'Minimum 2 Contact required.');
                //        $validator.focusInvalid();
                //        return false;
                //    }
                //}

                ////fund id 1 or 2 means it is SMSF Setup screen
                //if ($("#FundTypeId").val() == 1 || $("#FundTypeId").val() == 2) {

                //    TrusteeFile.GetValues();
                //}

                //else // other wise it is client screen
                //{
                //    ShareFile.GetValues();
                //}
            }

            else if (index == 4) {

                //var Count = TruesteeGrid.data('kendoGrid').dataSource.total();
                //// Validate Information
                //if ($("#FundTypeId option:selected").text() == 'SMSF - Individual') {
                //    if (Count != undefined && Count < 2) {
                //        ErrorMessage('', 'Minimum 2 trustees required.');
                //        $validator.focusInvalid();
                //        return false;
                //    }
                //} else {
                //    if (Count != undefined && Count < 1) {
                //        ErrorMessage('', 'Minimum 1 trustee required.');
                //        $validator.focusInvalid();
                //        return false;
                //    }
                //}
                $("#m-integration").click();
            }
            else if (index == 5) {
            }


            if (!$valid) {
                if (!(index == 1 || index == 2 || index == 3 || index == 4)) {
                    $validator.focusInvalid();
                    return false;
                }
            }
        },

        onInit: function (tab, navigation, index) {
            //check number of tabs and fill the entire row
            var $total = navigation.find('li').length;
            var $wizard = navigation.closest('.wizard-card');

            $first_li = navigation.find('li:first-child a').html();
            $moving_div = $('<div class="moving-tab">' + $first_li + '</div>');
            $('.wizard-card .wizard-navigation').append($moving_div);

            refreshAnimation($wizard, index);

            $('.moving-tab').css('transition', 'transform 0s');
        },

        onTabClick: function (tab, navigation, index) {
            //return false;
            var $valid = $('.wizard-card form').valid();
            if (index == 0) {
                var frmMain = $('input[name="FundNumber"]').closest("form");
                $.validator.unobtrusive.parse(frmMain);
                frmMain.validate();
                if (frmMain.valid()) {
                    SaveChanges();
                } else {
                    $validator.focusInvalid();
                    return false;
                }
            }
            validateTrusteeCheckBox();
            if (!$valid) {
                if (!(index == 1 || index == 2 || index == 3 || index == 4)) {
                    return false;
                }
            } else {
                return true;
            }
        },

        onTabShow: function (tab, navigation, index) {

            var $total = $('#FundNavigation .display').length;//navigation.find('li').length;
            var $current = index + 1;

            var $wizard = navigation.closest('.wizard-card');

            // If it's the last tab then hide the last button and show the finish instead
            if ($current >= $total) {
                $($wizard).find('.btn-next').hide();
                $($wizard).find('.btn-finish').show();
            } else {
                $($wizard).find('.btn-next').show();
                $($wizard).find('.btn-finish').hide();
            }

            //todo the issue - new code
            debugger;
            var liList = navigation.find('li');
            for (var i = $current; i <= liList.length; i++) {
                var li = navigation.find('li:nth-child(' + i + ')');
                var a = li.find('a');

                if ($(a).hasClass('display')) {

                    button_text = $(a).html();

                    $current = i;
                    //$('.wizard-card').find('div:child(' + $current + ')').addClass('');
                    break;
                }
            }

            //todo old code
            //button_text = navigation.find('li:nth-child(' + $current + ')').html();

            setTimeout(function () {
                debugger;
                $('.moving-tab').text(button_text);
            }, 150);

            var checkbox = $('.footer-checkbox');

            if (!index == 0) {
                $(checkbox).css({
                    'opacity': '0',
                    'visibility': 'hidden',
                    'position': 'absolute'
                });
            } else {
                $(checkbox).css({
                    'opacity': '1',
                    'visibility': 'visible'
                });
            }

            refreshAnimation($wizard, index);
        }
    });

    $('.btn-finish').click(function () {
        //SaveChanges();
        //if (FinishChanges) {
        //    alert("Will call finishchange method now");
        //    FinishChanges();
        //}
        if ($("#FundTypeId option:selected").text() != 'SMSF - Individual') {
            SaveEstbTrusteeData();
        }
        window.location.href = "../Mail/Inbox";//clientfinishUrl;
    });

    // Prepare the preview for profile picture
    $("#wizard-picture").change(function () {
        readURL(this);
    });

    $('[data-toggle="wizard-radio"]').click(function () {
        wizard = $(this).closest('.wizard-card');
        wizard.find('[data-toggle="wizard-radio"]').removeClass('active');
        $(this).addClass('active');
        $(wizard).find('[type="radio"]').removeAttr('checked');
        $(this).find('[type="radio"]').attr('checked', 'true');
    });

    $('[data-toggle="wizard-checkbox"]').click(function () {
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
            $(this).find('[type="checkbox"]').removeAttr('checked');
        } else {
            $(this).addClass('active');
            $(this).find('[type="checkbox"]').attr('checked', 'true');
        }
    });

    $('.set-full-height').css('height', 'auto');

});

function validateTrusteeCheckBox() {
    if ($("#FundTypeId option:selected").text() == 'SMSF - Individual') {
        $("#rdoesttrustee_individual").attr("disabled", true);
        $("#rdoesttrustee_corportate").attr("disabled", true);
    }
    else {
        $("#rdoesttrustee_individual").attr("disabled", true);
        $("#rdoesttrustee_corportate").attr("disabled", false);
        $("#rdoesttrustee_corportate").attr("checked", true);
        $('#dvtrusteebody').show();
    }
}
//Function to show image before upload

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#wizardPicturePreview').attr('src', e.target.result).fadeIn('slow');
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$(window).resize(function () {
    $('.wizard-card').each(function () {
        $wizard = $(this);

        index = $wizard.bootstrapWizard('currentIndex');
        refreshAnimation($wizard, index);

        $('.moving-tab').css({
            'transition': 'transform 0s'
        });
    });
});

function refreshAnimation($wizard, index) {
    $total = $wizard.find('.nav-pills li').length;
    $li_width = 100 / $total;

    total_steps = $wizard.find('.nav-pills li').length;
    move_distance = $wizard.width() / total_steps;
    index_temp = index;
    vertical_level = 0;

    mobile_device = $(document).width() < 600 && $total > 3;

    if (mobile_device) {
        move_distance = $wizard.width() / 2;
        index_temp = index % 2;
        $li_width = 50;
    }

    $wizard.find('.nav li').css('width', $li_width + '%');

    step_width = move_distance;
    move_distance = move_distance * index_temp;

    $current = index + 1;

    if ($current == 1 || (mobile_device == true && (index % 2 == 0))) {
        move_distance -= 8;
    } else if ($current == total_steps || (mobile_device == true && (index % 2 == 1))) {
        move_distance += 8;
    }

    if (mobile_device) {
        vertical_level = parseInt(index / 2);
        vertical_level = vertical_level * 38;
    }

    $wizard.find('.moving-tab').css('width', step_width);
    $('.moving-tab').css({
        'transform': 'translate3d(' + move_distance + 'px, ' + vertical_level + 'px, 0)',
        'transition': 'all 0.5s cubic-bezier(0.29, 1.42, 0.79, 1)'

    });

    // setTimeout(function () {
    //    //$('.nav-pills > li').width('20%');
    //    $('.moving-tab').width('20%');
    //}, 100);
}
materialDesign = {

    checkScrollForTransparentNavbar: debounce(function () {
        if ($(document).scrollTop() > 260) {
            if (transparent) {
                transparent = false;
                $('.navbar-color-on-scroll').removeClass('navbar-transparent');
            }
        } else {
            if (!transparent) {
                transparent = true;
                $('.navbar-color-on-scroll').addClass('navbar-transparent');
            }
        }
    }, 17)

}

function debounce(func, wait, immediate) {
    var timeout;
    return function () {
        var context = this, args = arguments;
        clearTimeout(timeout);
        timeout = setTimeout(function () {
            timeout = null;
            if (!immediate) func.apply(context, args);
        }, wait);
        if (immediate && !timeout) func.apply(context, args);
    };
};

function ValidateAndFundAddressSave() {
    var frmAddress = $('input[name="AddressLine1"]').closest("form");
    $.validator.unobtrusive.parse(frmAddress);
    frmAddress.validate();
    if (frmAddress.valid()) {
        AddFundAddress();
    }
}

var kendoGrid;
function AddFundAddress() {
    
    var request = {
        FundId: $("#hdn_FundId").val(), FirmId: $('#FirmId').val(),
        AddressId: $('#create_addreess_addressId1').val(), MeetingLocation: $('#txtrequestwiz_address_meetinglocation').val(), AddressLine1: $('#txt_estbrequest_fund_addressline1').val(), City: $('#txt_estbrequest_fund_city').val(),
        State: $('#txt_estbrequest_fund_state').val(), PostCode: $('#txt_estbrequest_fund_postcode').val(), Country: $('#txt_estbrequest_fund_country').val(), id: $('#id').val()
    }
    isAddMode = false;
    var result = CallController('/EstablishmentRequest/AddressPartial', 'Post', request);
    if (typeof result != undefined && result.HttpCode != "200") {
        ModelErrorMessage('#Message', '', result.Message);
        return;
    } else {
        closeNav('AddressPartialModal');
        clearAddressFields();
        GetValues();
        SuccessMessage('U', '');
    }
}

function clearAddressFields() {
    $('#create_addreess_addressId1').val('');
    $('#txtrequestwiz_address_meetinglocation').val('');
    $('#txt_estbrequest_fund_addressline1').val('');
    $('#txt_estbrequest_fund_city').val('');
    $("#txt_estbrequest_fund_state").selectpicker('val', '-1');
    $('#txt_estbrequest_fund_postcode').val('');
    $('#txt_estbrequest_fund_country').val('');

}
function GetValues() {
    var request = { FirmId: $("#FirmId").val(), FundId: $("#hdn_FundId").val() };
    var result = CallController('/EstablishmentRequest/AddressPartial', 'Get', request);
    if (!kendoGrid) {
        kendoGrid = $("#address-table").kendoGrid({
            dataSource: {
                data: result,
                pageSize: 10
            },
            //height: 950,
            sortable: true,
            filterable: true,
            pageable: {
                pageSizes: true,
                buttonCount: 5
            },
            columns: [
                {
                    field: "MeetingLocation",
                    title: "Meeting Location",
                    width: 150
                },
            {
                field: "AddressLine1",
                title: "Address Line 1",
                width: 150
            }, {
                field: "City",
                title: "City",
                width: 120
            }, {
                field: "State",
                title: "State",
                width: 120
            }, {
                field: "PostCode",
                title: "Post Code",
                width: 120
            }, {
                field: "Country",
                title: "Country",
                width: 120
            }
            , {
                template: "<div class='action-tools'><a class='btn btn-success btn-xs btn-font btn-primary' onclick='GetEditAddressValues(#:AddressId #);'><i class='fa fa-edit'></i></a>" +
                                        "&nbsp;<a class='btn btn-danger btn-xs btn-font btn-Delete' onclick='DeleteValues(#: AddressId #, \"Address\");'><i class='fa fa-trash-o'></i></a> </div>"
            }]
        });
    }
    else {
        kendoGrid.data('kendoGrid').dataSource.data(result);
        kendoGrid.data('kendoGrid').refresh();
    }
}

function GetEditAddressValues(addressId)
{
    clearAddressFields();
    var request = { Id: addressId };
    var addressData = CallController('/EstablishmentRequest/GetAddressById', 'Get', request);
    addressData = JSON.parse(addressData);
    $('#create_addreess_addressId1').val(addressId);
    $('#txtrequestwiz_address_meetinglocation').val(addressData.MeetingLocation);
    $('#txt_estbrequest_fund_addressline1').val(addressData.AddressLine1);
    $('#txt_estbrequest_fund_city').val(addressData.City);
    $("#txt_estbrequest_fund_state").selectpicker('val', addressData.State);
    $('#txt_estbrequest_fund_postcode').val(addressData.PostCode);
    $('#txt_estbrequest_fund_country').val(addressData.Country);
    openNav('AddressPartialModal');
}

function DeleteValues(Id, context) {
    bootbox.confirm({
        message: "Are you sure you want to delete?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                var request = { ID: Id };
                CallController('/EstablishmentRequest/DeleteValues', 'Post', request);
                switch (context) {
                    case "Address":
                        GetValues();
                        break;
                    case "Member":
                        GetFundMemberData();
                        break;
                    case "Trustee":
                        break;

                }
                SuccessMessage('D', '');
            }
            else
                return;
        }
    });
}

function MovetoNextTab(tabname) {
    switch (tabname) {
        case "Main":
            $('#contactaddress').removeClass('active');
            $('#contacttrusteemain').addClass('active');
            $('#membernomination').removeClass('active');

            $('#address-contact-tab').removeClass('active');
            $('#addresss-main-tab').addClass('active');
            $('#member-nomination-tab').removeClass('active');
            break;
        case "Address":
            $('#contactaddress').addClass('active');
            $('#contacttrusteemain').removeClass('active');
            $('#membernomination').removeClass('active');

            $('#address-contact-tab').addClass('active');
            $('#addresss-main-tab').removeClass('active');
            $('#member-nomination-tab').removeClass('active');
            break;
        case "Nomination":
            $('#contactaddress').removeClass('active');
            $('#contacttrusteemain').removeClass('active');
            $('#membernomination').addClass('active');

            $('#address-contact-tab').removeClass('active');
            $('#addresss-main-tab').removeClass('active');
            $('#member-nomination-tab').addClass('active');
            break;
    }

}

function MovetoPreviousTab(tabname) {
    switch (tabname) {
        case "Main":
            $('#contactaddress').removeClass('active');
            $('#contacttrusteemain').addClass('active');
            $('#membernomination').removeClass('active');

            $('#address-contact-tab').removeClass('active');
            $('#addresss-main-tab').addClass('active');
            $('#member-nomination-tab').removeClass('active');
            break;
        case "Address":
            $('#contactaddress').addClass('active');
            $('#contacttrusteemain').removeClass('active');
            $('#membernomination').removeClass('active');

            $('#address-contact-tab').addClass('active');
            $('#addresss-main-tab').removeClass('active');
            $('#member-nomination-tab').removeClass('active');
            break;
    }

}

function AddFundMemberDetails() {
    var nominations = [];
    nominations.push({ FullName: $('#txt_member_nominee_fullname_row1').val(), DateOfBirth: $('#txt_member_nominee_dateofbirth_row1').val(), Relationship: $('#txt_member_nominee_relationship_row1').val(), Percentage: $('#txt_member_nominee_percentage_row1').val(), Condition: $('#txt_member_nominee_conditions_row1').val() });

    nominations.push({ FullName: $('#txt_member_nominee_fullname_row2').val(), DateOfBirth: $('#txt_member_nominee_dateofbirth_row2').val(), Relationship: $('#txt_member_nominee_relationship_row2').val(), Percentage: $('#txt_member_nominee_percentage_row2').val(), Condition: $('#txt_member_nominee_conditions_row2').val() });

    nominations.push({ FullName: $('#txt_member_nominee_fullname_row3').val(), DateOfBirth: $('#txt_member_nominee_dateofbirth_row3').val(), Relationship: $('#txt_member_nominee_relationship_row3').val(), Percentage: $('#txt_member_nominee_percentage_row3').val(), Condition: $('#txt_member_nominee_conditions_row3').val() });

    nominations.push({ FullName: $('#txt_member_nominee_fullname_row4').val(), DateOfBirth: $('#txt_member_nominee_dateofbirth_row4').val(), Relationship: $('#txt_member_nominee_relationship_row4').val(), Percentage: $('#txt_member_nominee_percentage_row4').val(), Condition: $('#txt_member_nominee_conditions_row4').val() });
    var request = {
        Id: $('#hdn_fundMemberId').val(),
        FirmId: $("#FirmId").val(), FundId: $("#hdn_FundId").val(),
        Title: $('#ddl_estrequest_member_title').val(),
        FirstName: $('#txt_estrequest_member_firstname').val(),
        MiddleName: $('#txt_estrequest_member_middlename').val(),
        LastName: $('#txt_estrequest_member_lastname').val(),
        TFN: $('#txt_estrequest_member_tfn').val(),
        DOB: $('#DOB').val(),
        CityOfBirth: $('#txt_estrequest_member_birthcity').val(),
        EmailAddress: $('#txt_estrequest_member_emailaddress').val(),
        MemberAddress: {
            AddressLine1: $('#txt_estbrequest_contactmember_addressline1').val(),
            AddressLine2: $('#').val(),
            City: $('#txt_estbrequest_contactmember_city').val(),
            PostCode: $('#txt_estbrequest_contactmember_postcode').val(),
            State: $('#ddl_estbrequest_contactmember_state').val(),
            Country: $('#txt_estbrequest_contactmember_country').val(),
        },
        MemberNominations: nominations
    };
    var result = CallController('/EstablishmentRequest/AddFundMember', 'Post', request);
    if (typeof result != undefined && result.HttpCode != "200") {
        ModelErrorMessage('#Message', '', result.Message);
        return;
    } else {
        closeNav('ContactModal');
        ClearMemberValues();
        GetFundMemberData();
        SuccessMessage('U', '');
    }
}

var fundmembergrid = null;
function GetFundMemberData() {
    var request = { FirmId: $("#FirmId").val(), FundId: $("#hdn_FundId").val() };
    var result = CallController('/EstablishmentRequest/GetFundMember', 'Get', request);
    if (!fundmembergrid) {
        fundmembergrid = $("#fundestb-contacts-table").kendoGrid({
            dataSource: {
                data: result,
                pageSize: 10
            },
            //height: 950,
            sortable: true,
            //filterable: true,
            pageable: {
                pageSizes: true,
                buttonCount: 5
            },
            columns: [
                {
                    field: "Title",
                    title: "Meeting Location",
                    width: 150
                },
            {
                field: "FirstName",
                title: "First Name",
                width: 150
            }, {
                field: "DOB",
                title: "DOB",
                width: 120
            },
            {
                field: "IsChairperson",
                title: "Is Chairperson",
                width: 120,
                template: "<input type='checkbox' id=chk_Ischairperson_#:Id # />"
            }
            , {
                template: "<div class='action-tools'><a class='btn btn-success btn-xs btn-font btn-primary' onclick='GetEditMemberValues(#:Id #);'><i class='fa fa-edit'></i></a>" +
                        "&nbsp;<a class='btn btn-danger btn-xs btn-font btn-Delete' onclick='DeleteValues(#: Id #, \"Member\");'><i class='fa fa-trash-o'></i></a> </div>"
            }]
        });
    }
    else {
        fundmembergrid.data('kendoGrid').dataSource.data(result);
        fundmembergrid.data('kendoGrid').refresh();
    }
}

function GetEditMemberValues(contactId) {
    ClearMemberValues();
    var request = { Id: contactId };
    var memberData = CallController('/EstablishmentRequest/GetFundMemberById', 'Get', request);
    memberData = JSON.parse(memberData);
    $('#hdn_fundMemberId').val(contactId);
    $('#ddl_estrequest_member_title').val(memberData.Title);
    $('#txt_estrequest_member_firstname').val(memberData.FirstName);
    $('#txt_estrequest_member_middlename').val(memberData.MiddleName);
    $('#txt_estrequest_member_lastname').val(memberData.LastName);
    $('#txt_estrequest_member_tfn').val(memberData.TFN);
    $('#DOB').val(memberData.DateOfBirth);
    $('#txt_estrequest_member_birthcity').val(memberData.CityOfBirth);
    $('#txt_estrequest_member_emailaddress').val(memberData.EmailAddress);

    if (memberData.MemberAddress != null) {
        $('#txt_estbrequest_contactmember_addressline1').val(memberData.MemberAddress.AddressLine1);
        $('#txt_estbrequest_contactmember_city').val(memberData.MemberAddress.City);
        $('#txt_estbrequest_contactmember_postcode').val(memberData.MemberAddress.PostCode);
        $('#ddl_estbrequest_contactmember_state').val(memberData.MemberAddress.State);
        $('#txt_estbrequest_contactmember_country').val(memberData.MemberAddress.Country);
    }

    if (memberData.MemberNominations != null) {
        $('#txt_member_nominee_fullname_row1').val(memberData.MemberNominations[0].FullName);
        $('#txt_member_nominee_dateofbirth_row1').val(memberData.MemberNominations[0].DateOfBirth);
        $('#txt_member_nominee_relationship_row1').val(memberData.MemberNominations[0].Relationship);
        $('#txt_member_nominee_percentage_row1').val(memberData.MemberNominations[0].Percentage);
        $('#txt_member_nominee_conditions_row1').val(memberData.MemberNominations[0].Condition);

        $('#txt_member_nominee_fullname_row2').val(memberData.MemberNominations[1].FullName);
        $('#txt_member_nominee_dateofbirth_row2').val(memberData.MemberNominations[1].DateOfBirth);
        $('#txt_member_nominee_relationship_row2').val(memberData.MemberNominations[1].Relationship);
        $('#txt_member_nominee_percentage_row2').val(memberData.MemberNominations[1].Percentage);
        $('#txt_member_nominee_conditions_row2').val(memberData.MemberNominations[1].Condition);

        $('#txt_member_nominee_fullname_row3').val(memberData.MemberNominations[2].FullName);
        $('#txt_member_nominee_dateofbirth_row3').val(memberData.MemberNominations[2].DateOfBirth);
        $('#txt_member_nominee_relationship_row3').val(memberData.MemberNominations[2].Relationship);
        $('#txt_member_nominee_percentage_row3').val(memberData.MemberNominations[2].Percentage);
        $('#txt_member_nominee_conditions_row3').val(memberData.MemberNominations[2].Condition);

        $('#txt_member_nominee_fullname_row4').val(memberData.MemberNominations[3].FullName);
        $('#txt_member_nominee_dateofbirth_row4').val(memberData.MemberNominations[3].DateOfBirth);
        $('#txt_member_nominee_relationship_row4').val(memberData.MemberNominations[3].Relationship);
        $('#txt_member_nominee_percentage_row4').val(memberData.MemberNominations[3].Percentage);
        $('#txt_member_nominee_conditions_row4').val(memberData.MemberNominations[3].Condition);
    }

    openNav("ContactModal");

}

function ClearMemberValues() {
    $('#ContactId').val('');
    $('#ddl_estrequest_member_title').val('');
    $('#txt_estrequest_member_firstname').val('');
    $('#txt_estrequest_member_middlename').val('');
    $('#txt_estrequest_member_lastname').val('');
    $('#txt_estrequest_member_tfn').val('');
    $('#DOB').val('')
    $('#txt_estrequest_member_birthcity').val('');
    $('#txt_estrequest_member_emailaddress').val('');

    $('#txt_estbrequest_contactmember_addressline1').val('');
    $('#txt_estbrequest_contactmember_city').val('');
    $('#txt_estbrequest_contactmember_postcode').val('');
    $('#ddl_estbrequest_contactmember_state').val('');
    $('#txt_estbrequest_contactmember_country').val('');
    $('#txt_member_nominee_fullname_row1').val('');
    $('#txt_member_nominee_dateofbirth_row1').val('');
    $('#txt_member_nominee_relationship_row1').val('');
    $('#txt_member_nominee_percentage_row1').val('');
    $('#txt_member_nominee_conditions_row1').val('');

    $('#txt_member_nominee_fullname_row2').val('');
    $('#txt_member_nominee_dateofbirth_row2').val('');
    $('#txt_member_nominee_relationship_row2').val('');
    $('#txt_member_nominee_percentage_row2').val('');
    $('#txt_member_nominee_conditions_row2').val('');

    $('#txt_member_nominee_fullname_row3').val('');
    $('#txt_member_nominee_dateofbirth_row3').val('');
    $('#txt_member_nominee_relationship_row3').val('');
    $('#txt_member_nominee_percentage_row3').val('');
    $('#txt_member_nominee_conditions_row3').val('');

    $('#txt_member_nominee_fullname_row4').val('');
    $('#txt_member_nominee_dateofbirth_row4').val('');
    $('#txt_member_nominee_relationship_row4').val('');
    $('#txt_member_nominee_percentage_row4').val('');
    $('#txt_member_nominee_conditions_row4').val('');
}

function SaveEstbTrusteeData() {
    var request = {
        TrusteeId: $('#estb_request_trusteeid').val(),
        FirmId: $("#FirmId").val(), FundId: $("#hdn_FundId").val(),
        TrusteeType: 'Corporate',
        TrusteeCompany: $('#txtProposedNameForTrusteeCompany').val(),
        RegistrationState: $('#ddl_Corporate_Trustee_Stateof_registration').val(),
        DoesTrusteeCompanyOccupyAddress: $('#chkTrusteeCopmayOccupy').attr('checked'),
        RegisteredOfficeAddress: {
            AddressLine1: $('#txt_roa_addressline1').val(),
            City: $('#txt_roa_suburb').val(),
            PostCode: $('#txt_roa_postcode').val(),
            State: $('#ddl_roa_Stateof_registration').val(),
            Country: $('#txt_roa_country').val()
        },
        PrincipalOfficeAddress: {
            AddressLine1: $('#txt_poa_addressline1').val(),
            City: $('#txt_poa_suburb').val(),
            PostCode: $('#txt_poa_postcode').val(),
            State: $('#ddl_poa_Stateof_registration').val(),
            Country: $('#txt_poa_country').val()
        }
    };

    var result = CallController('/EstablishmentRequest/AddFundTrusteeData', 'Post', request);
    if (typeof result != undefined && result.HttpCode != "200") {
        ModelErrorMessage('#Message', '', result.Message);
        return;
    } else {
        //ClearTrusteeValues();
        SuccessMessage('U', '');
    }
}


function ClearTrusteeValues() {
    $('#estb_request_trusteeid').val('');
    $('#txtProposedNameForTrusteeCompany').val('');
    $("#ddl_Corporate_Trustee_Stateof_registration").selectpicker('val', '-1');
    $("#ddl_roa_Stateof_registration").selectpicker('val', '-1');
    $("#ddl_poa_Stateof_registration").selectpicker('val', '-1');
    $('#chkTrusteeCopmayOccupy').val('');
    $('#txt_roa_addressline1').val('');
    $('#txt_roa_suburb').val('');
    $('#txt_roa_postcode').val('');
    $('#txt_roa_country').val('');
    $('#txt_poa_addressline1').val('');
    $('#txt_poa_suburb').val('');
    $('#txt_poa_postcode').val('');
    $('#txt_poa_country').val('');
}



