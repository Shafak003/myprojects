﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalThrowCatchException
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter your Name Full");
            String Userinput = Console.ReadLine();
            if (Userinput.Length > 6)
            {
                Console.WriteLine("Name should b less than 6 Character");
                //return;    // it means it will stop and wont ask your age 
            }
            Console.WriteLine("Enter Your Age");
            int Age = 0;
            try
            {
                Age = Convert.ToInt32(Console.ReadLine()); // we can write same as above int age = Console.ReadLine()
                Console.WriteLine("Hello Mr "+ Userinput +" Your age is " + Age);
                Console.ReadLine();
            }
            catch(Exception ex)
            {
                Console.WriteLine("Age should be Numeric");
                Console.ReadLine();
            }

        }
    }
}
