﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalThrowCatchException
{
    /*
    paragraph comment
    */
    class Program
    {
        static void Main(string[] args)
        {
            //this line diplay Enter your name
            Console.WriteLine("Enter your Name Full");
            // this line takes input from the end user
            String Userinput = Console.ReadLine();
            try
            { 
                //this line check no. is not greater then6 
            if (Userinput.Length > 6)
            {
                throw new Exception("Name should b less than 6 Character");
                //return;    // it means it will stop and wont ask your age 
            }
            Console.WriteLine("Enter Your Age");
            int Age = 0;
              Age = Convert.ToInt32(Console.ReadLine()); // we can write same as above int age = Console.ReadLine()
                Console.WriteLine("Hello Mr " + Userinput + " Your age is " + Age);
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message.ToString());// Control Will come here
               
                Console.ReadLine();
            }

        }
    }
}
