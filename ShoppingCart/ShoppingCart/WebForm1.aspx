﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="ShoppingCart.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>
</head>
<body style="height: 102px">
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Size="X-Large" Text="Welcome to Shopping Cart Sites"></asp:Label>
        <br />
        <br />
&nbsp;In this Page you will get the Primium Products
    
        <asp:DataList ID="DataList1" runat="server" DataSourceID="SqlDataSource1" Height="472px" RepeatColumns="3" Width="466px" RepeatDirection="Horizontal" OnItemCommand="DataList1_ItemCommand" BorderColor="#FFFFCC">
            <ItemTemplate>
                <table class="auto-style1" aria-label="1" border="2">
                    <tr>
                        <td style="text-align: center; background-color: #FFCC00">
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("[product name]", "{0}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center; background-color: #FF0066">
                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("[product id]", "Number - {0:}") %>'></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
                            <asp:Image ID="Image1" runat="server" ItemStyle-Width="50px" ControlStyle-Width="100" ControlStyle-Height="100" ImageUrl='<%# Eval("[product image]") %>' />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center">
                            <asp:Button ID="Button1" runat="server" style="background-color: #9900CC" Text="Select Detail" CommandName="SelectDetail"  CommandArgument='<%# Eval("[product id]") %>' OnClick="Button1_Click"  />
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                <br />
                <br />
            </ItemTemplate>
        </asp:DataList>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:shoppingcartConnectionString3 %>" SelectCommand="SELECT * FROM [ShoppingCart_1]"></asp:SqlDataSource>
    
    </div>
    </form>
</body>
</html>
