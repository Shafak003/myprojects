﻿var TrusteeContactGrid;
var contactTypes = null;
var roles = null;
var validationmessage = "";

// Format Contacts Type Combo
function formatContactsType(id) {
    var value = null;
    for (var i = 0, iLen = contactTypes.length; i < iLen; i++) {
        if (contactTypes[i].Id == id) {
            value = contactTypes[i].Name;
        }
    }
    return value;
}

// Format Roles Combo
function formatRoles(id) {
    var value = null;
    for (var i = 0, iLen = roles.length; i < iLen; i++) {
        if (roles[i].Id == id) {
            value = roles[i].Name;
        }
    }
    return value;
}


jsTrusteeContact = function () {
    var isEditing = null;

    var EntityId = null;
    var isAddMode = false;
    var EntityType = "Fund";
    var FundRequestId = null;



    function GetValues() {

        GetInitialValues();

        var request = { EntityId: (jsTrusteeContact.EntityId == null ? jsTrusteeContact.FundRequestId : jsTrusteeContact.EntityId), EntityType: jsTrusteeContact.EntityType, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual' };
        var result = CallController('/Contact/TrusteeContacts', 'Get', request);
        contactTypes = CallController('/Contact/GetAllContactTypes', 'Get', null);
        roles = CallController('/Contact/GetAllRoles?isfirm=' + jsTrusteeContact.isfirm + '&issmsf=' + jsTrusteeContact.issmsf + '&isclient=' + jsTrusteeContact.isclient, 'Get', null);
        var icon = '';
        icon = '<i class="glyphicon glyphicon-ok"></i>';

        console.log(result);
        if (!TrusteeContactGrid) {
            $("#trustee-contacts-table").html("");
            TrusteeContactGrid = $("#trustee-contacts-table").kendoGrid({
                dataSource: {
                    data: result,
                    pageSize: 10
                },
                //height: 950,
                sortable: true,
                filterable: true,
                pageable: {
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                    //{
                    //    field: "Title + '|' +Email+ '|' + Phone + '|' + TFN + '|' + DOB + '|' + Gender",
                    //    //field: "formatAddressType(AddressTypeId)",
                    //    title: "",
                    //    width: 350
                    //},
                    //{
                    //    template: "#=formatContactsType(ContactTypeId)#",
                    //    title: "Contact Type",
                    //    width: 100
                    //},
                    {
                        field: "Title",
                        title: "Title",
                        width: 200
                    },
                    {
                        field: "TFN",
                        title: "TFN",
                        width: 150
                    }
                    , {
                        template: "<a class='btn btn-info edit' onclick='jsTrusteeContact.GetEditValues(#:Id #,#: ContactId #);'><i class='fa fa-edit'></i></a>" +
                        "&nbsp;<a class='btn btn-danger delete' onclick='jsTrusteeContact.DeleteValues(#: Id #);'><i class='fa fa-trash-o'></i></a>"
                    }]
            });
        }
        else {
            TrusteeContactGrid.data('kendoGrid').dataSource.data(result);
            TrusteeContactGrid.data('kendoGrid').refresh();
        }
    }

    function CheckNull(value) {
        return (value == null) ? "" : value;
    }
    // add event listener for opening and closing details
    $('#trustee-contacts-table tbody').on('click', 'td.details-control', function () {
        var table = $('#trustee-contacts-table').DataTable();
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        if (row.child.isShown()) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        } else {
            // Open this row
            row.child(format(row.data())).show();
            tr.addClass('shown');
        }
    });

    function format(d) {
        // `d` is the original data object for the row
        var values = d[0].split('|');
        if (values.length > 0) {
            var gender = values[5] == 0 ? 'Male' : 'Female';
            var date = new Date(parseInt(values[4].substr(6)));
            var formattedDate = ("0" + (date.getMonth() + 1)).slice(-2) + "-" +
                ("0" + date.getDate()).slice(-2) + "-" + date.getFullYear();

            return '<table class="table table-celled" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px; margin-bottom:20px;">' +
                '<tr>' +
                '<td>Title:</td>' +
                '<td>' + values[0] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Email:</td>' +
                '<td>' + values[1] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Phone:</td>' +
                '<td>' + values[2] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>TFN:</td>' +
                '<td>' + values[3] + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>DOB:</td>' +
                '<td>' + formattedDate + '</td>' +
                '</tr>' +
                '<tr>' +
                '<td>Gender:</td>' +
                '<td>' + gender + '</td>' +
                '</tr>' +
                '</table>';
        }
    }

    function GetEditValues(Id, ContactsId) {
        GetInitialValues();
        var request = { Id: Id, EntityType: jsTrusteeContact.EntityType, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual' };
        var result = CallController('/Contact/GetTrusteeContactById', 'Get', request);
        $('#TrusteeContactModal #DOB').val(result.DOB);
        $('#TrusteeContactModal #Email').val(result.Email);
        $('#TrusteeContactModal #FirstName').val(result.FirstName);
        $('#TrusteeContactModal #Gender').val(result.Gender);
        $('#TrusteeContactModal #LastName').val(result.LastName);
        $('#TrusteeContactModal #Phone').val(result.Phone);
        $('#TrusteeContactModal #TFN').val(result.TFN);
        $('#TrusteeContactModal #Title').val(result.Title);
        $('#TrusteeContactModal #RoleId').val(result.RoleId);
        $('#TrusteeContactModal #IsPrimary').prop('checked', result.IsPrimary);
        $('#TrusteeContactModal #Id').val(result.Id);
        $('#TrusteeContactModal #ContactId').val(result.ContactId);

        $('#TrusteeContactModal #AddressInfo_AddressId').val(result.AddressInfo.AddressId);
        $('#TrusteeContactModal #AddressInfo_AddressLine1').val(result.AddressInfo.AddressLine1);
        $('#TrusteeContactModal #AddressInfo_AddressLine2').val(result.AddressInfo.AddressLine2);
        $('#TrusteeContactModal #AddressInfo_City').val(result.AddressInfo.City);
        $('#TrusteeContactModal #AddressInfo_State').val(result.AddressInfo.State);
        $('#TrusteeContactModal #AddressInfo_PostCode').val(result.AddressInfo.PostCode);
        $('#TrusteeContactModal #AddressInfo_Country').val(result.AddressInfo.Country);

        openNav("TrusteeContactModal");
    }

    function GetInitialValues() {
        $('#TrusteeContactModal #DOB').val('');
        $('#TrusteeContactModal #Email').val('');
        $('#TrusteeContactModal #FirstName').val('');
        $('#TrusteeContactModal #Gender').val();
        $('#TrusteeContactModal #LastName').val('');
        $('#TrusteeContactModal #Phone').val('');
        $('#TrusteeContactModal #TFN').val('');
        $('#TrusteeContactModal #IsPrimary').prop('checked', false);
        $('#TrusteeContactModal #Title').val('');
        $('#TrusteeContactModal #Id').val('0');
        $('#TrusteeContactModal #ContactId').val('0');

        $('#TrusteeContactModal #AddressInfo_AddressId').val('0');
        $('#TrusteeContactModal #AddressInfo_AddressLine1').val('');
        $('#TrusteeContactModal #AddressInfo_AddressLine2').val('');
        $('#TrusteeContactModal #AddressInfo_City').val('');
        $('#TrusteeContactModal #AddressInfo_State').val('');
        $('#TrusteeContactModal #AddressInfo_PostCode').val('');
        $('#TrusteeContactModal #AddressInfo_Country').val('');

        BindCombo("#TrusteeContactModal #ContactIdAttach", "/Contact/GetAllContacts");

        jsTrusteeContact.FundRequestId = $('#FundRequestId').val();
        jsTrusteeContact.EntityId = $('#TrustId').val();
        jsTrusteeContact.EntityType = 'Trustee';

        // Tab Initial Setting
        $('#TrusteeContactModal #trusteecontactmain').addClass('active');
        $('#TrusteeContactModal #trustee-main-tab').addClass('active');

        $("#TrusteeContactModal #DOB").kendoDatePicker({
            animation: {
                open: {
                    effects: "zoom:in",
                    duration: 300
                }
            }
        });

        $('#TrusteeContactModal #ContactIdAttach').on('change', function () {

            var selected = $('#TrusteeContactModal #ContactIdAttach').val();
            if (selected > 0) {
                SetExistingValue(selected);
            }
        });
    }

    function SetExistingValue(selected) {
        var request = { id: selected };
        var result = CallController('/Contact/GetContact', 'Get', request);
        if (result != null) {
            $('#TrusteeContactModal #DOB').val(result.DOB);
            $('#TrusteeContactModal #Email').val(result.Email);
            $('#TrusteeContactModal #FirstName').val(result.FirstName);
            $('#TrusteeContactModal #Gender').val(result.Gender);
            $('#TrusteeContactModal #LastName').val(result.LastName);
            $('#TrusteeContactModal #Phone').val(result.Phone);
            $('#TrusteeContactModal #TFN').val(result.TFN);
            $('#TrusteeContactModal #Title').val(result.Title);

            $('#TrusteeContactModal #IsPrimary').prop('checked', result.IsPrimary);
            $('#TrusteeContactModal #Id').val(0);
            $('#TrusteeContactModal #ContactId').val(result.ContactId);
        }

    }
    function GetPopupValues() {
        // For Contacts Type Combo
        //BindCombo("#ContactTypeIdAttach", "/Contact/GetAllContactTypes");
        //$('#ContactTypeIdAttach').selectpicker('refresh');

        // For Contacts Combo
        BindCombo("#ContactIdAttach", "/Contact/GetAllContacts");
        //$('#ContactIdAttach').selectpicker('refresh');

        // For Contacts Combo
        //BindCombo("#ContactRoleIdAttach", "/Contact/GetAllRoles");
        //$('#ContactRoleIdAttach').selectpicker('refresh');

        //$("#ContactTypeIdAttach").select2({ dropdownParent: $("#AttachContactModal") });
        //$("#ContactIdAttach").select2({ dropdownParent: $("#AttachContactModal") });
        //$("#ContactRoleIdAttach").select2({ dropdownParent: $("#AttachContactModal") });
    }

    function BindCombo(id, url) {
        // For Contacts Type Combo
        var resultType = CallController(url, 'Get', null);
        $(id).empty();
        var options = "";
        // options = "<option value='-1'>None</option>"; // uncomment if you want this item
        $.each(resultType, function (a, b) {
            options += "<option value='" + b.Id + "'>" + b.Name + "</option>";
        });
        $(id).html(options);
        $(id).selectpicker('refresh');
    }



    function DeleteValues(Id) {
        bootbox.confirm({
            message: "Are you sure you want to delete?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var request = { ID: Id, FormType: jsTrusteeContact.EntityType, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual' };
                    CallController('/Contact/DeleteAttachedContact', 'Post', request);
                    SuccessMessage('D', '');
                    GetValues();
                }
                else
                    return;
            }
        });
    }

    function ValidateAndSave() {
        if ($('#frmTrusteeContact').valid()) {
            AddUpdate();
        }
    }

    function AddUpdate() {
        if ((jsTrusteeContact.EntityId != null && jsTrusteeContact.EntityId != undefined && jsTrusteeContact.EntityId > 0) || (jsTrusteeContact.FundRequestId != null && jsTrusteeContact.FundRequestId != undefined && jsTrusteeContact.FundRequestId > 0)) {
            var AddressInfo = {
                AddressId: $('#TrusteeContactModal #AddressInfo_AddressId').val(), AddressLine1: $('#TrusteeContactModal #AddressInfo_AddressLine1').val(), AddressLine2: $('#TrusteeContactModal #AddressInfo_AddressLine2').val(), City: $('#TrusteeContactModal #AddressInfo_City').val(),
                State: $('#TrusteeContactModal #AddressInfo_State').val(), PostCode: $('#TrusteeContactModal #AddressInfo_PostCode').val(), Country: $('#TrusteeContactModal #AddressInfo_Country').val()
            };
            var request = {
                Id: $('#TrusteeContactModal #Id').val(), FundRequestId: jsTrusteeContact.FundRequestId, EntityId: jsTrusteeContact.EntityId, EntityType: jsTrusteeContact.EntityType, ContactTypeId: $('#TrusteeContactModal #ContactTypeId').val(), ContactId: $('#TrusteeContactModal #ContactId').val(), RoleId: $('#TrusteeContactModal #RoleId').val(), DOB: ToDateFormat($('#TrusteeContactModal #DOB').val()), Email: $('#TrusteeContactModal #Email').val(), FirstName: $('#TrusteeContactModal #FirstName').val(),
                Gender: $('#TrusteeContactModal #Gender').val(), LastName: $('#TrusteeContactModal #LastName').val(), Phone: $('#TrusteeContactModal #Phone').val(), TFN: $('#TrusteeContactModal #TFN').val(), Title: $('#TrusteeContactModal #Title').val(), IsPrimary: $("#TrusteeContactModal #IsPrimary").is(':checked'), IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual', AddressInfo
            }
            isAddMode = false;

            var result = CallController('/Contact/UpdateTrusteeContact', 'Post', request);
            if (result !== undefined && result.HttpCode != "200") {
                ModelErrorMessage('#Message', '', result.Message);
                return;
            } else {
                GetValues();
                if (result.OperationStatuses.length > 0) {
                    var CurId = result.OperationStatuses[0].UniqueKey;
                    GetEditValues(CurId, 0);
                }
                closeNav('TrusteeContactModal');
                SuccessMessage('U', '');
            }

        } else {
            ModelErrorMessage('#Message', '', 'Mandatory fields missing on Main Tab.');
        }
    }

    return {
        EntityType: EntityType, GetValues: GetValues,
        GetPopupValues: GetPopupValues,
        AddUpdate: AddUpdate, ValidateAndSave: ValidateAndSave, GetInitialValues: GetInitialValues, GetEditValues: GetEditValues,
        DeleteValues: DeleteValues
    }
}();