﻿
jsCalendar = function () {
    
    function InitializeControl()
    {
        var dataManager = CallController('/Calendar/GetData', 'Get', null);
        $("#schedule").ejSchedule({
            timeZone: "UTC +00:00",
            width: "100%",
            currentView: ej.Schedule.CurrentView.Workweek,
            workWeek: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"],
            dateFormat: "dd/MM/yyyy",
            height: "600px",
            currentDate: new Date(),
            appointmentSettings: {
                dataSource: dataManager,
                id: "Id",
                ParentId: "ProgramId",
                subject: "ProgramName",
                startTime: "ProgramStartTime",
                endTime: "ProgramEndTime",
                allDay: "IsAllDay",
                recurrence: "IsRecurrence",
                recurrenceRule: "RecurrenceRule"
            },
            cellClick: "onCellClick",
            beforeAppointmentCreate: "onAppointmentSave",
            beforeAppointmentChange: "onAppointmentEdit",
            beforeAppointmentRemove: "onAppointmentDelete"
        });
    }

    function Delete(id) {
        var request = { id: id };
        var result = CallController('/Calendar/Delete', 'Get', request);
        SuccessMessage('D', '');
    }

    function AddUpdate(request) {
        
        var result = CallController('/Calendar/AddEdit', 'Post', request);
        if (result !== undefined && result.HttpCode != "200") {
            ErrorMessage('#Message', '', result.Message);
            return;
        } else {
            InitializeControl();
            SuccessMessage('U', '');
        }
    }

    return {
        InitializeControl: InitializeControl, Delete: Delete, AddUpdate: AddUpdate
    }
}();