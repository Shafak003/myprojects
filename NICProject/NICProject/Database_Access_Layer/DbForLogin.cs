﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using NICProject.Models;

namespace NICProject.Database_Access_Layer
{
    public class DbForLogin
    {
        public SqlConnection con;
        public SqlCommand cmd;

        public DbForLogin()
        {
            con = new SqlConnection();
            con.ConnectionString = ConfigurationManager.ConnectionStrings["DBConnect"].ToString();
        }
        public  void SecurityPersonReg(UserLogin usr)//this is used for inserting through stored procedure
        {
            cmd = new SqlCommand();
            cmd.Connection = con;
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "RegInsertLogin";
            cmd.Parameters.AddWithValue("@UserName", usr.UserName);
            cmd.Parameters.AddWithValue("@Password", usr.Password);
            con.Open();
            cmd.ExecuteNonQuery();
        }

       

    }
}