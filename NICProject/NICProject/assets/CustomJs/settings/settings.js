﻿//var oTableRoles = $('#role-list').dataTable({
//    "paging": true,
//    "filter": true,
//    "pageLength": 25,
//    "aoColumns": [
//        null,
//        null,
//        null,
//        { "bSortable": false }
//    ]
//});

//var oTable = $('#user-list').dataTable({
//    "paging": true,
//    "filter": true,
//    "pageLength": 25,
//    "aoColumns": [
//        null,
//        null,
//        null,
//        null,
//        null,
//        null,
//        null,
//        { "bSortable": false }
//    ]
//});
var UserGrid;
var actions = [];
function BindRoles() {
    $("#role-list > tbody:last").children().remove();
    oTableRoles.fnClearTable();
    var result = CallController('/Settings/GetRoles', 'Get', null);
    $.each(result, function (i, item) {
        var systemDefined = '<span class="label label-danger">No</span>'
        var disableDelete = ' href="javascript:DeleteRole(' + item.Id + ');" ';
        if (item.IsSystemRole == true) {
            systemDefined = '<span class="label label-success">Yes</span>'
            disableDelete = ' disabled href="#"';
        }
        var aiNew = oTableRoles.fnAddData([
         item.Id, item.Name, systemDefined,
        "<a class='btn btn-primary tasktools-sidebar' href='javascript:EditRole(" + item.Id + ");'><i class='fa fa-edit'></i>Edit</a> <a " + disableDelete + " class='btn btn-danger delete'><i class='fa fa-trash-o'></i>Delete</a>"
        ]);
    });
}

function GetActionButtons(RowId, list) {
    if (typeof list == undefined || list == null || list.length == 0) {
        return "";
    }
    for (var i = 0; i < list.length; i++) {
        if (list[i].Id == RowId)
            return list[i].template;
    }
    //return "";
}

function BindUsers() {
    //$("#user-list > tbody:last").children().remove();
    //oTable.fnClearTable();
    var result = CallController('/Settings/GetUsers', 'Get', null);

    //this code is used for generating template for edit and delete buttons
    $.each(result, function (i, item) {
        var disableDelete = ' href="javascript:DeleteUser(\'' + item.Id + '\');" ';
        var disableEdit = ' class="btn btn-success btn-xs btn-font btn-primary" href="javascript:EditUser(\'' + item.Id + '\');" ';

        if (item.Id != $('#LoggedInUserId').val() && $('#IsSuperAdmin').val() == false) {
            disableDelete = ' disabled href="#"';
            disableEdit = ' class="btn btn-primary" disabled href="#"';
        }

        if (item.IsSuperAdmin == true) {
            disableDelete = ' disabled href="#"';
        }


        var a = "<a " + disableEdit + "><i class='fa fa-edit'></i>Edit</a> <a " + disableDelete + " class='btn btn-danger btn-xs btn-font btn-Delete'><i class='fa fa-trash-o'></i>Delete</a>"
        actions.push({ Id: item.Id, template: a });
        //actions.template.push(a);
        //var aiNew = oTable.fnAddData([
        // //CheckNull(item.Username), CheckNull(item.FirstName), CheckNull(item.LastName), CheckNull(item.Email), CheckNull(item.Phone), CheckNull(item.Mobile),
        // CheckNull(item.Username), CheckNull(item.Role), CheckNull(item.FirstName), CheckNull(item.LastName), CheckNull(item.Email), CheckNull(item.Phone), CheckNull(item.Mobile),
        //"<a " + disableEdit + "><i class='fa fa-edit'></i>Edit</a> <a " + disableDelete + " class='btn btn-danger delete'><i class='fa fa-trash-o'></i>Delete</a>"
        //]);
    });


    if (!UserGrid) {
        UserGrid = $("#user-list").kendoGrid({
            dataSource: {
                data: result,
                pageSize: 20
            },
            //height: 950,
            sortable: true,
            filterable: {
                operators: {
                    string: {
                        contains: "Contains",
                        doesnotcontain: "Does not contain",
                        eq: "Equal to",
                        neq: "Not equal to",
                        startswith: "Starts with",
                        endswith: "Ends with",
                        special: "Contains special characters",
                        isempty: "Is empty",
                        isnotempty: "Is not empty",
                        isnotnull: "Is not null",
                        isnull: "Is null",
                        neq: "Not equal"
                    }
                }
            },
            pageable: {
                pageSizes: true,
                buttonCount: 5
            },
            columns: [
                {
                    field: "Username",
                    title: "User Name",
                    width: 250
                },
                {
                    field: "Role",
                    title: "Role",
                    width: 150
                }, {
                    field: "FirstName",
                    title: "First Name",
                    width: 150
                }, {
                    field: "LastName",
                    title: "Last Name",
                    width: 150
                }
                //, {
                //    title: "Email",
                //    field: "Phone",
                //    width: 150
                //}, {
                //    title: "Phone",
                //    field: "Phone",
                //    width: 100
                //}, {
                //    title: "Mobile",
                //    field: "Mobile",
                //    width: 100
                //}
                , {
                    template: "#= GetActionButtons(Id,actions)#"
                    //"<a class='btn btn-primary tasktools-sidebar' href='javascript:EditUser(\'#: Id #\"); ><i class='fa fa-edit'></i>Edit</a> <a href='javascript:DeleteUser(\"#:Id#\")'  class='btn btn-danger delete'><i class='fa fa-trash-o'></i>Delete</a>"
                }]
        });
    }
    else {
        UserGrid.data('kendoGrid').dataSource.data(result);
        UserGrid.data('kendoGrid').refresh();

    }
}

function CheckNull(value) {
    return (value == null) ? "" : value;
}

function DeleteRole(id) {
    bootbox.confirm({
        message: "Are you sure you want to delete?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                var request = { id: id };
                var result = CallController('/Settings/DeleteRole', 'Post', request);
                SuccessMessage('D', '');
                BindRoles();
            }
            else
                return;
        }
    });
}

function DeleteUser(id) {
    bootbox.confirm({
        message: "Are you sure you want to delete?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                var request = { id: id };
                var result = CallController('/Settings/DeleteUser', 'Post', request);
                SuccessMessage('D', '');
                BindUsers();
            }
            else
                return;
        }
    });
}


function EditRole(id) {
    var request = {
        Id: id
    };
    var result = CallController('/Settings/GetRoleByID', 'Get', request);
    $("#Id").val(result[0]);
    $("#Name").val(result[1]);
    $("#dialog-title").text("Edit Role");
}

function BindUserRoles() {
    var resultType = CallController('/Settings/GetRoles', 'Get', null);
    item_UserListSelect = document.getElementById('UserRoles');
    $('#UserRoles').empty();
    $.each(resultType, function (i, item) {
        item_UserListSelect.options[item_UserListSelect.options.length] = new Option(item.Name, item.Id);
    });
    $('#UserRoles').selectpicker('refresh');

    //$("#UserRoles").select2({
    //    placeholder: 'Select a Role',
    //    allowClear: true
    //});
}


function BindContacts() {
    var resultType = CallController('/Contact/GetAllContacts', 'Get', null);
    item_UserListSelect = document.getElementById('Contacts');
    $('#Contacts').empty();
    $.each(resultType, function (i, item) {
        item_UserListSelect.options[item_UserListSelect.options.length] = new Option(item.Name, item.Id);
    });
}
function EditUser(id) {
    var request = {
        Id: id
    };
    SetUserView();
    BindUserRoles();
    var result = CallController('/Settings/GetUserByID', 'Get', request);
    $("#Id").val(id);
    $("#Username").val(result.Username);
    //$("#Username").prop("readonly", true);
    $("#FirstName").val(result.FirstName);
    $("#LastName").val(result.LastName);
    $("#Email").val(result.Email);
    $("#Phone").val(result.Phone);
    $("#Mobile").val(result.Mobile);
    if (result.Roles != null) {
        //$('#UserRoles').select2().select2('val', result.Roles);
        $('#UserRoles').selectpicker('val', result.Roles);

    }

    $("#dialog-title").text("Edit User");
    openNav('task-side-wrap');
}

function ClearFields() {
    $("#Id").val('');
    $("#Name").val('');
}

function ClearUserFields(isContact) {
    BindUserRoles();
    $("#Id").val('');
    $("#Username").val('');
    $("#FirstName").val('');
    $("#LastName").val('');
    $("#Email").val('');
    $("#Phone").val('');
    $("#Mobile").val('');
    $("#Password").val('');
    $("#ConfirmPassword").val('');
    $("#dialog-title").text("Add User");
    if (isContact) {
        BindContacts();
        SetContactView();
    }
    else {
        SetUserView();
    }
}

function SetContactView() {
    $('#UserRoles').select2().select2('val', 2);
    $("#Contacts").val('');
    $("#divContacts").show();
    //$("#divFirstName").hide();
    //$("#divLastName").hide();
    //$("#divEmail").hide();
    //$("#divPhone").hide();
    //$("#divMobile").hide();
}

function SetUserView() {
    $("#divContacts").hide();
    //$("#divFirstName").show();
    //$("#divLastName").show();
    //$("#divEmail").show();
    //$("#divPhone").show();
    //$("#divMobile").show();
}

function ClosePopup(title) {
    closeNav("task-side-wrap");
    $("#dialog-title").text(title);
}

$('#Contacts').on('change', function () {
    var request = { id: $('#Contacts').val() }
    var result = CallController('/Contact/GetContact', 'Get', request);
    $("#FirstName").val(result[0].FirstName);
    $("#LastName").val(result[0].LastName);
    $("#Email").val(result[0].Email);
    $("#Phone").val(result[0].Phone);
});

function ValidateAndSave() {
    var frmMain = $('input[name="Name"]').closest("form");
    $.validator.unobtrusive.parse(frmMain);
    if (frmMain.valid()) {
        SaveChanges();
    }
}


function ValidateAndSaveUser() {
    var frmMain = $('input[name="Username"]').closest("form");
    $.validator.unobtrusive.parse(frmMain);
    if (frmMain.valid()) {
        var isNullContact = $('#divContacts').is(":visible") && $('#Contacts').val() == null;
        if ($('#UserRoles').val() == null || isNullContact) {
            if (isNullContact) {
                ModelErrorMessage('#Message', '', 'Please select Contact.');
            }
            else {
                ModelErrorMessage('#Message', '', 'Please select atleast one Role.');
            }
            return false;
        }
        else {
            SaveUser();
        }
    }
}

function SaveChanges() {
    var request = {
        Id: $('#Id').val(),
        Name: $('#Name').val(),
    }
    var result = CallController('/Settings/AddEditRole', 'Post', request);
    if (typeof result != undefined && result.HttpCode != "200") {
        ErrorMessage('', result.Message);
        return;
    } else {
        RefreshGrid('A', false);
    }
}

function SaveUser() {

    var request = {
        Id: $('#Id').val(),
        Username: $('#Username').val(),
        FirstName: $('#FirstName').val(),
        LastName: $('#LastName').val(),
        Email: $('#Email').val(),
        Phone: $('#Phone').val(),
        Mobile: $('#Mobile').val(),
        Password: $('#Password').val(),
        ConfirmPassword: $('#ConfirmPassword').val(),
        Roles: $('#UserRoles').val()
    }
    var result = CallController('/Settings/AddEditUser', 'Post', request);
    debugger;
    if (typeof result != undefined && !result.Succeeded) {
        ModelErrorMessage('#Message', '', result.Errors[0]);
        return;
    } else {
        RefreshGrid('A', true);
    }
}

function RefreshGrid(prefix, isUserGrid) {
    SuccessMessage(prefix, '');
    if (isUserGrid) {
        BindUsers();
    }
    else {
        BindRoles();
    }
    ClosePopup();
}