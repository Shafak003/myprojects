﻿var firmkendoGrid;
var IsSCAdmin, IsSCManager, IsSCTeamLead, IsSCAccountant;
function BindFirms(isScUser) {
    //Table Schema

    //Rights
    var ArchiveRights = (!IsSCAdmin && (IsSCManager || IsSCTeamLead || IsSCSrAccountant || IsSCAccountant) ? " hidden " : "");
    var EditRights = (!IsSCAdmin && (IsSCManager || IsSCTeamLead || IsSCSrAccountant || IsSCAccountant) ? " hidden " : "");
    var DeleteRights = (!IsSCAdmin && (IsSCManager || IsSCTeamLead || IsSCSrAccountant || IsSCAccountant) ? " hidden " : "");
    var LogoRights = IsSCAccountant ? " hidden " : "";
    //Rights

    var result = CallController('/Firm/FirmsList', 'Get', null);
    if (!firmkendoGrid) {
        $("#firm-recent").html("");
        var columns = null;
        if (isScUser) {
            columns = [
                {
                    field: "Logo",
                    title: "Logo",
                    template: "<img class='img img-responsive img-circle' width='50px' src='" + siteRoot + "/Content/LogoUploads/#if(Logo == null || Logo == '' || Logo == 'NULL') {##='noimage.png'##} else {##=Logo##}#' />",
                    width: 75
                },
                {
                    field: "Name",
                    title: "Firm Name",
                    width: 100
                },
                {
                    field: "ClientCount",
                    title: "Number of Clients",
                    width: 140
                },
                {
                    field: "ServiceName",
                    title: "Service(s)",
                    width: 200
                },
                {
                    field: "SCManager",
                    title: "Manager",
                    width: 130
                },
                {
                    field: "SCTeamLead",
                    title: "TeamLead",
                    width: 150
                },
                {
                    field: "SCAccountant",
                    title: "Accountant",
                    width: 150
                },
                {
                    width: 130,
                    template: "<div class='action-tools'><a title='Upload Logo' href='javascript:ShowModalUpload(#:FirmId #);' class='btn btn-default" + LogoRights + " btn-xs btn-font btn-Delete'><i class='fa fa-file-video-o'></i></a><a href='" + siteRoot + "/Fund/Recent/#:FirmId #' class='btn btn-default" + ArchiveRights + " btn-xs btn-font btn-View'><i class='fa fa-user'></i></a>" +
                                "&nbsp;<a href='" + siteRoot + "/Firm/Wizard/#: FirmId #' class='btn btn-success" + EditRights + " btn-xs btn-font btn-primary'><i class='fa fa-edit'></i></a>" +
                                "&nbsp;<a href='javascript:DeleteFirm(#:FirmId #);' class='btn btn-danger" + DeleteRights + " btn-xs btn-font btn-Delete'><i class='fa fa-trash-o'></i></a></div>",
                }
            ];
        }
        else {
            columns = [

                {
                    field: "Name",
                    title: "Firm Name",
                },
                 {
                     field: "ClientCount",
                     title: "Number of Clients",
                 },
                {
                    width: 300,
                    template: "<div class='action-tools'> <a href='" + siteRoot + "/Fund/Recent/#:FirmId #' class='btn btn-default btn-xs btn-font btn-View'><i class='fa fa-user'></i></a>" +
                                "</div>",
                }];
        }
        firmkendoGrid = $('#firm-recent').kendoGrid({
            dataSource: {
                data: result,
                pageSize: 18
            },
            height: 850,
            sortable: true,
            filterable: {
                operators: {
                    string: {
                        contains: "Contains",
                        doesnotcontain: "Does not contain",
                        eq: "Equal to",
                        neq: "Not equal to",
                        startswith: "Starts with",
                        endswith: "Ends with",
                        special: "Contains special characters",
                        isempty: "Is empty",
                        isnotempty: "Is not empty",
                        isnotnull: "Is not null",
                        isnull: "Is null",
                        neq: "Not equal"
                    },
                    number: {
                        eq: "Equal to",
                        neq: "Not equal to",
                        gt: "Greater than",
                        gte: "Greater than or equal",
                        lt: "Less than",
                        lte: "Less than or equal",
                        isnotnull: "Is not null",
                        isnull: "Is null"
                    }
                }
            },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            columns: columns
        });
    }
    else {
        firmkendoGrid.data('kendoGrid').dataSource.data(result);
        firmkendoGrid.data('kendoGrid').refresh();

    }

}

function DeleteFirm(id) {
    bootbox.confirm({
        message: "Are you sure you want to delete?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                var request = { id: id };
                var result = CallController('/Firm/Delete', 'Get', request);
                SuccessMessage('D', '');
                BindFirms(true);
            }
            else
                return;
        }
    });
}


function ShowModalUpload(firmid) {
    $("#logo_firmid").val(firmid);



    $("#UploadModal").modal("show");
}
