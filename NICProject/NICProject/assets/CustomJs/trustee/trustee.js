var TruesteeGrid;
TrusteeFile = function () {
    var isEditing = null;
    var trusteeTypes = null;
    var mainFormID = null;
    var isAddMode = false;
    var FormType_id = "";
    var FundRequestId = null;
    var Area = "";
    //Trustee Datatable
    //var oTable = $('#trustee-table').dataTable({
    //    "paging": true,
    //    "filter": true,
    //    "pageLength": 25,
    //    "aoColumns": [
    //        null,
    //        null,
    //        null,
    //        null,
    //        null,
    //        null,
    //        { "bSortable": false }
    //    ]
    //});

    function AttachTrustee() {

        var trusteeId = $('#trustee_id').val();
        if ((TrusteeFile.mainFormID > 0 && TrusteeFile.mainFormID != null && TrusteeFile.mainFormID != undefined) || (TrusteeFile.FundRequestId > 0 && TrusteeFile.FundRequestId != null && TrusteeFile.FundRequestId != undefined)) {
            if (trusteeId != undefined && trusteeId > 0) {
                var request = {
                    TrustId: trusteeId, MainId: mainFormID, FormType_id: TrusteeFile.FormType_id, FundRequestId: FundRequestId
                }
                var result = CallController('/Trustee/TrusteeAttach', 'Post', request);
                if (typeof result != undefined && result.HttpCode != "200") {
                    ModelErrorMessage('#Message', '', result.Message);
                    return;
                } else {
                    //$('#TrsuteeAttachModal').modal('hide');
                    closeNav('TrsuteeAttachModal');
                    GetValues();
                    SuccessMessage('U', '');
                }
            }
        }
        else {
            {
                ModelErrorMessage('#Message', '', 'Mandatory fields missing on Main Tab.');
            }
        }

    }

    function GetValues() {
        GetInitialValues();

        if ($("#FundTypeId option:selected").text() == 'SMSF - Individual') {
            $('#DivACN').hide();
        } else {
            $('#DivACN').show();
        }

        if ($('#FundId')) {
            TrusteeFile.mainFormID = $('#FundId').val();
        }
        else {
            TrusteeFile.FundRequestId = $('#FundRequestId').val();
        }
        if (!TrusteeFile.FundRequestId) {
            TrusteeFile.FormType_id = $("#FundTypeId option:selected").text();
        }
        //$("#trustee-table > tbody:last").children().remove();
        //oTable.fnClearTable();
        var request = {
            ID: TrusteeFile.mainFormID, FormType: TrusteeFile.FormType_id, FundRequestId: TrusteeFile.FundRequestId, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual',
            AccessArea: TrusteeFile.Area
        }
        var result = CallController('/Trustee/TrusteePartial', 'Get', request);

        //this means its from add/edit request page.
        if (TrusteeFile.Area != null && TrusteeFile.Area != "" && TrusteeFile.Area == "AddUpdateRequest") {
            if (result.length == 0) {
                $('#btnSubmit').hide();
                $('#btnCreateTrustee').prop('disabled', true);
            }
            else {
                $('#btnSubmit').show();
                $('#btnCreateTrustee').prop('disabled', false);
            }

        }
        var icon = '';
        icon = '<i class="glyphicon glyphicon-ok"></i>';
        //$.each(result, function (i, item) {
        //    if (item.ATF) {
        //        icon = '<i class="glyphicon glyphicon-ok"></i>';
        //    }
        //    else
        //        icon = '';
        //    var aiNew = oTable.fnAddData([
        //     item.TrustName, item.TFN, item.ACN, item.ABN, icon, item.ATFName,
        //    "<a href='#'  class='btn btn-info edit' onclick='TrusteeFile.GetEditValues(" + item.TrustId + ");'><i class='fa fa-edit'></i>Edit</a> <a href='#' class='btn btn-danger delete' onclick='TrusteeFile.DeleteValues(" + item.TrustId + ");'><i class='fa fa-trash-o'></i>Delete</a>"
        //    ]);

        //});

        if (!TruesteeGrid) {
            $("#trustee-table").html("");
            TruesteeGrid = $("#trustee-table").kendoGrid({
                dataSource: {
                    data: result,
                    pageSize: 10
                },
                //height: 950,
                filterable: true,
                sortable: true,
                pageable: {
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                    {
                        field: "TrustName",
                        title: "Trustee Name"
                    }, {
                        field: "ACN",
                        title: "ACN",
                        width: 150
                    }, {
                        template: "<a class='btn btn-success btn-xs btn-font btn-primary' onclick='TrusteeFile.GetEditValues(#:TrustId#);'><i class='fa fa-edit'></i></a>"
                        + "&nbsp;<a class='btn btn-danger btn-xs btn-font btn-Delete' onclick='TrusteeFile.DeleteValues(#:TrustId#);'><i class='fa fa-trash-o'></i></a>"
                    }]
            });
        }
        else {
            TruesteeGrid.data('kendoGrid').dataSource.data(result);
            TruesteeGrid.data('kendoGrid').refresh();
        }


    }

    function GetCompanyName() {
        var request = { id: $('#ACN').val() }
        var result = CallController('/Trustee/GetCompanyName', 'Get', request);
        if (result != null) {
            if (result.Message != null) {
                $('#TrustName').val(result.Message);
            } else {
                $('#TrustName').val(result.Name);
            }

        }
    }

    function GetEditValues(TrustId) {

        GetInitialValues();
        var request = {
            ID: TrustId, FormType: TrusteeFile.FormType_id, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual'
        }
        var result = CallController('/Trustee/TrusteePartialEdit', 'Get', request);
        $('#TrustId').val(result.TrustId);
        $('#TrustName').val(result.TrustName);
        $('#trusteemain #TFN').val(result.TFN);
        $('#ACN').val(result.ACN);
        $('#ABN').val(result.ABN);
        $('#ATF').val(result.ATF);
        $('#ATFName').val(result.ATFName);

        openNav('TrusteePartialModal');
        //$('#TrusteePartialModal').modal('show');

    }

    function GetInitialValues() {

        $('#TrustId').val('0');
        $('#TrustName').val('');
        $('#TFN').val('');
        $('#ACN').val('');
        $('#ABN').val('');
        $('#ATF').val(false);
        $('#ATFName').val('');

        // Get All Trustee's
        GetPopupValues();

        // Tab Initial Setting
        $('#trusteemain').addClass('active');
        $('#trustee-contact-tab').removeClass('active');
        $('#trustee-address-tab').removeClass('active');
        $('#trusteeaddress').removeClass('active');
        $('#trusteecontact').removeClass('active');
        $('#trustee-main-tab').addClass('active');

        $('#trustee_id').on('change', function (e, clickedIndex) {

            var selected = $('#trustee_id').val();
            //if (selected > 0) {
            SetExistingValue(selected, clickedIndex);
            // }
        });
    }

    function SetExistingValue(selected, clickedIndex) {
        if (selected > 0) {
            var request = {
                ID: selected, FormType: TrusteeFile.FormType_id, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual'
            }
            var result = CallController('/Trustee/TrusteePartialEdit', 'Get', request);
            $('#TrustId').val(0);
            $('#TrustName').val(result.TrustName);
            $('#trusteemain #TFN').val(result.TFN);
            $('#ACN').val(result.ACN);
            $('#ABN').val(result.ABN);
            $('#ATF').val(result.ATF);
            $('#ATFName').val(result.ATFName);
        }
        else {
            $('#TrustName').val($("#trustee_id option:selected").text());
        }
    }

    function GetPopupValues() {

        // For Trustee Combo
        var request = {
            ID: TrusteeFile.mainFormID, FormType: TrusteeFile.FormType_id, FundRequestId: TrusteeFile.FundRequestId, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual',
            AccessArea: TrusteeFile.Area
        }
        var resultTrustee = CallController('/Trustee/TrusteeGetAll', 'Get', request);
        trusteeSelect = document.getElementById('trustee_id');
        $('#trustee_id').empty();
        if (resultTrustee != null) {
            $.each(resultTrustee, function (i, item) {
                trusteeSelect.options[trusteeSelect.options.length] = new Option(item.TrustName, item.TrustId);
            });
        }
        $('#trustee_id').selectpicker('refresh');
    }


    function DeleteValues(Id) {
        bootbox.confirm({
            message: "Are you sure you want to delete?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var request = {
                        TrustId: Id, FormType_id: TrusteeFile.FormType_id, MainId: mainFormID, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual'
                    }
                    CallController('/Trustee/TrusteePartialDelete', 'Post', request);
                    SuccessMessage('D', '');
                    GetValues();
                }
                else
                    return;
            }
        });
    }

    function ValidateAndSave() {
        var frmTrustee = $('input[name="TrustName"]').closest("form");
        $.validator.unobtrusive.parse(frmTrustee);
        //frmContact.validate();
        if (frmTrustee.validate()) {
            //if (frmTrustee.valid()) {
            AddUpdate();
        }
    }

    function AddUpdate() {
        debugger;
        if (!TrusteeFile.FundRequestId) {
            TrusteeFile.FormType_id = $("#FundTypeId option:selected").text();
        }
        if ((TrusteeFile.mainFormID > 0 && TrusteeFile.mainFormID != null && TrusteeFile.mainFormID != undefined) || (TrusteeFile.FundRequestId > 0 && TrusteeFile.FundRequestId != null && TrusteeFile.FundRequestId != undefined)) {
            var request = {
                TrustId: $('#TrustId').val(), TrustName: $('#TrustName').val(), TFN: $('#trusteemain #TFN').val(), ACN: $('#ACN').val(), ABN: $('#ABN').val(), ATF: $('#ATF').is(":checked"), ATFName: $('#ATFName').val(),
                MainId: TrusteeFile.mainFormID, FormType_id: TrusteeFile.FormType_id, FundRequestId: TrusteeFile.FundRequestId, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual',
                AccessArea: TrusteeFile.Area
            }

            //this means its from add/edit request page.
            if (TrusteeFile.Area != null && TrusteeFile.Area != "" && TrusteeFile.Area == "AddUpdateRequest") {
                request.MainId = null;
            }


            isAddMode = false;
            var result = CallController('/Trustee/TrusteePartial', 'Post', request);
            if (typeof result != undefined && result.HttpCode != "200") {
                ModelErrorMessage('#Message', '', result.Message);
                return;
            } else {
                GetValues();
                if (result.OperationStatuses.length > 0) {
                    var CurId = result.OperationStatuses[0].UniqueKey;
                    GetEditValues(CurId);
                }
                //$('#TrusteePartialModal').modal('hide');
                //closeNav('TrusteePartialModal');
                SuccessMessage('U', '');
            }
        } else {
            ModelErrorMessage('#Message', '', 'Mandatory fields missing on Main Tab.');
        }
    }

    return {
        FormType_id: FormType_id, GetValues: GetValues,
        GetPopupValues: GetPopupValues, AttachTrustee: AttachTrustee,
        AddUpdate: AddUpdate, ValidateAndSave: ValidateAndSave, GetInitialValues: GetInitialValues, GetEditValues: GetEditValues,
        DeleteValues: DeleteValues, mainFormID: mainFormID, GetCompanyName: GetCompanyName
    }
}();