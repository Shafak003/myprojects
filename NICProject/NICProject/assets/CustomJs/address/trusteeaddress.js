var TAkendoGrid;
TrusteeAddressFile = function () {
    var isEditing = null;
    var addressTypes = null;
    var mainFormID = null;
    var isAddMode = false;
    var FormType_id = "";
    var FundRequestId = null;


    function AttachAddress() {

        var selectedValue = $("#AttachAddressTypeId :selected").text();
        if (!ValidateData(selectedValue)) {
            return;
        }

        var addressType = $('#AttachAddressTypeId').val();
        var addressId = $('#address_id').val();
        if ((TrusteeAddressFile.mainFormID > 0 && TrusteeAddressFile.mainFormID != null && TrusteeAddressFile.mainFormID != undefined) || (TrusteeAddressFile.FundRequestId > 0 && TrusteeAddressFile.FundRequestId != null && TrusteeAddressFile.FundRequestId != undefined)) {
            if (addressId != undefined && addressId > 0) {
                var request = {
                    AddressTypeId: addressType, AddressId: addressId, Main_id: TrusteeAddressFile.mainFormID, id: 0, FormType_id: TrusteeAddressFile.FormType_id, FundRequestId: FundRequestId
                }
                var result = CallController('/Address/AddressAttach', 'Post', request);
                if (typeof result != undefined && result.HttpCode != "200") {
                    ModelErrorMessage('#Message', '', result.Message);
                    return;
                } else {
                    //$('#AddressAttachModal').modal('hide');
                    closeNav('AddressAttachModal');
                    GetValues();
                    SuccessMessage('U', '');
                }
            } else {
                ModelErrorMessage('#Message', '', 'Must Select Address before Attachment.');
            }
        } else {
            ModelErrorMessage('#Message', '', 'Mandatory fields missing on Main Tab.');
        }
    }

    function GetValues() {
        GetInitialValues();
        var request = { ID: (TrusteeAddressFile.mainFormID > 0 ? TrusteeAddressFile.mainFormID : TrusteeAddressFile.FundRequestId), FormType: TrusteeAddressFile.FormType_id, FundRequestId: TrusteeAddressFile.FundRequestId, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual' };
        var result = CallController('/Address/AddressPartial', 'Get', request);
        TrusteeAddressFile.addressTypes = CallController('/Address/AddressPartialGetTypes', 'Get', null);
        //var existingGrid = $('#trustee-address-table').data('kendoGrid');

        if (!TAkendoGrid) {
            TAkendoGrid = $("#trustee-address-table").kendoGrid({
                dataSource: {
                    data: result,
                    pageSize: 10
                },
                //height: 950,
                filterable: true,
                sortable: true,
                pageable: {
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                    {
                        //field: "AddressTypeId",
                        template: "#= TrusteeAddressFile.formatAddressType(AddressTypeId) #",
                        //template: '#= Simple(OffDuty,OnDuty) #',
                        title: "Address Type",
                        width: 100
                    },
                {
                    field: "AddressLine1",
                    title: "Address Line 1",
                    width: 150
                }, {
                    field: "State",
                    title: "State",
                    width: 100
                }, {
                    field: "Country",
                    title: "Country",
                    width: 100
                }
                    , {
                        template: "<div class='action-tools'><a class='btn btn-success btn-xs btn-font btn-primary' onclick='TrusteeAddressFile.GetEditValues(#: id #,#: AddressId #);'><i class='fa fa-edit'></i></a>" +
                        "&nbsp;&nbsp;<a class='btn btn-danger btn-xs btn-font btn-Delete' onclick='TrusteeAddressFile.DeleteValues(#: id #);'><i class='fa fa-trash-o'></i></a></div>"
                    }]
            });
        } else {
            TAkendoGrid.data('kendoGrid').dataSource.data(result);
            TAkendoGrid.data('kendoGrid').refresh();
        }



    }

    function GetEditValues(Id, AddressId) {

        GetInitialValues();
        var request = { ID: Id, FormType: TrusteeAddressFile.FormType_id, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual' };
        var result = CallController('/Address/AddressPartialEdit', 'Get', request);
        $('#TrusteeAddressPartialModal #AddressId').val(result.AddressId);
        $('#TrusteeAddressPartialModal #AddressLine1').val(result.AddressLine1);
        $('#TrusteeAddressPartialModal #AddressLine2').val(result.AddressLine2);
        $('#TrusteeAddressPartialModal #City').val(result.City);
        $('#TrusteeAddressPartialModal #State').val(result.State);
        $('#TrusteeAddressPartialModal #PostCode').val(result.PostCode);
        $('#TrusteeAddressPartialModal #Country').val(result.Country);
        $('#TrusteeAddressPartialModal #id').val(result.id);
        $(' #TrusteeAddressPartialModal #AddressTypeId_Old').val(result.AddressTypeId);

        $("#TrusteeAddressPartialModal #AddressTypeId option").each(function () {
            if ($(this).val() == result.AddressTypeId) {
                $(this).attr('selected', 'selected');
                $('#TrusteeAddressPartialModal #select2-AddressTypeId-container').text($("#TrusteeAddressPartialModal #AddressTypeId :selected").text());
            }
        });
        ShowAddressFinder(false)
        //$('#AddressPartialModal').modal('show');
        openNav("TrusteeAddressPartialModal");

    }


    function ShowAddressFinder(flag) {
        if (flag) {
            $(".visible").hide();
            $("#TrusteeAddressPartialModal .txtMailingAddress").show();
            $('#TrusteeAddressPartialModal #showTA').prop('checked', false);
        }
        else {
            $(".visible").show();
            $("#TrusteeAddressPartialModal .txtMailingAddress").hide();
            $('#TrusteeAddressPartialModal #showTA').prop('checked', true);
        }
    }

    function ClearFields() {
        ShowAddressFinder(false);
        $("#TrusteeAddressPartialModal #address_fieldTA").val("");
        $('#TrusteeAddressPartialModal #AddressId').val('0');
        $('#TrusteeAddressPartialModal #AddressLine1').val('');
        $('#TrusteeAddressPartialModal #AddressLine2').val('');
        $('#TrusteeAddressPartialModal #City').val('');
        $('#TrusteeAddressPartialModal #State').val('');
        $('#TrusteeAddressPartialModal #PostCode').val('');
        $('#TrusteeAddressPartialModal #Country').val('');
        $('#TrusteeAddressPartialModal #id').val('0');
    }

    function GetInitialValues() {
        ClearFields();

        TrusteeAddressFile.FundRequestId = $('#FundRequestId').val();
        TrusteeAddressFile.mainFormID = $('#TrustId').val();
        TrusteeAddressFile.FormType_id = 'Trustee';

        // For Address Type Combo
        var resultType = CallController('/Address/AddressPartialGetTypes', 'Get', null);
        addressTypeSelect = document.querySelector("#TrusteeAddressPartialModal #AddressTypeId");
        // Previous
        $('#TrusteeAddressPartialModal #AddressTypeId').empty();
        $.each(resultType, function (i, item) {
            addressTypeSelect.options[addressTypeSelect.options.length] = new Option(item.Name, item.Id);
        });
        $('#TrusteeAddressPartialModal #AddressTypeId').selectpicker('refresh');

    }

    function GetPopupValues() {
        // For Address Type Combo
        var resultType = CallController('/Address/AddressPartialGetTypes', 'Get', null);
        addressTypeSelect = document.getElementById('AttachAddressTypeId');
        $('#AttachAddressTypeId').empty();
        console.log("Address Type Combo");
        console.log(resultType);
        $.each(resultType, function (i, item) {
            addressTypeSelect.options[addressTypeSelect.options.length] = new Option(item.Name, item.Id);
        });

        // For Address Combo
        var resultAddress = CallController('/Address/AddressGetAll', 'Get', null);
        addressSelect = document.getElementById('address_id');
        $('#address_id').empty();
        console.log("Address Combo");
        console.log(resultAddress);
        $.each(resultAddress, function (i, item) {
            addressSelect.options[addressSelect.options.length] = new Option(item.Name, item.Id);
        });

        //$("#AttachAddressTypeId").select2({ dropdownParent: $("#AddressAttachModal") });
        //$("#address_id").select2({ dropdownParent: $("#AddressAttachModal") });
    }


    // Format Address Type Combo
    function formatAddressType(id) {
        var value = null;
        for (var i = 0, iLen = TrusteeAddressFile.addressTypes.length; i < iLen; i++) {
            if (TrusteeAddressFile.addressTypes[i].Id == id) {
                value = TrusteeAddressFile.addressTypes[i].Name;
            }
        }
        return value;
    }

    function DeleteValues(Id) {
        bootbox.confirm({
            message: "Are you sure you want to delete?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var request = { ID: Id, FormType: TrusteeAddressFile.FormType_id, IsIndividual: $("#FundTypeId option:selected").text() == 'SMSF - Individual' };
                    CallController('/Address/AddressPartialDelete', 'Post', request);
                    SuccessMessage('D', '');
                    GetValues();
                }
                else
                    return;
            }
        });
    }

    function ValidateData(selectedValue) {
        // Validate Address Type if Exist
        var isExist = false;
        $('#trustee-address-table > tbody  > tr').each(function () {
            if (this.cells[0].innerHTML == selectedValue) {
                ModelErrorMessage('#Message', '', 'Address Type Already Exist.');
                isExist = false;
                return isExist;
            } else {
                isExist = true;
            }
        });
        return isExist;
    }

    function ValidateAndSave() {
        if ($('#frmTrusteeAddress').valid()) {
            AddUpdate();
        }
    }

    function AddUpdate() {
        var selectedValue = $("#TrusteeAddressPartialModal #AddressTypeId :selected").text();
        var newValue = $('#TrusteeAddressPartialModal #AddressTypeId').val();
        var oldValue = $('#TrusteeAddressPartialModal #AddressTypeId_Old').val();
        //if (oldValue != newValue && !ValidateData(selectedValue)) {
        //    return;
        //}

        if ((TrusteeAddressFile.mainFormID > 0 && TrusteeAddressFile.mainFormID != null && TrusteeAddressFile.mainFormID != undefined) || (TrusteeAddressFile.FundRequestId > 0 && TrusteeAddressFile.FundRequestId != null && TrusteeAddressFile.FundRequestId != undefined)) {
            var request = {
                FundRequestId: TrusteeAddressFile.FundRequestId,
                AddressTypeId: $('#TrusteeAddressPartialModal #AddressTypeId').val(), AddressId: $('#TrusteeAddressPartialModal #AddressId').val(), AddressLine1: $('#TrusteeAddressPartialModal #AddressLine1').val(), AddressLine2: $('#TrusteeAddressPartialModal #AddressLine2').val(), City: $('#TrusteeAddressPartialModal #City').val(),
                State: $('#TrusteeAddressPartialModal #State').val(), PostCode: $('#TrusteeAddressPartialModal #PostCode').val(), Country: $('#TrusteeAddressPartialModal #Country').val(), id: $('#TrusteeAddressPartialModal #id').val(), Main_id: TrusteeAddressFile.mainFormID, FormType_id: TrusteeAddressFile.FormType_id
            }
            isAddMode = false;
            var result = CallController('/Address/AddressPartial', 'Post', request);
            if (typeof result != undefined && result.HttpCode != "200") {
                ModelErrorMessage('#TrusteeAddressPartialModal #Message', '', result.Message);
                return;
            } else {
                //$('#AddressPartialModal').modal('hide');
                closeNav('TrusteeAddressPartialModal');
                GetValues();
                SuccessMessage('U', '');
            }
        } else {
            ModelErrorMessage('#Message', '', 'Mandatory fields missing on Main Tab.');
        }
    }

    return {
        FormType_id: FormType_id, GetValues: GetValues,
        GetPopupValues: GetPopupValues, AttachAddress: AttachAddress,
        AddUpdate: AddUpdate, ValidateAndSave: ValidateAndSave, GetInitialValues: GetInitialValues, GetEditValues: GetEditValues,
        DeleteValues: DeleteValues, mainFormID: mainFormID, formatAddressType: formatAddressType, FundRequestId: FundRequestId
    }
}();



(function () {
    var widget, initAF = function () {
        widget = new AddressFinder.Widget(
            document.getElementById('address_fieldTA'),
            AddressFinderAccessKey,
            'AU',
            { show_locations: false }
        );

        widget.on("result:select", function (fullAddress, metaData) {
            $('#TrusteeAddressPartialModal #AddressLine1').val(metaData.address_line_1);
            $('#TrusteeAddressPartialModal #AddressLine2').val(metaData.address_line_2);
            $('#TrusteeAddressPartialModal #City').val(metaData.locality_name);
            $('#TrusteeAddressPartialModal #State').val(metaData.state_territory);
            $('#TrusteeAddressPartialModal #PostCode').val(metaData.postcode);
            $('#TrusteeAddressPartialModal #Country').val("Australia");
            $('.af_list').hide();
        });
    };

    $(document).ready(function () {
        $.getScript('https://api.addressfinder.io/assets/v3/widget.js', initAF);

        $("#TrusteeAddressPartialModal #showTA").click(function () {
            var val = $('#TrusteeAddressPartialModal #showTA').is(":checked");
            if (val == true) {
                $("#TrusteeAddressPartialModal .visible").show();
                $("#TrusteeAddressPartialModal .txtMailingAddress").hide();
            } else {
                $("#TrusteeAddressPartialModal .visible").hide();
                $("#TrusteeAddressPartialModal .txtMailingAddress").show();
            }
        });

    });

})();
