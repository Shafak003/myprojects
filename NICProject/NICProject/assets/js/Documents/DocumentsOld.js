﻿var oTable = $('#documents-library').dataTable({
    "paging": true,
    "filter": true,
    "pageLength": 25,
    "aoColumns": [
        null,
        null,
        null,
        null,
        { "bSortable": false }
    ]
});

function BindDocuments() {
    //Table Schema
    var tId = $('#TaskId').val();
    var isFirst = true;
    $("#documents-library > tbody:last").children().remove();
    oTable.fnClearTable();
    var request = { id: $('#Clients').val(), taskId: tId }
    var result = CallController('/Documents/GetDocuments', 'Get', request);
    if (tId == "" || tId == "0")
    {
        isFirst = false;
    }
    $.each(result, function (i, item) {
        if (isFirst) {
            $("#TaskClientId").val(item.ClientId);
            var title = $("#library-title").text();
            $("#library-title").text(title + " (" + item.TaskName + ")");
            isFirst = false;
        }
        var aiNew = oTable.fnAddData([
         item.JobType, item.JobYear, item.Name, item.DocType,
        "<a target='_blank' class='btn btn-primary' href='/DocumentViewer/Viewer/" + item.DocumentId + "'><i class='pe-7s-portfolio'></i>View</a><a class='btn btn-primary' href='/TaskQuickView/DownloadFile/" + item.DocumentId + "'><i class='pe-7s-download'></i>Download</a> <a onclick='DeleteDocument(" + item.DocumentId + ");' class='btn btn-danger delete'><i class='fa fa-trash-o'></i>Delete</a>"
        ]);
    });
}

function BindCombo(id, url) {
    // For Contacts Type Combo
    var resultType = CallController(url, 'Get', null);
    $(id).empty();
    var options = "<option value='0'>-- Select --</option>";
    // options = "<option value='-1'>None</option>"; // uncomment if you want this item
    $.each(resultType, function (a, b) {
        options += "<option value='" + b.Id + "'>" + b.Name + "</option>";
    });
    $(id).html(options);
}

function BindDocumentViewer(Docid) {

    var request = { id: Docid }
    var result = CallController('/Documents/GetDocumentbyId', 'Get', request);

    var extension = result.Name.substr((result.Name.lastIndexOf('.') + 1));
    switch (extension.toLowerCase()) {
        case "doc":
        case "docx":
        case "xls":
        case "xlsx":
        case "pptx":
            var DynamicURL = 'https://s3-ap-southeast-2.amazonaws.com/devdocs4/' + result.DocumentPath;
            var GoogleURL = 'https://view.officeapps.live.com/op/view.aspx?src=' + DynamicURL + '&embedded=true';
            $('#DocView').show();
            $('#DocView').attr('src', GoogleURL);
            break;
        case "pdf":
        case "txt":
        case "rtf":
            var DynamicURL = 'https://s3-ap-southeast-2.amazonaws.com/devdocs4/' + result.DocumentPath;
            var GoogleURL = 'http://docs.google.com/viewer?url=' + DynamicURL + '&embedded=true';
            $('#DocView').show();
            $('#DocView').attr('src', GoogleURL);
            break;
        case "png":
        case "jpg":
        case "jpeg":
            var ImgURL = 'https://s3-ap-southeast-2.amazonaws.com/devdocs4/'+ result.DocumentPath;
            $('#ImgViewDiv').show();
            $('#ImgView').attr('src', ImgURL);
            break;
        
        default:
            alert('File Type is not compatible, Unknown extension ' + extension);
    }
}

function SelectedFileOpen(s, e) {
    var DocId = e.file.id.substring(e.file.id.indexOf('-') + 1, e.file.id.length);
    $('#documentopen').attr('href', '/DocumentViewer/Viewer/' + DocId);
    document.getElementById("documentopen").click();
}

function SelectedFileDownload(s, e) {
    var DocId = e.file.id.substring(e.file.id.indexOf('-') + 1, e.file.id.length);
    //window.location.href = 'http://' + window.location.host + '/TaskQuickView/DownloadFile/' + DocId;
    var S3path = "https://s3-ap-southeast-2.amazonaws.com/devdocs4/";
    var request = { id: DocId }
    var result = CallController('/Documents/GetDocumentbyId', 'Get', request);
    if (result != null) {
        S3path = S3path + result.DocumentPath
        $('#documentdownload').attr('href', S3path);
        document.getElementById("documentdownload").click();
    }
    
    e.stopPropagation();
}

function SelectedFileDelete(s, e) {
    
    var request = { Path: e.fullName };
    var result = CallController('/FileManager/DeleteDocumentbyId', 'Get', request);
    
    //e.stopPropagation();
}



function DownloadDocument(DocumentId) {
    var request = {
        Id: DocumentId
    };
    $.ajax(
        {
            type: 'Get',
            cache: false,
            async: false,
            url: '/TaskQuickView/DownloadFile',
            data: request,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert(errorThrown);
            },
            success: function (returnValue) {
                window.location = '/TaskQuickView/DownloadFile?Id=' + returnValue;
            }
        });

}

function DeleteDocument(id) {
    bootbox.confirm({
        message: "Are you sure you want to delete?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                var request = { id: id };
                var result = CallController('/Documents/DeleteDocument', 'Post', request);
                SuccessMessage('D', '');
                BindDocuments();
            }
            else
                return;
        }
    });
}

function UploadControl(Ref1, Ref2, Ref3, ViewType) {
    if (ViewType == "Manager-M") {
        debugger;
        var pathArray = Ref3.split('\\');
        if (pathArray.length > 3) {
            Ref3 = Ref3.replace(/\\/g, '-');
            Ref3 = Ref3.replace(/ /g, '+');
            var getUrl = window.location.origin + siteRoot + '/FileManager/FileUploader?Ref1=' + Ref1 + ' &Ref2=' + Ref2 + '&Ref3=' + Ref3 + '&ViewType=' + ViewType;
            var win = window.open(getUrl, '_blank', 'width=500,height=200');
            win.focus();
        } else {
            alert('You can not add document on this node.')
        }
    }
    
    if (ViewType == "Queries-M") {
        ValidateAndSave();
        if (Ref1 > 0) {
            var getUrl = window.location.origin + siteRoot + '/FileManager/FileUploader?Ref1=' + Ref1 + ' &Ref2=' + Ref2 + '&Ref3=' + Ref3 + '&ViewType=' + ViewType;
            var win = window.open(getUrl, '_blank', 'width=500,height=200');
            win.focus();
        } else {
            alert('You can not add document.')
        }
    }
    
    if (ViewType == "Task-M") {
        TemplateFile.AddUpdateTask();
        if (Ref1 > 0) {
            var getUrl = window.location.origin + siteRoot + '/FileManager/FileUploader?Ref1=' + Ref1 + ' &Ref2=' + Ref2 + '&Ref3=' + Ref3 + '&ViewType=' + ViewType;
            var win = window.open(getUrl, '_blank', 'width=500,height=200');
            win.focus();
        } else {
            alert('You can not add document.')
        }
    }
    if (ViewType == "Task-C") {
        jsComments.ValidateAndSave();
        Ref1 = $('#DetailId').val();
        ViewType = $('#ViewType').val() == 'Task' ? 'Task-C' : 'Queries-C'
        if (Ref1 > 0) {
            var getUrl = window.location.origin + siteRoot + '/FileManager/FileUploader?Ref1=' + Ref1 + ' &Ref2=' + Ref2 + '&Ref3=' + Ref3 + '&ViewType=' + ViewType;
            var win = window.open(getUrl, '_blank', 'width=500,height=200');
            win.focus();
        } else {
            alert('You can not add document.')
        }
    }
}