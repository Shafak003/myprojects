from django.conf.urls import url
from . import views

urlpatterns = [
           url(r'^register/$',views.index,name='index'),
           url(r'^user/create/$',views.create_user),
           url(r'^login/$', views.UserSingIn ),
           url(r'^homepage/$', views.homepage, name='homepage'),
           url(r'^ajax/validate_username/$', views.validate_username, name='validate_username'),
           url(r'^forgotpassword/$',views.forgotpass,name='forgotpass'),
           url(r'^passwordconfirmation/$',views.passconfm,name='passconfm'),
           # url(r'^activate_account/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
           # views.ActivateAccountView, name='activate_account'),
]