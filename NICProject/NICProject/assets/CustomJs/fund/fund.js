﻿var FundGrid;
var IsSCManager, IsSCTeamLead, IsSCAccountant;

function BindFunds(hideclass, disclass, a) {
    var ArchiveRights = (!IsSCAdmin && (!IsSCUser || IsSCManager || IsSCTeamLead || IsSCAccountant || IsSCSrSccountant) ? " hidden " : "");
    var EditRights = (!IsSCAdmin && (!IsSCUser ||  IsSCManager || IsSCTeamLead || IsSCAccountant || IsSCSrSccountant) ? " hidden " : "");
    var request = { Id: $('#FirmId').val() }
    var result = CallController('/Fund/Funds', 'Get', request);
    if (!FundGrid) {
        $("#fund-recent").html("");

        FundGrid = $('#fund-recent').kendoGrid({
            dataSource: {
                data: result,
                pageSize: 20
            },
            sortable: true,
            //filterable: true,
            filterable: {
                operators: {
                    string: {
                        contains: "Contains",
                        doesnotcontain: "Does not contain",
                        eq: "Equal to",
                        neq: "Not equal to",
                        startswith: "Starts with",
                        endswith: "Ends with",
                        special: "Contains special characters",
                        isempty: "Is empty",
                        isnotempty: "Is not empty",
                        isnotnull: "Is not null",
                        isnull: "Is null",
                        neq: "Not equal"
                    },
                    number: {
                        eq: "Equal to",
                        neq: "Not equal to",
                        gt: "Greater than",
                        gte: "Greater than or equal",
                        lt: "Less than",
                        lte: "Less than or equal",
                        isnotnull: "Is not null",
                        isnull: "Is null"
                    }
                }
            },
            pageable: {
                refresh: true,
                pageSizes: true,
                buttonCount: 5
            },
            columns: [
                {
                    field: "SelectedFirm",
                    title: "Firm Name",
                    width: 300
                },
                {
                    field: "FundName",
                    title: "Client Name",
                    width: 300
                },
                {
                    field: "SelectedFundType",
                    title: "Client Type",
                    width: 200
                },
                {
                    field: "FundDetails.ABN",
                    title: "ABN",
                    width: 150
                },
                 {
                     field: "Name",
                     title: "Assign To",
                     width: 150
                 },
                //{
                //    width: 150,
                //    title: "Fund Profile",
                //    template: "<div style='text-align:left;' class='action-tools'><a class='btn btn-primary btn-xs btn-font' href='" + siteRoot + "/DashboardViewer/Fund/#:FundId #'><i class='fa fa-address-book-o'></i></a></div>"
                //},
                //{
                //    field: "FundDetails.FundBalance",
                //    format: "{0:c2}",
                //    title: "Fund Balance",
                //    width: 150
                //},
                {
                    width: 150,
                    template: "<div class='action-tools'><a href='" + siteRoot + "/Jobs/Recent/#:FundId #' class='btn btn-default " + ArchiveRights + " btn-xs btn-font btn-View'><i class='fa fa-briefcase'></i></a>" +
                                "&nbsp;<a href='" + siteRoot + "/Fund/Wizard/#: FundId #' class='btn btn-success " + disclass + EditRights + " btn-xs btn-font btn-primary'><i class='fa fa-edit'></i></a>" +
                                "&nbsp;  <a href='javascript:DeleteFund(#:FundId #);' class='btn btn-danger " + a + "  btn-xs btn-font btn-Delete'><i class='fa fa-trash-o'></i></a></div>",
                }]
        });

    }
    else {
        FundGrid.data('kendoGrid').dataSource.data(result);
        FundGrid.data('kendoGrid').refresh();
    }

    //FundGrid.kendoTooltip({
    //    //filter: "td:nth-child(2)", //this filter selects the second column's cells
    //    position: "right",
    //    content: function (e) {
    //        var dataItem = FundGrid.data("kendoGrid").dataItem(e.target.closest("tr"));
    //        debugger;
    //        var content = dataItem.Text;
    //        return content;
    //    }
    //}).data("kendoTooltip");
}

function DeleteFund(id) {
    bootbox.confirm({
        message: "Are you sure you want to delete?",
        buttons: {
            confirm: {
                label: 'Yes',
                className: 'btn-success'
            },
            cancel: {
                label: 'No',
                className: 'btn-danger'
            }
        },
        callback: function (result) {
            if (result) {
                var request = { detailsId: id };
                var result = CallController('/Fund/Delete', 'Get', request);
                SuccessMessage('D', '');
                BindFunds();
            }
            else
                return;
        }
    });
}
