TemplateFile = function () {
    var isEditing = null;
    var addressTypes = null;
    var mainFormID = 0;
    var isAddMode = false;
    var FormType_id = "";


    function GetValues() {
        //window.location.reload();
        var request = null;
        if ($("#JobIdForTemplate").val() != "") {
            var request = { id: $("#JobIdForTemplate").val() };
        }

        var result = CallController('/Workflow/Refresh', 'Post', request, 'html');
        $('#divTaskTemplatePartial').html(result);
        //window.location.href = '/Workflow/Recent'
        jQuery.ready();
        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle="popover"]').popover({
            html: true,
            container: 'body',
            trigger: 'hover',
        });
    }

    function GetEditValues(TemplateId) {

        GetInitialValues();
        var request = { ID: TemplateId };
        var result = CallController('/Workflow/GetTemplateByID', 'Get', request);
        $("#name").val(result.Name);
        $('#notes').val(result.Notes);
        $('#TaskTemplateId').val(result.TaskTemplateId);
        $("#TemplateType option").each(function () {
            if ($(this).val() == result.TemplateType) {
                //$(this).attr('selected', 'selected');

                $("#TemplateType").selectpicker('val', result.TemplateType);
                //$('#select2-TemplateType-container').text($("#TemplateType :selected").text());
            }
        });
        //$('#TemplateAddModal').modal('show');
        openNav('TemplateAddModal');

    }

    function GetEditValuesTask(TaskId, TemplateId) {

        GetTaskInitialValues();
        var request = { ID: TaskId };
        var result = CallController('/Workflow/GetTaskByID', 'Get', request);

        $('#TaskId').val(result.TaskId);
        $('#taskDescription').val(result.Description);
        $('#task-name').val(result.Name);
        $('#CurrentTemplateId').val(TemplateId);
        $('#JobId').val(result.JobId);
        $('#CreatedBy').val(result.CreatedBy);
        $('#ModifiedBy').val(result.ModifiedBy);
        $('#IsTaskInstance').val(result.IsTaskInstance);
        $("input[name=Priority][value=" + result.Priority + "]").prop("checked", true);
        //var instance = $('#PercentComplete').data("ionRangeSlider");
        //instance.update({
        //    from: result.PercentComplete != null ? result.PercentComplete : 0
        //});
        $('#EstimatedHours').val(result.EstimatedHours);
        $('#EstimatedMinutes').val(result.EstimatedMinutes);
        var Percentage = result.PercentComplete != null ? result.PercentComplete : 0;
        $('#TaskProgress').html("Progress so far (" + Percentage + "%)");
        $('#PercentComplete').val(Percentage).change();

        if (result.JobId > 0) {
            $(".active").removeClass('active');
            $(".tab-pane fade active in").removeClass("tab-pane fade active in");

            $("#Activity1").removeClass("hide");
            $("#Activity1").addClass("active");
            $("#tab_content1").addClass("tab-pane fade active in");
        } else {
            $(".active").removeClass('active');
            $(".tab-pane fade active in").removeClass("tab-pane fade active in");

            $("#Activity1").addClass("hide");
            $("#Activity2").addClass("active");
            $("#tab_content2").addClass("tab-pane fade active in");
        }

        if (result.StartDate != null) {
            var date = new Date(parseInt(result.StartDate.substr(6)));
            var formattedDate = ("0" + (date.getMonth() + 1)).slice(-2) + "-" +
                  ("0" + date.getDate()).slice(-2) + "-" + date.getFullYear();
            $('#StartDate').val(formattedDate);
        }
        else {
            $('#StartDate').val('');
        }

        if (result.DueDate != null) {
            date = new Date(parseInt(result.DueDate.substr(6)));
            formattedDate = ("0" + (date.getMonth() + 1)).slice(-2) + "-" +
                  ("0" + date.getDate()).slice(-2) + "-" + date.getFullYear();
            $('#DueDate').val(formattedDate);
        }
        else {
            $('#DueDate').val('');
        }
        //$("#item_UserList option").each(function () {
        //    if ($(this).val() == result.UserIds) {
        //        $(this).attr('selected', 'selected');
        //        $('#select2-item_UserList-container').text($("#item_UserList :selected").text());
        //    }
        //});

        //$('#item_UserList').select2().select2('val', result.UserIds);
        $('#item_UserList').selectpicker('val', result.UserIds);


        // Get Values For Other Tabs
        jsPermission.GetEditValuesPermission();
        jsFollowers.GetEditValuesFollowers();

        $('#TaskAddModal').modal('show');
    }


    function ClearFields() {
        $("#name").val('');
        $('#notes').val('');

    }

    function GetInitialValues() {
        ClearFields();
        // For Template Type Combo
        //var resultType = CallController('/Workflow/GetAllTemplateTypes', 'Get', null);
        //TemplateTypeSelect = document.getElementById('TemplateType');
        //$('#TemplateType').empty();
        //$.each(resultType, function (i, item) {
        //    TemplateTypeSelect.options[TemplateTypeSelect.options.length] = new Option(item.Name, item.Id);
        //});
        //$('#TemplateType').selectpicker('refresh');

        //$("#TemplateType").select2({ dropdownParent: $("#TemplateAddModal") });
    }

    function GetTaskInitialValues(TemplateId) {
        $('#TaskId').val('');
        $('#taskDescription').val('');
        $('#task-name').val('');
        $('#TemplateTaskId').val('');
        $('#CurrentTemplateId').val(TemplateId);
        $('#JobId').val($("#JobIdForTemplate").val());
        $('#StartDate').val('');
        $('#DueDate').val('');
        $('#PercentComplete').val(0).change();
        // Is Public Default True
        $('#IsPublic').prop('checked', true);

        //Dropzone.forElement("#dropzoneForm").removeAllFiles(true);

        // For User Combo
        var resultType = CallController('/Workflow/GetAllUsers', 'Get', null);
        item_UserListSelect = document.getElementById('item_UserList');
        //$('#item_UserList').selectpicker();
        $('#item_UserList').empty();

        $.each(resultType, function (i, item) {
            item_UserListSelect.options[item_UserListSelect.options.length] = new Option(item.Name, item.Id);
        });
        $('#item_UserList').selectpicker('refresh');

        //$("#item_UserList").select2({
        //    dropdownParent: $("#TaskAddModal"), placeholder: 'Select a User',
        //    allowClear: true
        //});

        // For Menu Setting Default
        $(".active").removeClass('active');
        $(".tab-pane fade active in").removeClass("tab-pane fade active in");

        $("#Activity1").addClass("hide");
        $("#Activity2").addClass("active");
        $("#tab_content2").addClass("tab-pane fade active in");

    }

    function DeleteValues(Id) {
        bootbox.confirm({
            message: "Are you sure you want to delete?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var request = { Id: Id };
                    CallController('/Workflow/Delete', 'Post', request);
                    GetValues();
                    SuccessMessage('D', '');
                }
                else
                    return;
            }
        });
    }

    function DeleteValuesTask(Id, JobId) {
        bootbox.confirm({
            message: "Are you sure you want to delete?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var request = { Id: Id, jobId: JobId };
                    CallController('/Workflow/DeleteTask', 'Post', request);
                    GetValues();
                    SuccessMessage('D', '');
                }
                else
                    return;
            }
        });
    }

    function ValidateData(selectedValue) {
        // Validate Template Type if Exist
        return false;
    }

    function ValidateAndSave() {
        if ($('#name').val() == '' || $('#name').val() == undefined) {
            ModelErrorMessage('#Message', '', 'Please enter a Template name');
            return;
        }
        AddUpdate();
    }

    function AddUpdate() {
        var request = {
            TaskTemplateId: $('#TaskTemplateId').val(), Name: $('#name').val(), TemplateType: $('#TemplateType').val(), Notes: $('#notes').val()
        }
        isAddMode = false;
        var result = CallController('/Workflow/AddEdit', 'Post', request);
        //$('#TemplateAddModal').modal('hide');
        closeNav('TemplateAddModal');
        if (typeof result != undefined && result.HttpCode != "200") {
            ErrorMessage('', result.Message);
            return;
        } else {
            GetValues();
            SuccessMessage('A', '');
        }

    }

    function ValidateTask() {
        if ($('#task-name').val() == '' || $('#task-name').val() == undefined) {
            ModelErrorMessage('#Message', '', 'Please enter a Task name');
            return false;
        }

        if ($('#StartDate').val() > $('#DueDate').val() && $('#DueDate').val() != '' && $('#DueDate').val() != undefined) {
            ModelErrorMessage('#Message', '', 'Task start date can not be later than the task due date');
            return false;
        }
        if ((($('#EstimatedHours').val() * 60) + $('#EstimatedMinutes').val()) > 600000) {
            ModelErrorMessage('#Message', '', 'Estimated time can not exceed 600000 minutes. Please enter a lower estimate.');
            return false;
        }
        return true;
    }

    function AddUpdateTabs(TaskId) {

        jsPermission.ValidateAndSave(TaskId);
        jsFollowers.ValidateAndSave(TaskId);

    }

    function AddUpdateTask() {
        var validated = ValidateTask();
        if (validated) {
            var request = {
                TaskId: $('#TaskId').val(), Description: $('#taskDescription').val(),
                Name: $('#task-name').val(), TemplateId: $('#CurrentTemplateId').val(),
                UserIds: $('#item_UserList').val(), StartDate: $('#StartDate').val(),
                DueDate: $('#DueDate').val(),
                JobId: $('#JobId').val(),
                IsTaskInstance: $('#IsTaskInstance').val(),
                CreatedBy: $('#CreatedBy').val(),
                ModifiedBy: $('#ModifiedBy').val(),
                Priority: $("input[name=Priority]:checked").val(),
                PercentComplete: $('#PercentComplete').val(),
                EstimatedHours: $('#EstimatedHours').val(),
                EstimatedMinutes: $('#EstimatedMinutes').val(),
                Followers: jsFollowers.GetFollowers()
            };
            isAddMode = false;
            var result = CallController('/Workflow/AddEditTask', 'Post', request);
            if (typeof result != undefined && result.HttpCode != "200") {
                ErrorMessage('', result.Message);
                return;
            } else {
                $('#TaskAddModal').modal('hide');

                // Call Save/Post of All Tabs from below lines.
                AddUpdateTabs(result.OperationStatuses[0].UniqueKey);
                GetValues();
                SuccessMessage('A', '');
                jsTask.BindTaskList();
            }
        }
    }

    function HideMenu() {
        if ($(".tooloptiontext").hasClass("intro")) {
            $(".tooloptiontext").removeClass("intro");
        }
    }   

    function MarkIsCompleted(e, TaskId) {         
        var value = $(e).is(":checked")
        ChangeStatus(TaskId,value);
    }  

    function ChangeStatus(TaskId, value) {       
        var request = {
            TaskId: TaskId,
            IsCompleted: value,
            JobId: $('#JobIdForTemplate').val()
        };
        var result = CallController('/Workflow/MarkIsCompleted', 'Post', request);
        if (typeof result != undefined && result.HttpCode != "200") {
            var mValidate = $('#ValidateModal');
            mValidate.find('.modal-body').text(result.Message);
            $('#ValidateModal').modal('show');
            return;
        }
    }

    return {
        FormType_id: FormType_id, GetValues: GetValues,
        AddUpdate: AddUpdate, AddUpdateTask: AddUpdateTask, ValidateAndSave: ValidateAndSave, GetInitialValues: GetInitialValues, GetEditValues: GetEditValues,
        GetEditValuesTask: GetEditValuesTask, GetTaskInitialValues: GetTaskInitialValues, MarkIsCompleted: MarkIsCompleted,
        DeleteValues: DeleteValues, DeleteValuesTask: DeleteValuesTask, mainFormID: mainFormID, HideMenu: HideMenu
    }
}();

