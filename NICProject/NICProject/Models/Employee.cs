﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace NICProject.Models
{
    public class Employee
    {

        public string Id { get; set; }
         [Required]
        public string Identification { get; set; }
         [Required]
        public string CardNo { get; set; }
         [Required]
        public string FirstName { get; set; }
         [Required]
        public string MiddleName { get; set; }
         [Required]
        public string LastName { get; set; }
         [Required]
        public string Address { get; set; }
         [Required]
        public string Mobile { get; set; }
         [Required]
        public string Date { get; set; }
         [Required]
        public string Time { get; set; }
         [Required]
        public string Company { get; set; }
         [Required]
        public string Purpose { get; set; }

    }
}