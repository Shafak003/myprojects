var siteRoot;
var ClientOtherField = "New Client";
var ddlNewClientId;
var AddressFinderAccessKey = "FWE6PJAGK3HLX4Y8QVTN";
function CallController(url, type, parameter, dataType) {
    if (siteRoot.length == 1) {
        siteRoot = '';
    }
    dataType = dataType || 'json';
    var data;
    $("#wait").css("display", "block");
    //$("body").css("overflow", "hidden");
    $.ajax({
        type: type,
        cache: false,
        async:  false,
        url: siteRoot + url,
        dataType: dataType,
        data: parameter,
        error: function (XMLHttpRequest, textStatus, errorThrown) {
            alert(errorThrown);
            //$("body").css("overflow", "auto");
        },
        success: function (result) {
            data = result;
        },
        complete: function () {
            $("#wait").css("display", "none");
            //$("body").css("overflow", "auto");
        }
    });
    return data;
}

function CallControllerV2(url, type, parameter, dataType) {
    if (siteRoot.length == 1) {
        siteRoot = '';
    }
    dataType = dataType || 'json';
    $("#wait").css("display", "block");
    return $.ajax({
        type: type,
        cache: false,
        url: siteRoot + url,
        dataType: dataType,
        data: parameter,
    });
}

function SuccessMessage(Type, Message) {
    $("#success-alert").alert();

    if (Type == 'A') {
        $('#success-alert #hassuccess').text(Message != '' ? Message : 'Record has been saved successfully.');
    } else if (Type == 'U') {
        $('#success-alert #hassuccess').text(Message != '' ? Message : 'Record has been updated successfully.');
    } else if (Type == 'D') {
        $('#success-alert #hassuccess').text(Message != '' ? Message : 'Record has been deleted successfully.');
    } else {
        $('#success-alert #hassuccess').text(Message != '' ? Message : 'Record has been saved successfully.');
    }

    $("#success-alert").fadeTo(3000, 1000).slideUp(2000, function () {
        $("#success-alert").slideUp(2000);
    });
}

function ErrorMessage(Type, Message) {
    $("#error-alert").alert();

    $('#haserror').text(Message != '' ? Message : 'Some error has occured.');

    $("#error-alert").fadeTo(3000, 1000).slideUp(2000, function () {
        $("#error-alert").slideUp(2000);
    });
}

function ErrorGritter(Message) {
    $.gritter.add({
        title: 'Failure',
        text: Message != '' ? Message : 'Some error has occured.',
        sticky: false,
        time: '3000'
    });
}

function SuccessGritter(Type, Message) {

    if (Type == 'A') {
        Message != '' ? Message : 'Record has been saved successfully.';
    } else if (Type == 'U') {
        Message != '' ? Message : 'Record has been updated successfully.';
    } else if (Type == 'D') {
        Message != '' ? Message : 'Record has been deleted successfully.';
    } else {
        Message != '' ? Message : 'Record has been saved successfully.';
    }

    $.gritter.add({
        title: 'Success',
        text: Message != '' ? Message : 'Some error has occured.',
        sticky: false,
        time: '3000'
    });
}

function ModelSuccessMessage(Selector, Type, Message) {
    $(Selector + ' #Model-success-alert').alert();

    if (Type == 'A') {
        $(Selector + ' #Model-success-alert #hasmessage').text(Message != '' ? Message : 'Record has been saved successfully.');
    } else if (Type == 'U') {
        $(Selector + ' #Model-success-alert #hasmessage').text(Message != '' ? Message : 'Record has been updated successfully.');
    } else if (Type == 'D') {
        $(Selector + ' #Model-success-alert #hasmessage').text(Message != '' ? Message : 'Record has been deleted successfully.');
    } else {
        $('#hasmessage').text(Message != '' ? Message : 'Record has been saved successfully.');
    }

    $(Selector + ' #Model-success-alert').fadeTo(3000, 1000).slideUp(2000, function () {
        $(Selector + ' #Model-success-alert').slideUp(500);
    });
}

function ModelErrorMessage(Selector, Type, Message) {
    $(Selector + ' #Model-error-alert').alert();

    $(Selector + ' #Model-error-alert #haserror').text(Message != '' ? Message : 'Some error has occured.');

    $(Selector + ' #Model-error-alert').fadeTo(3000, 1000).slideUp(2000, function () {
        $(Selector + ' #Model-error-alert').slideUp(2000);
    });
}

function GetShortName(name) {
    var matches = name.match(/\b(\w)/g);
    if (matches != null) return matches.join('');
    return "";
}

function ParseDate(timestamp) {
    // create a new date object
    if (timestamp != "/Date(-62135596800000)/") {
        var newDate = new Date(parseInt(timestamp.substr(6)));
        var dd = newDate.getDate();
        var mm = newDate.getMonth() + 1; //January is 0!

        var yyyy = newDate.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }
        var curDate = mm + '/' + dd + '/' + yyyy;
        return curDate;
    } else {
        return null;
    }
    
}

function ToDateFormat(timestamp) {
    timestamp = new Date(timestamp);
    var toDate = (timestamp.getMonth() + 1) + '/' + timestamp.getDate() + '/' + timestamp.getFullYear()
    return toDate;
}
