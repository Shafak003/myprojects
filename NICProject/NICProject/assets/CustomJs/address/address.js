var kendoGrid;
var addressTypes = null;
// Format Address Type Combo
function formatAddressType(id) {
    var value = null;
    for (var i = 0, iLen = addressTypes.length; i < iLen; i++) {
        if (addressTypes[i].Id == id) {
            value = addressTypes[i].Name;
        }
    }
    return value;
}

AddressFile = function () {
    var isEditing = null;

    var mainFormID = null;
    var isAddMode = false;
    var FormType_id = "";
    var FundRequestId = null;
    //Address Datatable
    //var oTable = $('#address-table').dataTable({
    //    "paging": true,
    //    "filter": true,
    //    "pageLength": 25,
    //    "aoColumns": [
    //        null,
    //        null,
    //        null,
    //        null,
    //        null,
    //        null,
    //        null,
    //        { "bSortable": false }
    //    ]
    //});

    function AttachAddress() {

        var selectedValue = $("#AttachAddressTypeId :selected").text();
        if (!ValidateData(selectedValue)) {
            return;
        }

        var addressType = $('#AttachAddressTypeId').val();
        var addressId = $('#address_id').val();
        if ((AddressFile.mainFormID > 0 && AddressFile.mainFormID != null && AddressFile.mainFormID != undefined) || (AddressFile.FundRequestId > 0 && AddressFile.FundRequestId != null && AddressFile.FundRequestId != undefined)) {
            if (addressId != undefined && addressId > 0) {
                var request = {
                    AddressTypeId: addressType, AddressId: addressId, Main_id: AddressFile.mainFormID, id: 0, FormType_id: AddressFile.FormType_id, FundRequestId: FundRequestId
                }
                var result = CallController('/Address/AddressAttach', 'Post', request);
                if (typeof result != undefined && result.HttpCode != "200") {
                    ModelErrorMessage('#Message', '', result.Message);
                    return;
                } else {
                    //$('#AddressAttachModal').modal('hide');
                    closeNav('AddressAttachModal');
                    GetValues();
                    SuccessMessage('U', '');
                }
            } else {
                ModelErrorMessage('#Message', '', 'Must Select Address before Attachment.');
            }
        } else {
            ModelErrorMessage('#Message', '', 'Mandatory fields missing on Main Tab.');
        }
    }

    function GetValues() {
        //$("#address-table > tbody:last").children().remove();
        //oTable.fnClearTable();
        var request = { ID: (AddressFile.mainFormID != null ? AddressFile.mainFormID : AddressFile.FundRequestId), FormType: AddressFile.FormType_id };
        var result = CallController('/Address/AddressPartial', 'Get', request);
        addressTypes = CallController('/Address/AddressPartialGetTypes', 'Get', null);
        //$.each(result, function (i, item) {
        //    var aiNew = oTable.fnAddData([
        //     formatAddressType(item.AddressTypeId), item.AddressLine1, item.AddressLine2, item.City, item.State, item.PostCode, item.Country,
        //    "<a href='#'  class='btn btn-info edit btn-xs' onclick='AddressFile.GetEditValues(" + item.id + "," + item.AddressId + ");'><i class='fa fa-edit'></i> Edit</a> <a href='#' class='btn btn-danger delete btn-xs' onclick='AddressFile.DeleteValues(" + item.id + ");'><i class='fa fa-trash-o'></i> Delete</a>"
        //    ]);

        //});
        if (!kendoGrid) {
            kendoGrid = $("#address-table").kendoGrid({
                dataSource: {
                    data: result,
                    pageSize: 10
                },
                //height: 950,
                sortable: true,
                filterable: true,
                pageable: {
                    pageSizes: true,
                    buttonCount: 5
                },
                columns: [
                    {
                        //field: "AddressTypeId",
                        template: "#= formatAddressType(AddressTypeId) #",
                        //template: '#= Simple(OffDuty,OnDuty) #',
                        title: "Address Type",
                        width: 150
                    },
                {
                    field: "AddressLine1",
                    title: "Address Line 1",
                    width: 150
                }, {
                    field: "City",
                    title: "City",
                    width: 120
                }, {
                    field: "State",
                    title: "State",
                    width: 120
                }, {
                    field: "PostCode",
                    title: "Post Code",
                    width: 120
                }, {
                    field: "Country",
                    title: "Country",
                    width: 120
                }
                , {
                    template: "<div class='action-tools'><a class='btn btn-success btn-xs btn-font btn-primary' onclick='AddressFile.GetEditValues(#: id #,#: AddressId #);'><i class='fa fa-edit'></i></a>" +
                        "&nbsp;<a class='btn btn-danger btn-xs btn-font btn-Delete' onclick='AddressFile.DeleteValues(#: id #);'><i class='fa fa-trash-o'></i></a> </div>"
                }]
            });
        }
        else {
            kendoGrid.data('kendoGrid').dataSource.data(result);
            kendoGrid.data('kendoGrid').refresh();

        }
    }

    function GetEditValues(Id, AddressId) {

        GetInitialValues();
        var request = { ID: Id, FormType: AddressFile.FormType_id };
        var result = CallController('/Address/AddressPartialEdit', 'Get', request);
        $('#AddressId').val(result.AddressId);
        $('#AddressLine1').val(result.AddressLine1);
        $('#AddressLine2').val(result.AddressLine2);
        $('#City').val(result.City);
        $('#State').val(result.State);
        $('#PostCode').val(result.PostCode);
        $('#Country').val(result.Country);
        $('#id').val(result.id);
        $('#AddressTypeId_Old').val(result.AddressTypeId);

        $("#AddressTypeId option").each(function () {
            if ($(this).val() == result.AddressTypeId) {
                $(this).attr('selected', 'selected');
                //$('#select2-AddressTypeId-container').text($("#AddressTypeId :selected").text());
                $("#AddressTypeId").selectpicker('val', result.AddressTypeId);
            }
        });
        ShowAddressFinder(false)
        //$('#AddressPartialModal').modal('show');
        openNav("AddressPartialModal");

    }


    function ShowAddressFinder(flag) {
        if (flag) {
            $(".visible").hide();
            $(".txtMailingAddress").show();
            $('#show').prop('checked', false);
        }
        else {
            $(".visible").show();
            $(".txtMailingAddress").hide();
            $('#show').prop('checked', true);
        }
    }

    function ClearFields() {
        ShowAddressFinder(false);
        $("#address_field").val("");
        $('#AddressId').val('0');
        $('#AddressLine1').val('');
        $('#AddressLine2').val('');
        $('#City').val('');
        $('#State').val('');
        $('#PostCode').val('');
        $('#Country').val('');
        $('#id').val('0');
    }

    function GetInitialValues() {
        ClearFields();
        // For Address Type Combo
        var resultType = CallController('/Address/AddressPartialGetTypes', 'Get', null);
        addressTypeSelect = document.getElementById('AddressTypeId');

        // Previous
        $('#AddressTypeId').empty();
        if (addressTypeSelect != null) {
            $.each(resultType, function (i, item) {
                addressTypeSelect.options[addressTypeSelect.options.length] = new Option(item.Name, item.Id);
            });
        }
        $('#AddressTypeId').selectpicker('refresh');

    }

    function GetPopupValues() {
        // For Address Type Combo
        var resultType = CallController('/Address/AddressPartialGetTypes', 'Get', null);
        addressTypeSelect = document.getElementById('AttachAddressTypeId');
        $('#AttachAddressTypeId').empty();
        console.log("Address Type Combo");
        console.log(resultType);
        if (addressTypeSelect != null) {
            $.each(resultType, function (i, item) {
                addressTypeSelect.options[addressTypeSelect.options.length] = new Option(item.Name, item.Id);
            });
        }

        // For Address Combo
        var resultAddress = CallController('/Address/AddressGetAll', 'Get', null);
        addressSelect = document.getElementById('address_id');
        $('#address_id').empty();
        console.log("Address Combo");
        console.log(resultAddress);
        $.each(resultAddress, function (i, item) {
            addressSelect.options[addressSelect.options.length] = new Option(item.Name, item.Id);
        });

        //$("#AttachAddressTypeId").select2({ dropdownParent: $("#AddressAttachModal") });
        //$("#address_id").select2({ dropdownParent: $("#AddressAttachModal") });
    }



    function DeleteValues(Id) {
        bootbox.confirm({
            message: "Are you sure you want to delete?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    var request = { ID: Id, FormType: AddressFile.FormType_id };
                    CallController('/Address/AddressPartialDelete', 'Post', request);
                    SuccessMessage('D', '');
                    GetValues();
                }
                else
                    return;
            }
        });
    }

    function ValidateData(selectedValue) {
        // Validate Address Type if Exist
        var isExist = false;
        $('#address-table > tbody  > tr').each(function () {
            if (this.cells[0].innerHTML == selectedValue) {
                ModelErrorMessage('#Message', '', 'Address Type Already Exist.');
                isExist = false;
                return isExist;
            } else {
                isExist = true;
            }
        });
        return isExist;
    }

    function ValidateAndSave() {
        var frmAddress = $('input[name="AddressLine1"]').closest("form");
        $.validator.unobtrusive.parse(frmAddress);
        frmAddress.validate();
        if (frmAddress.valid()) {
            AddUpdate();
        }
    }

    function AddUpdate() {
        var selectedValue = $("#AddressTypeId :selected").text();
        var newValue = $('#AddressTypeId').val();
        var oldValue = $('#AddressTypeId_Old').val();
        //if (oldValue != newValue && !ValidateData(selectedValue)) {
        //    return;
        //}

        if ((AddressFile.mainFormID > 0 && AddressFile.mainFormID != null && AddressFile.mainFormID != undefined) || (AddressFile.FundRequestId > 0 && AddressFile.FundRequestId != null && AddressFile.FundRequestId != undefined)) {
            var request = {
                FundRequestId: AddressFile.FundRequestId,
                AddressTypeId: $('#AddressTypeId').val(), AddressId: $('#AddressId').val(), AddressLine1: $('#AddressLine1').val(), AddressLine2: $('#AddressLine2').val(), City: $('#City').val(),
                State: $('#State').val(), PostCode: $('#PostCode').val(), Country: $('#Country').val(), id: $('#id').val(), Main_id: AddressFile.mainFormID, FormType_id: AddressFile.FormType_id
            }
            isAddMode = false;
            var result = CallController('/Address/AddressPartial', 'Post', request);
            if (typeof result != undefined && result.HttpCode != "200") {
                ModelErrorMessage('#Message', '', result.Message);
                return;
            } else {
                //$('#AddressPartialModal').modal('hide');
                closeNav('AddressPartialModal');
                GetValues();
                SuccessMessage('U', '');
            }
        } else {
            ModelErrorMessage('#Message', '', 'Mandatory fields missing on Main Tab.');
        }
    }

    return {
        FormType_id: FormType_id, GetValues: GetValues,
        GetPopupValues: GetPopupValues, AttachAddress: AttachAddress,
        AddUpdate: AddUpdate, ValidateAndSave: ValidateAndSave, GetInitialValues: GetInitialValues, GetEditValues: GetEditValues,
        DeleteValues: DeleteValues, mainFormID: mainFormID
    }
}();



(function () {
    var widget, initAF = function () {
        widget = new AddressFinder.Widget(
            document.getElementById('address_field'),
            AddressFinderAccessKey,
            'AU',
            { show_locations: false }
        );

        widget.on("result:select", function (fullAddress, metaData) {
            document.getElementById('AddressLine1').value = metaData.address_line_1;
            document.getElementById('AddressLine2').value = metaData.address_line_2;
            document.getElementById('City').value = metaData.locality_name;
            document.getElementById('State').value = metaData.state_territory;
            document.getElementById('PostCode').value = metaData.postcode;
            document.getElementById('Country').value = "Australia";
            $('.af_list').hide();
        });
    };

    $(document).ready(function () {
        $.getScript('https://api.addressfinder.io/assets/v3/widget.js', initAF);

        $("#show").click(function () {
            var val = $('#show').is(":checked");
            if (val == true) {
                $(".visible").show();
                $(".txtMailingAddress").hide();
            } else {
                $(".visible").hide();
                $(".txtMailingAddress").show();
            }
        });

    });

})();
