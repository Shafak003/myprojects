﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculatorfinal
{
    public partial class Form1 : Form
    {
        Double ResultValue = 0;
        String OperationPerformed = "";
        bool isOperationPerformed = false;
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)   //CE this is for clear Entire Screen
        {
            TextBox_Result.Text = "0";
        }

        private void button_Click(object sender, EventArgs e) // Click button produce (Function) or Event For 1 to 9 number.
        {
            if ((TextBox_Result.Text == "0") || (isOperationPerformed))
               TextBox_Result.Clear(); // It makes zero don't appear in the starting
            isOperationPerformed = false;
            Button Button = (Button)sender;
            if(Button.Text == ".")
            {
               if(!TextBox_Result.Text.Contains("."))
                    TextBox_Result.Text = TextBox_Result.Text + Button.Text;
            }
            else

            TextBox_Result.Text = TextBox_Result.Text+ Button.Text;  // it concatinate the tectbox numbers  1111 or 2222 or 2345
        }

        private void operation_click(object sender, EventArgs e) // operation click in + button create a (functions) or Events for operators  +,-,* etc
        {
           
            Button Button = (Button)sender;    // this function send the button click event / (function) so that when click any arthemetic it takes the value             
            OperationPerformed = Button.Text; //button click function put into the variable button and again put buuton.text event in Operationperformed
            ResultValue = Double.Parse(TextBox_Result.Text); //It gives the resultvalue first convert it(screen result ) into string or integer by by double parse 
            labelCurrentOperation.Text = ResultValue + " " + OperationPerformed;
            isOperationPerformed = true;
            

        }

        private void button6_Click(object sender, EventArgs e)
        {
            TextBox_Result.Text = "0"; //it is used for Clear evrything "C" symbol in calculator
            ResultValue = 0;
        }

        private void button16_Click(object sender, EventArgs e) // This EVent is in equal to operator and contaion whole methods for all operator
        {            

            switch (OperationPerformed) // Switch statement check the variable Operationperformed for all operators
            {
                case "+":
                    TextBox_Result.Text = (ResultValue + Double.Parse(TextBox_Result.Text)).ToString();// shows in screen/textbox screen concatinate result with double parse means again convert it in to string
                    break;
                case "-":
                    TextBox_Result.Text = (ResultValue - Double.Parse(TextBox_Result.Text)).ToString();
                    break;
                case "*":
                    TextBox_Result.Text = (ResultValue * Double.Parse(TextBox_Result.Text)).ToString();
                    break;
                case "/":
                    TextBox_Result.Text = (ResultValue / Double.Parse(TextBox_Result.Text)).ToString();
                    break;//

            }
        }
    }
}
